note
	description: "Summary description for {EAC_LOGIN_DIALOG}."
	author: "Howard Thomson"
	date: "25-May-2020"

	TODO: "[
		Use login name to [temporarily !] determine the host URL for connection ...
			ht => inproc ... for testing
			jt => server on ht-shuttle

		Add box for database selection [thomson ...]
		Need means of setting up new user/password, initial user/password
	]"

class
	EAC_LOGIN_DIALOG

inherit
		EV_DIALOG
		redefine
			create_interface_objects,
			initialize,
			show_relative_to_window,
			show_modal_to_window
		end



	EAC_REPORTER
		undefine default_create, copy end

create
	default_create

feature {NONE}

	create_interface_objects
		do
			Precursor
			create box
			create url_label
			create host_url
			create user_label
			create user_name
			create pwd_label
			create password
			create ok_button
			create cancel_button
			ok_button.set_text ("Connect")
			cancel_button.set_text ("Cancel")
		end

	initialize
		local
			x, y, w, h: INTEGER
			dx, dy: INTEGER
		do
			Precursor
			set_title ("Connect / Login")
			extend (box)
		--	box.set_minimum_size (300, 300)
		--	box.show
			user_label.set_text ("Username: ")
			pwd_label.set_text ("Password: ")
			url_label.set_text ("Host URL: ")
			w := 200
			h := 20
			dx := 80
			dy := 30

		-- This does not work, the item 'user_name' is not displayed!
		--	box.extend_with_position_and_size (user_name, x+dx, y, 79, 17)

		-- This does not work either, the position stays at 0,0 not x+dx,y
		--	box.extend (user_name)
		--	box.set_item_position_and_size (user_name, x + dx, y, 79, 17)

			box.extend (user_label)
			box.set_item_position (user_label, x, y)
			box.extend (user_name)
			box.set_item_position (user_name, x + dx, y)
			box.set_item_width (user_name, w)
			y := y + dy

			box.extend (pwd_label)
			box.set_item_position (pwd_label, x, y)
			box.extend (password)
			box.set_item_position (password, x + dx, y)
			box.set_item_width (password, w)
			y := y + dy

			box.extend (url_label)
			box.extend (host_url)
			box.set_item_position (url_label, x, y)
			box.set_item_position (host_url, x + dx, y)
			box.set_item_width (host_url, w)
			y := y + dy

			box.extend (ok_button)
			box.set_item_position (ok_button, x + dx, y)
			y := y + dy

			box.extend (cancel_button)
			box.set_item_position (cancel_button, x + dx, y)

			key_press_actions.extend (agent cancel)

			box.key_press_actions.extend (agent cancel)
			cancel_button.key_press_actions.extend (agent cancel)

			box.resize_actions.extend (agent box_resized)
			user_name.focus_out_actions.extend (agent user_name_leave_focus)

			ok_button.select_actions.extend (agent set_ok_selected)
		end

feature {EAC_MAIN_WINDOW}

	box: EV_FIXED

	user_label: EV_LABEL
	user_name: EV_TEXT_FIELD

	pwd_label: EV_LABEL
	password: EV_PASSWORD_FIELD

	url_label: EV_LABEL
	host_url: EV_TEXT_FIELD

	ok_button: EV_BUTTON
	cancel_button: EV_BUTTON

	cancel (a_key: EV_KEY)
		do
			if a_key.code.is_equal (a_key.key_escape) then
				destroy
			end
		end

	ok_selected: BOOLEAN

	set_ok_selected
		do
			ok_selected := True
			destroy
		end

	user_name_leave_focus
		do
			if host_url.text.is_empty then
					-- TEMP !!!
				if user_name.text.is_equal ("jt") then
					host_url.set_text ("tcp://ht-shuttle")
				elseif user_name.text.is_equal ("ht") then
					host_url.set_text ("inproc://eac_test")
				end
			end
		end

	box_resized (x, y, w, h: INTEGER_32)
		local
			l_w: INTEGER
		do
			l_w := w - user_name.x_position - 15
			box.set_item_width (user_name, l_w)
			box.set_item_width (password, l_w)
			box.set_item_width (host_url, l_w)
		end

	show_relative_to_window (a_window: EV_WINDOW)
		do
			load_defaults
			Precursor (a_window)
		end

	show_modal_to_window (a_window: EV_WINDOW)
		do
			load_defaults
			ok_selected := False
			Precursor (a_window)
		end

	load_defaults
		do
		end

end
