note
	description: "Summary description for {MAIN_WINDOW}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

	comment: "Class MAIN_WINDOW is a provider to the Amalasoft cluster ... "

class
	MAIN_WINDOW

inherit
	EAC_MAIN_WINDOW

end
