note
	description: "Summary description for {EAC_MESSAGE}."
	author: "Howard Thomson"
	date: "16-May-2020"

	TODO: "[
		Adjust nng_msg_len() to match buffer_position at an appropriate time ... nng_msg_chop()
		Consider what happens about 'dispose' when using 'from_nng_message' creation.
	]"

	notes: "[
		Use class REFLECTOR to create receiving instance of class ?
		Use Eiffel Serialize/Deserialize classes ?
	]"
class
	EAC_MESSAGE

inherit
	SED_STORABLE_FACILITIES
		undefine default_create end
	EAC_MSG_TYPE_IDS
		undefine default_create end
	NNG_MESSAGE
		rename
			managed_pointer as nng_managed_pointer
		redefine
			default_create,
			make_size
		end
	SED_BINARY_READER_WRITER
		export
			{ANY} buffer, buffer_position
		undefine
			default_create
		redefine
			write_footer
		end

create
	default_create,
	make_size,
	from_nng_message

feature {NONE} -- Creation

	default_create
		do
			Precursor {NNG_MESSAGE}
			create {MANAGED_POINTER} buffer.share_from_pointer (default_pointer, 0)
			buffer_size := 0
		ensure then
			buffer_set: buffer /= Void
			buffer_size_set: buffer_size = 0
		end

	make_size (a_size: INTEGER)
		do
			Precursor (a_size)
			create {MANAGED_POINTER} buffer.share_from_pointer (msg_body_ptr, a_size)
			buffer_size := a_size
		end

	from_nng_message (a_nng_message: NNG_MESSAGE)
		local
			l_size: INTEGER_32
		do
			default_create
			set_msg_ptr (a_nng_message.msg_ptr)
			l_size := a_nng_message.count
			create {MANAGED_POINTER} buffer.share_from_pointer (msg_body_ptr, l_size)
			buffer_size := l_size
			a_nng_message.set_msg_ptr (default_pointer)
		end

feature -- Selection ID for processing

--	type_id: INTEGER

feature -- Processing

--	process_request
--			-- Routine to execute the message action on the receiving side
--		do

--			inspect type_id
--			when Msg_connect then

--			else
--				check False end
--			end
--		end

--	process_reply
--			-- Routine for the sender to process the reply
--		do

--		end


feature

	check_buffer (n: INTEGER)
		local
			l_new_size: INTEGER_32
		do
--			check false end
			--TODO (see SED_MEMORY_READER_WRITER)
				-- Resize the NNG allocated space
				-- Update the 'buffer' MANAGED_POINTER
				-- Update buffer_size

			if msg_ptr = default_pointer then
				last_error := nng_msg_alloc (Current, Default_buffer_size)
				buffer.set_from_pointer (msg_body_ptr, Default_buffer_size)
				buffer_size := Default_buffer_size
			else
				if n + buffer_position > buffer_size then
				--	buffer.resize ((n + buffer_position).max (buffer.count + buffer.count // 2))
						-- Resize the message buffer
					l_new_size := (n + buffer_position).max (count + count // 2)
					last_error := c_nng_msg_realloc (msg_ptr, l_new_size)
					check last_error = 0 end
					buffer.set_from_pointer (msg_body_ptr, l_new_size)
					buffer_size := count
				end
			end
		end

	write_footer
		do
			if count > buffer_position then
				last_error := c_nng_msg_chop (msg_ptr, count - buffer_position)
				check last_error = 0 end
				check count = buffer_position end
			end
--			check false end
		end

feature

	memory_reader_writer: SED_MEMORY_READER_WRITER
		once
			create Result.make
		end

end
