note
	description: "GUI interaction class between design tool and Eiffel code"
	author: "Howard Thomson"

	TODO: "[
		Rename this class to be more in line with it's future functionality ...
	]"

class EV_WIDGET_FACTORY

inherit
	ANY redefine default_create end

create
	default_create

feature -- Creation

	default_create
		do
			create table.make (20)
			create known_keys.make (20)
			known_keys.extend (0, "vbox")
		end

feature -- Attributes

	table: HASH_TABLE [ EV_WIDGET, STRING ]

	known_keys: STRING_TABLE [ INTEGER ]

feature -- Registration for widgets known to the calling code

	register_widget (a_widget: EV_WIDGET; a_tag: STRING)
		require
			tag_is_known: known_keys.has (a_tag)
		do
			table.extend (a_widget, a_tag)
		end

end
