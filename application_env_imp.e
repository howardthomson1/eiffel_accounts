note
	description: "Provides platform specific implementation."
	system: "Part of the Amalasoft Eiffel Library."
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 1995-2013 Amalasoft Corporation.  All Rights Reserved"
	date: "See comments at bottom of class."
	revision: "See comments at bottom of class."
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."

class APPLICATION_ENV_IMP

inherit
	AEL_APPLICATION_ENV_IMP

--|========================================================================
feature {APPLICATION_ENV} -- Constants
--|========================================================================

	Ks_log_path_parent: STRING = "/var/log"
			-- Name of directory in which the log directory for this 
			-- application will reside.  This NOT the name of the log 
			-- directory itself, but of its parent directory.
			--| Replace this with your own.

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-Dec-2013
--|     Created original module (for Eiffel 7.2, void-safe)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is inherited by AEL_APPLICATION_ENV_IMP.
--|
--| Edit the value of 'Ks_log_path_parent' as needed.
--| Extend this class to accommodate platform specifics.
--|----------------------------------------------------------------------

end -- class APPLICATION_ENV_IMP
