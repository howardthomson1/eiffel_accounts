note
	description: "Summary description for {EAC_REQUEST_TEST}."
	author: "Howard Thomson"
	date: "20-Feb-2022"
	revision: "$Revision$"

class
	EAC_REQUEST_TEST

inherit
	EAC_REQUEST

create
	make

feature

	execute
		do
			main_window.eac_request_add_to_idle (agent xxx)
		end

	xxx -- (m: detachable MANAGED_POINTER)
		do
			print ("Do once on idle%N")
		end

end
