note
	description: "Summary description for {EAC_REQUEST}."
	author: "Howard Thomson"
	date: "22-April-2021"

	extended_description: "[
		Base class for network request root class for an EAC Request
		
		This is executed in the context of the GUI ...
	]"


deferred class
	EAC_REQUEST

feature {ANY} -- Routine(s)


	main_window: EAC_MAIN_WINDOW
		-- TODO change to more appropriate ancestor

	make (a_target: like main_window)
		do
			main_window := a_target
		end

	execute
		deferred
		end
end
