note
	description: "Summary description for {EAC_RESPONSE}."
	author: "Howard Thomson"
	date: "22-April-2021"

deferred class
	EAC_RESPONSE

feature {ANY} -- Routine(s)


	main_window: EAC_MAIN_WINDOW
		-- TODO change to more appropriate ancestor

	make (a_target: like main_window)
		do
			main_window := a_target
		end

	execute
		deferred
		end

end
