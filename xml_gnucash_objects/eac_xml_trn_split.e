note
	description: "Summary description for {EAC_XML_TRN_SPLIT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_TRN_SPLIT

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			report_callback
		end

feature

	xml_tag: STRING = "trn:split"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	Precursor (a_namespace, a_prefix, a_local_part, a_value)
			if not attached {EAC_XML_TRN_SPLITS} parent as l_parent then
				check false end
			end

		end

feature -- Attributes

	account: detachable STRING

	set_account (a_account: STRING)
		do
			account := a_account
		end

	id: detachable STRING

	set_id (an_id: STRING)
		do
			id := an_id
		end

	action: detachable STRING

	set_action (an_action: STRING)
		do
			action := an_action
		end

	memo: detachable STRING

	set_memo (a_memo: STRING)
		do
			memo := a_memo
		end

	quantity: detachable STRING

	set_quantity (a_quantity: STRING)
		do
			quantity := a_quantity
		end

	reconciled: detachable STRING

	set_reconciled (a_reconciled: STRING)
		do
			reconciled := a_reconciled
		end

	value_string: detachable STRING

	set_value_string (a_value_string: STRING)
		do
			value_string := a_value_string
		end

	value: INTEGER_64

	set_value (a_value: INTEGER_64)
		do
			value := a_value
		end

	report_callback (a_callbacks: XML_GNUCASH_CALLBACKS)
		do
			if attached {EAC_XML_TRN_SPLITS} parent as l_parent then
				l_parent.add_split (Current)
				if value < 0 then
				--	l_parent.set_debit_value (value)
				else
				--	l_parent.set_credit_value (value)
				end
			else
				check false end
			end
		end



end
