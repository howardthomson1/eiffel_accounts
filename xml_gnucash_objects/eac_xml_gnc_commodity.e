note
	description: "Summary description for {EAC_XML_COMMODITY}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_GNC_COMMODITY

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute
		end


feature

	xml_tag: STRING = "gnc:commodity"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
	--		print ("Processing Commodity attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

end
