note
	description: "Summary description for {EAC_XML_TRN_SPLITS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_TRN_SPLITS

inherit
	EAC_XML_OBJECT
		redefine
			default_create,
			process_attribute,
			report_callback
		end

	EAC_REPORTER
		undefine
			default_create
		end

feature

	default_create
		do
			create splits.make (10)
			Precursor {EAC_XML_OBJECT}
		end

	xml_tag: STRING = "trn:splits"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	splits: HASH_TABLE [EAC_XML_TRN_SPLIT, STRING]

	add_split (a_split: EAC_XML_TRN_SPLIT)
		do
			if attached a_split.id as l_id then
				splits.extend (a_split, l_id)
			else
				report ("Void Account for split ...%N")
			end
		end

	report_callback (a_callbacks: XML_GNUCASH_CALLBACKS)
		do
			if attached {EAC_XML_GNC_TRANSACTION} parent as l_parent then
				l_parent.set_splits (Current)
			else
				check false end
			end
		end



end
