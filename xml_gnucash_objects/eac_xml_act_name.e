note
	description: "Summary description for {EAC_XML_ACT_NAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_ACT_NAME

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end


feature

	xml_tag: STRING = "act:name"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_content (a_content: READABLE_STRING_32)
		do
				-- This routine will be redefined in descendants where
				-- content processing is needed ...
		--	print ("%NEAC_XML_ACT_NAME:process_content called ...%N")
			if attached {EAC_XML_GNC_ACCOUNT} parent as a_parent then
				a_parent.set_name (a_content)
			end

		end



end
