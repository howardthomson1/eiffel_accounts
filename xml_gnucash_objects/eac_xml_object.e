note
	description: "Summary description for {XML_OBJECT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	EAC_XML_OBJECT

inherit
	ANY
		redefine
			default_create
		end

feature

	default_create
		do
		--	content_processor := agent process_string_content
			content_processor := agent discard_content_processor
		end

	xml_tag: STRING
			-- The GnuCash tag for this object class
		deferred
		end

	parent: detachable EAC_XML_OBJECT

	set_parent (a_parent: detachable EAC_XML_OBJECT)
		do
			parent := a_parent
		end

	target: detachable XML_GNUCASH_CALLBACKS

	set_target (a_target: detachable XML_GNUCASH_CALLBACKS)
		do
			target := a_target
		end

	content_processor: ROUTINE [ detachable TUPLE [READABLE_STRING_32]]
		-- The agent to use when processing the content after the end of the start tag
		-- Set by 'process_attribute' below

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		local
			l_str: STRING
		do
			if a_prefix /= Void and then a_prefix.is_equal ("xmlns") then
					-- Ignore
				content_processor := agent discard_content_processor
			elseif a_prefix /= Void and then a_prefix.is_equal ("cd") then
					-- Ignore for the moment: Count data
				content_processor := agent default_content_processor
			elseif a_local_part.is_equal ("type") then
				if a_value.is_equal ("string") then
					content_processor := agent process_string_content
				elseif a_value.is_equal ("guid") then
					content_processor := agent process_guid_content
				elseif a_value.is_equal ("numeric") then
					content_processor := agent process_numeric_content
				elseif a_value.is_equal ("integer") then
					content_processor := agent process_integer_content
				elseif a_value.is_equal ("frame") then
					content_processor := agent process_frame_content
				elseif a_value.is_equal ("gdate") then
					content_processor := agent process_gdate_content
				else
					check unexpected_a_value: false end
				end
			else
				check a_local_part.is_equal ("version") and
				(a_value.is_equal ("2.0.0") or a_value.is_equal ("1.0.0")) end
			end
		end

feature -- Content processing

	process_content (a_content: READABLE_STRING_32)
		do
			if not a_content.is_whitespace then
				content_processor.call ([ a_content ])
			end
		end

	default_content_processor (a_content: READABLE_STRING_32)
		do
--			print ("default_content_processor called ...%N")
			check false end
		end

	discard_content_processor (a_content: READABLE_STRING_32)
		do
			-- Discard the 'content' ...
		end

	process_string_content (a_content: READABLE_STRING_32)
		do
				-- Redefined where needed ...
			check false end
		end

	process_guid_content (a_content: READABLE_STRING_32)
		do
				-- Redefined where needed ...
			check false end
		end

	process_numeric_content (a_content: READABLE_STRING_32)
		do
				-- Redefined where needed ...
			check false end
		end

	process_integer_content (a_content: READABLE_STRING_32)
		do
				-- Redefined where needed ...
			check false end
		end

	process_frame_content (a_content: READABLE_STRING_32)
		do
				-- Redefined where needed ...
			check false end
		end

	process_gdate_content (a_content: READABLE_STRING_32)
		do
				-- Redefined where needed ...
			check false end
		end

	set_link (a_callbacks: XML_GNUCASH_CALLBACKS)
		do
			-- Redefined in descendants
		end

	report_callback (a_callbacks: XML_GNUCASH_CALLBACKS)
		do

		end


end
