note
	description: "Summary description for {EAC_XML_ACT_NON_STANDARD_SCU}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_ACT_NON_STANDARD_SCU

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute
		end


feature

	xml_tag: STRING = "act:non-standard-scu"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

end
