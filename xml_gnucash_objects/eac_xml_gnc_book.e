note
	description: "Summary description for {EAC_XML_BOOK}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_GNC_BOOK

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			set_target
		end


feature

	xml_tag: STRING = "gnc:book"

	set_target (a_target: detachable XML_GNUCASH_CALLBACKS)
		do
			Precursor (a_target)
			if a_target /= Void then
				check a_target.xml_stack.count = 2 end
			end
		end


	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

end
