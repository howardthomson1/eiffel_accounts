note
	description: "Summary description for {EAC_XML_TS_DATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_TS_DATE

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end
	EAC_REPORTER
		undefine default_create end

feature

	xml_tag: STRING = "ts:date"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_content (a_content: READABLE_STRING_32)
		do
--			report ("EAC_XML_TS_DATE:process_content: " + a_content + "%N")
			if attached {EAC_XML_TRN_DATE_POSTED} parent as l_parent then
				if not a_content.is_whitespace then
					l_parent.set_date_posted (a_content)
				end
			elseif attached {EAC_XML_TRN_DATE_ENTERED} parent as l_parent then
				-- TODO: set_date_entered ...
			else
				check unexpected_parent_type: false end
			end
		end


end
