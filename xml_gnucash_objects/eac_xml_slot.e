note
	description: "Summary description for {EAC_XML_SLOT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_SLOT

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute
		end


feature

	xml_tag: STRING = "slot"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

feature -- Attributes

	value: detachable STRING

	set_value (a_value: STRING)
		do
			value := a_value
		end

	key: detachable STRING

	set_key (a_key: STRING)
		do
			key := a_key
		end

end
