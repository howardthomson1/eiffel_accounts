note
	description: "Summary description for {EAC_XML_SPLIT_RECONCILED_STATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_SPLIT_RECONCILED_STATE

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end

feature

	xml_tag: STRING = "split:reconciled-state"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end


	process_content (a_content: READABLE_STRING_32)
		do
			check a_content.is_equal ("y")
			or else a_content.is_equal ("n")
			or else a_content.is_equal ("c")
			end
		end

end
