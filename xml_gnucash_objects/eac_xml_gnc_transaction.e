note
	description: "Summary description for {EAC_XML_TRANSACTION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_GNC_TRANSACTION

inherit
	EAC_XML_OBJECT
		redefine
			default_create,
			process_attribute,
			set_link,
			report_callback
		end

	EAC_REPORTER
		undefine
			default_create
		end

feature

	default_create
		do
		--	create act_slots.make (10)
			Precursor {EAC_XML_OBJECT}
		end

	xml_tag: STRING = "gnc:transaction"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Transaction attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

feature -- Attributes

	id: detachable STRING
		-- Unique UUID for this transaction

	date_posted: detachable STRING
		-- Date for which entry applies


--	currency
--	date_entered


	description: detachable STRING
	num: detachable STRING

--	slots

	splits: detachable EAC_XML_TRN_SPLITS

feature -- Attribute setting

	set_id (an_id: STRING)
		do
			id := an_id
		end

	set_date_posted (a_date_string: STRING)
		do
	--		report ("{EAC_XML_GNC_TRANSACTION}.set_date_posted (%"")
	--		report (a_date_string)
	--		report ("%")%N")
			check not a_date_string.is_whitespace end
			date_posted := a_date_string
		end

	set_description (a_description: STRING)
		do
			description := a_description
		end

	set_num (a_num: STRING)
		do
			num := a_num
		end



	set_splits (a_splits: EAC_XML_TRN_SPLITS)
		do
			splits := a_splits
		end

feature

	set_link (a_callbacks: XML_GNUCASH_CALLBACKS)
		do

			if attached {EAC_XML_GNC_BOOK}parent as l_parent then
					-- Add this Account to the callbacks repository
		--		report ("Transaction with Book parent ...%N")
		--		a_callbacks.report_transaction (Current)

--				if act_name = Void then
--					report ("Void act_name ... %N")
--				end
--				if act_parent = Void then
--					report ("Void act_parent ... %N")
--				end
--				if act_type = Void then
--					report ("Void act_type ... %N")
--				end
--				if act_description = Void then
--					report ("Void act_description ... %N")
--				end
			else
					-- These accounts are part of the 'Template Transactions'
					-- part of the GnuCash data file.
					-- It is unclear (!) why there are numerous 'ROOT' accounts
					-- in this section of the data file ...
				check attached {EAC_XML_GNC_TEMPLATE_TRANSACTIONS}parent  end
	--			report ("Transaction with Template_transactions parent ...%N")
			end
		end

	report_callback (a_callbacks: XML_GNUCASH_CALLBACKS)
		do
			a_callbacks.report_transaction (Current)
		end

end
