note
	description: "Summary description for {EAC_XML_SPLIT_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_SPLIT_ACCOUNT

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end

feature

	xml_tag: STRING = "split:account"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_content (a_content: READABLE_STRING_32)
		do
			if attached {EAC_XML_TRN_SPLIT} parent as l_parent then
				l_parent.set_account (a_content)
			else
				check unexpected_parent_type: false end
			end
		end

--	process_content (a_content: READABLE_STRING_32)
--		do
--			if attached {EAC_XML_TRN_SPLIT} parent as l_parent then
--				l_parent.set_action (a_content)
--			end
--		end



end
