note
	description: "Summary description for {AEC_XML_ACT_DESCRIPTION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

	TODO: "[
		Detect and truncate multi-line (!) descriptions, and trailing spaces ...
	]"

class
	EAC_XML_ACT_DESCRIPTION

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end


feature

	xml_tag: STRING = "act:description"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_content (a_content: READABLE_STRING_32)
		local
			l_string: STRING_32
			i: INTEGER
		do
			if attached {EAC_XML_GNC_ACCOUNT} parent as l_parent then
				if a_content.count > 0 and then a_content.item (a_content.count) = ' ' then
					create l_string.make_from_string (a_content)
					from i := l_string.count
					until i < 0 or else l_string.item (i) /= ' '
					loop
						i := i - 1
					end
					l_string.remove_tail (l_string.count - i)
					l_parent.set_description (l_string)
				else
					l_parent.set_description (a_content)
				end
			else
				check unexpected_parent_type: false end
			end
		end


end
