note
	description: "Summary description for {EAC_XML_ROOT}."
	author: "Howard Thomson"
	Extended_description: "[
		This class is the root class for all elements in the XML uncompressed
		data file for GnuCash -- as at 14-April-2020 ...
		
		After completing the data import, it should have one child: a gnc:book
	]"

class
	EAC_XML_GNC_V2

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content,
			set_target
		end

feature

	xml_tag: STRING = "gnc-v2"

	set_target (a_target: detachable XML_GNUCASH_CALLBACKS)
		do
			Precursor (a_target)
			if a_target /= Void then
				check a_target.xml_stack.count = 1 end
			end
		end

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
			if attached {XML_GNUCASH_CALLBACKS} target as l_callbacks then
				l_callbacks.set_has_gnc_v2_element
			end
		end

	process_content (a_content: READABLE_STRING_32)
		do
--			print ("gnc-v2: Ignoring content: ")
--			print (a_content)
--			io.put_new_line
		end

end
