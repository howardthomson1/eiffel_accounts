note
	description: "Summary description for {EAC_XML_FACTORY}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_FACTORY

inherit
	EAC_REPORTER

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			make_factory
		end

	make_factory
		do
			create tag_table.make (20000)

			tag_table.extend (agent create_gnc_v2,				"gnc-v2")

			tag_table.extend (agent create_gnc_book,					"gnc:book")
			tag_table.extend (agent create_gnc_commodity, 				"gnc:commodity")
			tag_table.extend (agent create_gnc_count_data, 				"gnc:count-data")
			tag_table.extend (agent create_gnc_recurrence, 				"gnc:recurrence")
			tag_table.extend (agent create_gnc_schedxaction, 			"gnc:schedxaction")
			tag_table.extend (agent create_gnc_template_transactions, 	"gnc:template-transactions")
			tag_table.extend (agent create_gnc_transaction, 			"gnc:transaction")

			tag_table.extend (agent create_book_id, 			"book:id")
			tag_table.extend (agent create_book_slots, 			"book:slots")

			tag_table.extend (agent create_slot, 				"slot")
			tag_table.extend (agent create_slot_key, 			"slot:key")
			tag_table.extend (agent create_slot_value, 			"slot:value")

			tag_table.extend (agent create_cmdty_fraction, 		"cmdty:fraction")
			tag_table.extend (agent create_cmdty_get_quotes, 	"cmdty:get_quotes")
			tag_table.extend (agent create_cmdty_id, 			"cmdty:id")
			tag_table.extend (agent create_cmdty_name, 			"cmdty:name")
			tag_table.extend (agent create_cmdty_quote_source, 	"cmdty:quote_source")
			tag_table.extend (agent create_cmdty_quote_tz, 		"cmdty:quote_tz")
			tag_table.extend (agent create_cmdty_space, 		"cmdty:space")
			tag_table.extend (agent create_cmdty_xcode, 		"cmdty:xcode")

			tag_table.extend (agent create_gnc_account, 		"gnc:account")

			tag_table.extend (agent create_act_code, 			"act:code")
			tag_table.extend (agent create_act_commodity, 		"act:commodity")
			tag_table.extend (agent create_act_commodity_scu, 	"act:commodity-scu")
			tag_table.extend (agent create_act_description, 	"act:description")
			tag_table.extend (agent create_act_id, 				"act:id")
			tag_table.extend (agent create_act_name, 			"act:name")
			tag_table.extend (agent create_act_non_standard_scu,"act:non-standard-scu")
			tag_table.extend (agent create_act_parent, 			"act:parent")
			tag_table.extend (agent create_act_slots, 			"act:slots")
			tag_table.extend (agent create_act_type, 			"act:type")

			tag_table.extend (agent create_trn_currency, 		"trn:currency")
			tag_table.extend (agent create_trn_date_entered, 	"trn:date-entered")
			tag_table.extend (agent create_trn_date_posted, 	"trn:date-posted")
			tag_table.extend (agent create_trn_description, 	"trn:description")
			tag_table.extend (agent create_trn_id, 				"trn:id")
			tag_table.extend (agent create_trn_num, 			"trn:num")
			tag_table.extend (agent create_trn_slots, 			"trn:slots")
			tag_table.extend (agent create_trn_split, 			"trn:split")
			tag_table.extend (agent create_trn_splits, 			"trn:splits")

			tag_table.extend (agent create_ts_date, 			"ts:date")

			tag_table.extend (agent create_gdate, 				"gdate")

			tag_table.extend (agent create_recurrence_mult, 		"recurrence:mult")
			tag_table.extend (agent create_recurrence_period_type, 	"recurrence:period_type")
			tag_table.extend (agent create_recurrence_start, 		"recurrence:start")
			tag_table.extend (agent create_recurrence_weekend_adj, 	"recurrence:weekend_adj")

			tag_table.extend (agent create_split_account, 			"split:account")
			tag_table.extend (agent create_split_action, 			"split:action")
			tag_table.extend (agent create_split_id, 				"split:id")
			tag_table.extend (agent create_split_memo, 				"split:memo")
			tag_table.extend (agent create_split_quantity, 			"split:quantity")
			tag_table.extend (agent create_split_reconciled_state, 	"split:reconciled-state")
			tag_table.extend (agent create_split_slots, 			"split:slots")
			tag_table.extend (agent create_split_value, 			"split:value")

			tag_table.extend (agent create_sx_advanceCreateDays, 	"sx:advanceCreateDays")
			tag_table.extend (agent create_sx_advanceRemindDays, 	"sx:advanceRemindDays")
			tag_table.extend (agent create_sx_autoCreate, 			"sx:autoCreate")
			tag_table.extend (agent create_sx_autoCreateNotify, 	"sx:autoCreateNotify")
			tag_table.extend (agent create_sx_enabled, 				"sx:enabled")
			tag_table.extend (agent create_sx_id, 					"sx:id")
			tag_table.extend (agent create_sx_instanceCount, 		"sx:instanceCount")
			tag_table.extend (agent create_sx_last, 				"sx:last")
			tag_table.extend (agent create_sx_name, 				"sx:name")
			tag_table.extend (agent create_sx_schedule, 			"sx:schedule")
			tag_table.extend (agent create_sx_start, 				"sx:start")
			tag_table.extend (agent create_sx_templ_acct, 			"sx:templ-acct")
		end

	create_gnc_v2: EAC_XML_OBJECT					do create {EAC_XML_GNC_V2} Result end

	create_book_id: EAC_XML_OBJECT 					do create {EAC_XML_BOOK_ID} Result end
	create_book_slots: EAC_XML_OBJECT 				do create {EAC_XML_BOOK_SLOTS} Result end

	create_slot: EAC_XML_OBJECT 					do create {EAC_XML_SLOT} Result end
	create_slot_key: EAC_XML_OBJECT 				do create {EAC_XML_SLOT_KEY} Result end
	create_slot_value: EAC_XML_OBJECT 				do create {EAC_XML_SLOT_VALUE} Result end

	create_cmdty_fraction: EAC_XML_OBJECT 			do create {EAC_XML_CMDTY_FRACTION} Result end
	create_cmdty_get_quotes: EAC_XML_OBJECT 		do create {EAC_XML_CMDTY_GET_QUOTES} Result end
	create_cmdty_id: EAC_XML_OBJECT 				do create {EAC_XML_CMDTY_ID} Result end
	create_cmdty_name: EAC_XML_OBJECT 				do create {EAC_XML_CMDTY_NAME} Result end
	create_cmdty_quote_source: EAC_XML_OBJECT 		do create {EAC_XML_CMDTY_QUOTE_SOURCE} Result end
	create_cmdty_quote_tz: EAC_XML_OBJECT 			do create {EAC_XML_CMDTY_QUOTE_TZ} Result end
	create_cmdty_space: EAC_XML_OBJECT 				do create {EAC_XML_CMDTY_SPACE} Result end
	create_cmdty_xcode: EAC_XML_OBJECT 				do create {EAC_XML_CMDTY_XCODE} Result end

	create_gnc_account: EAC_XML_OBJECT 				do create {EAC_XML_GNC_ACCOUNT} Result end

	create_act_code: EAC_XML_OBJECT 				do create {EAC_XML_ACT_CODE} Result end
	create_act_commodity: EAC_XML_OBJECT 			do create {EAC_XML_ACT_COMMODITY} Result end
	create_act_commodity_scu: EAC_XML_OBJECT 		do create {EAC_XML_ACT_COMMODITY_SCU} Result end
	create_act_description: EAC_XML_OBJECT 			do create {EAC_XML_ACT_DESCRIPTION} Result end
	create_act_id: EAC_XML_OBJECT 					do create {EAC_XML_ACT_ID} Result end
	create_act_name: EAC_XML_OBJECT 				do create {EAC_XML_ACT_NAME} Result end
	create_act_non_standard_scu: EAC_XML_OBJECT 	do create {EAC_XML_ACT_NON_STANDARD_SCU} Result end
	create_act_parent: EAC_XML_OBJECT 				do create {EAC_XML_ACT_PARENT} Result end
	create_act_slots: EAC_XML_OBJECT 				do create {EAC_XML_ACT_SLOTS} Result end
	create_act_type: EAC_XML_OBJECT 				do create {EAC_XML_ACT_TYPE} Result end

	create_trn_currency: EAC_XML_OBJECT 			do create {EAC_XML_TRN_CURRENCY} Result end
	create_trn_date_entered: EAC_XML_OBJECT 		do create {EAC_XML_TRN_DATE_ENTERED} Result end
	create_trn_date_posted: EAC_XML_OBJECT 			do create {EAC_XML_TRN_DATE_POSTED} Result end
	create_trn_description: EAC_XML_OBJECT 			do create {EAC_XML_TRN_DESCRIPTION} Result end
	create_trn_id: EAC_XML_OBJECT 					do create {EAC_XML_TRN_ID} Result end
	create_trn_num: EAC_XML_OBJECT 					do create {EAC_XML_TRN_NUM} Result end
	create_trn_slots: EAC_XML_OBJECT 				do create {EAC_XML_TRN_SLOTS} Result end
	create_trn_split: EAC_XML_OBJECT 				do create {EAC_XML_TRN_SPLIT} Result end
	create_trn_splits: EAC_XML_OBJECT 				do create {EAC_XML_TRN_SPLITS} Result end

	create_gdate: EAC_XML_OBJECT 					do create {EAC_XML_GDATE} Result end

	create_gnc_book: EAC_XML_OBJECT 				do create {EAC_XML_GNC_BOOK} Result end
	create_gnc_commodity: EAC_XML_OBJECT 			do create {EAC_XML_GNC_COMMODITY} Result end
	create_gnc_count_data: EAC_XML_OBJECT 			do create {EAC_XML_GNC_COUNT_DATA} Result end
	create_gnc_recurrence: EAC_XML_OBJECT 			do create {EAC_XML_GNC_RECURRENCE} Result end
	create_gnc_schedxaction: EAC_XML_OBJECT 		do create {EAC_XML_GNC_SCHEDXACTION} Result end
	create_gnc_template_transactions:EAC_XML_OBJECT do create {EAC_XML_GNC_TEMPLATE_TRANSACTIONS} Result end
	create_gnc_transaction: EAC_XML_OBJECT 			do create {EAC_XML_GNC_TRANSACTION} Result end

	create_recurrence_mult: EAC_XML_OBJECT 			do create {EAC_XML_RECURRENCE_MULT} Result end
	create_recurrence_period_type: EAC_XML_OBJECT 	do create {EAC_XML_RECURRENCE_PERIOD_TYPE} Result end
	create_recurrence_start: EAC_XML_OBJECT 		do create {EAC_XML_RECURRENCE_START} Result end
	create_recurrence_weekend_adj: EAC_XML_OBJECT 	do create {EAC_XML_RECURRENCE_WEEKEND_ADJ} Result end

	create_split_account: EAC_XML_OBJECT 			do create {EAC_XML_SPLIT_ACCOUNT} Result end
	create_split_action: EAC_XML_OBJECT 			do create {EAC_XML_SPLIT_ACTION} Result end
	create_split_id: EAC_XML_OBJECT 				do create {EAC_XML_SPLIT_ID} Result end
	create_split_memo: EAC_XML_OBJECT 				do create {EAC_XML_SPLIT_MEMO} Result end
	create_split_quantity: EAC_XML_OBJECT 			do create {EAC_XML_SPLIT_QUANTITY} Result end
	create_split_reconciled_state: EAC_XML_OBJECT 	do create {EAC_XML_SPLIT_RECONCILED_STATE} Result end
	create_split_slots: EAC_XML_OBJECT 				do create {EAC_XML_SPLIT_SLOTS} Result end
	create_split_value: EAC_XML_OBJECT 				do create {EAC_XML_SPLIT_VALUE} Result end

	create_sx_advanceCreateDays: EAC_XML_OBJECT 	do create {EAC_XML_SX_ADVANCE_CREATE_DAYS} Result end
	create_sx_advanceRemindDays: EAC_XML_OBJECT 	do create {EAC_XML_SX_ADVANCE_REMIND_DAYS} Result end
	create_sx_autoCreate: EAC_XML_OBJECT 			do create {EAC_XML_SX_AUTO_CREATE} Result end
	create_sx_autoCreateNotify: EAC_XML_OBJECT 		do create {EAC_XML_SX_AUTO_CREATE_NOTIFY} Result end
	create_sx_enabled: EAC_XML_OBJECT 				do create {EAC_XML_SX_ENABLED} Result end
	create_sx_id: EAC_XML_OBJECT 					do create {EAC_XML_SX_ID} Result end
	create_sx_instanceCount: EAC_XML_OBJECT 		do create {EAC_XML_SX_INSTANCE_COUNT} Result end
	create_sx_last: EAC_XML_OBJECT 					do create {EAC_XML_SX_LAST} Result end
	create_sx_name: EAC_XML_OBJECT 					do create {EAC_XML_SX_NAME} Result end
	create_sx_schedule: EAC_XML_OBJECT 				do create {EAC_XML_SX_SCHEDULE} Result end
	create_sx_start: EAC_XML_OBJECT 				do create {EAC_XML_SX_START} Result end
	create_sx_templ_acct: EAC_XML_OBJECT 			do create {EAC_XML_SX_TEMPL_ACCT} Result end

	create_ts_date: EAC_XML_OBJECT 					do create {EAC_XML_TS_DATE} Result end

	tag_table: HASH_TABLE [	FUNCTION [detachable TUPLE, EAC_XML_OBJECT], STRING ]

feature

	new_object (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32): EAC_XML_OBJECT
		local
			l_str: STRING_32
		do
			create l_str.make_from_string (a_local_part)
			if a_prefix /= Void then
				l_str.prepend_character (':')
				l_str.prepend (a_prefix)
			end
			tag_table.search (l_str)
			if tag_table.found then
				if attached tag_table.found_item as l_agent then
					Result := l_agent.item (Void)
				else
						-- This 'cannot happen' as an entry has been found in the table,
						-- and all entries in the table have non-Void associated items
					check false end
					create {EAC_XML_OTHER} Result
				end
			else
					-- This also 'cannot happen' as all tags in my GnuCash data file
					-- are provided for [I hope !]
				check false end
				create {EAC_XML_OTHER} Result
			end
		end

end
