note
	description: "Summary description for {EAC_XML_BOOK_ID}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_BOOK_ID

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_guid_content
		end


feature

	xml_tag: STRING = "book:id"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_guid_content (a_content: READABLE_STRING_32)
		do
			-- Discard ...
		end



end
