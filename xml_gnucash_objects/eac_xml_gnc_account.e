note
	description: "Summary description for {EAC_XML_ACCOUNT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_GNC_ACCOUNT

inherit
	EAC_XML_OBJECT
		redefine
			default_create,
			process_attribute,
			report_callback
		end

	EAC_REPORTER
		undefine
			default_create
		end

feature

	default_create
		do
			create act_slots.make (10)
			Precursor {EAC_XML_OBJECT}
			create tree_item
		end

	tree_item: EV_TREE_ITEM

	xml_tag: STRING = "gnc:account"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Account attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

feature -- GnuCash attributes

	act_code: detachable STRING
	act_commodity: detachable EAC_XML_ACT_COMMODITY
	act_commodity_scu: detachable EAC_XML_ACT_COMMODITY_SCU
	act_description: detachable STRING
	act_id: detachable STRING
	act_name: detachable STRING
	act_non_standard_scu: detachable STRING
	act_parent: detachable STRING
	act_slots: HASH_TABLE [ EAC_XML_SLOT, STRING ]
	act_type: detachable STRING

feature {EAC_XML_OBJECT}

	set_act_code (a_code: STRING)
		do
			act_code := a_code
		end

	set_act_commodity (a_commodity: EAC_XML_ACT_COMMODITY)
		do
			act_commodity := a_commodity
		end

	set_act_commodity_scu (a_commodity_scu: EAC_XML_ACT_COMMODITY_SCU)
		do
			act_commodity_scu := a_commodity_scu
		end

	set_description (a_description: STRING)
		do
			act_description := a_description
		end

	set_act_id (a_guid: STRING)
		do
			act_id := a_guid
		end

	set_name (a_name: STRING)
		do
			act_name := a_name
		end

	set_act_parent (a_parent: STRING)
		do
			act_parent := a_parent
		end

feature

	set_act_type (a_type: STRING)
		do
			act_type := a_type
			if attached a_type as t then
				check t.is_equal ("ROOT")
				or else t.is_equal ("ASSET")
				or else t.is_equal ("BANK")
				or else t.is_equal ("CREDIT")
				or else t.is_equal ("EXPENSE")
				or else t.is_equal ("INCOME")
				or else t.is_equal ("LIABILITY")
				or else t.is_equal ("EQUITY")
				or else t.is_equal ("CASH")
				or else t.is_equal ("MUTUAL")
				or else t.is_equal ("PAYABLE")
				or else t.is_equal ("RECEIVABLE")
				or else t.is_equal ("STOCK")
				or else t.is_equal ("TRADING")
				end
				if t.is_equal ("ROOT") then
					is_root := True
				end
			end
		end

	is_root: BOOLEAN

	report_callback (a_callbacks: XML_GNUCASH_CALLBACKS)
		do
			a_callbacks.report_account (Current)
		end

	balance: INTEGER_64

	cumulative_balance: INTEGER_64

	update_balance (a_value: INTEGER_64)
		do
			balance := balance + a_value
			cumulative_balance := cumulative_balance + a_value
		end

	update_cumulative_balance (a_value: INTEGER_64)
		do
			cumulative_balance := cumulative_balance + a_value
		end


end
