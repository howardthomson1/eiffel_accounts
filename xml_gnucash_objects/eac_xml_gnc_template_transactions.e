note
	description: "Summary description for {EAC_XML_GNC_TEMPLATE_TRANSACTIONS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_GNC_TEMPLATE_TRANSACTIONS

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			set_target
		end


feature
	xml_tag: STRING = "gnc:template-transactions"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	set_target (a_target: detachable XML_GNUCASH_CALLBACKS)
		do
			target := a_target
			if attached target as l_target then
				l_target.start_template_transactions
			end
		end

end
