note
	description: "Summary description for {EAC_XML_TRN_DESCRIPTION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_TRN_DESCRIPTION

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end

feature

	xml_tag: STRING = "trn:description"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_content (a_content: READABLE_STRING_32)
		do
			if attached {EAC_XML_GNC_TRANSACTION} parent as l_parent then
				l_parent.set_description (a_content)
			else
				check unexpected_parent_type: false end
			end
		end

end
