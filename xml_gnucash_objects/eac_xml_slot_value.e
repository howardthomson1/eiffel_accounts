note
	description: "Summary description for {EAC_XML_SLOT_VALUE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_SLOT_VALUE

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_string_content,
			process_numeric_content,
			process_integer_content,
			process_guid_content
		end

feature

	xml_tag: STRING = "slot:value"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
		--	print ("Processing Book attribute ...%N")
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_string_content (a_content: READABLE_STRING_32)
		do
			if attached {EAC_XML_SLOT} parent as l_parent then
				l_parent.set_value (a_content)
			else
				check unexpected_parent_type: false end
			end
		end

	process_numeric_content (a_content: READABLE_STRING_32)
		do
			if attached {EAC_XML_SLOT} parent as l_parent then
				l_parent.set_value (a_content)
			else
				check unexpected_parent_type: false end
			end
		end

	process_integer_content (a_content: READABLE_STRING_32)
		do
			if attached {EAC_XML_SLOT} parent as l_parent then
				l_parent.set_value (a_content)
			else
				check unexpected_parent_type: false end
			end
		end

	process_guid_content (a_content: READABLE_STRING_32)
		do
			if attached {EAC_XML_SLOT} parent as l_parent then
				l_parent.set_value (a_content)
			else
				check unexpected_parent_type: false end
			end
		end



end
