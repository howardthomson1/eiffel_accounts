note
	description: "Summary description for {EAC_XML_TRN_DATE_ENTERED}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_TRN_DATE_ENTERED

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content
		end

feature

	xml_tag: STRING = "trn:date-entered"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		do
			Precursor (a_namespace, a_prefix, a_local_part, a_value)
		end

	process_content (a_content: READABLE_STRING_32)
		do
		end


end
