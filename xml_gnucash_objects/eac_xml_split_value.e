note
	description: "Summary description for {EAC_XML_SPLIT_VALUE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_XML_SPLIT_VALUE

inherit
	EAC_XML_OBJECT
		redefine
			process_attribute,
			process_content,
			report_callback
		end

feature

	xml_tag: STRING = "split:value"

	process_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
		local
			l_str: STRING
			l_value: INTEGER_64
		do
		--	print ("Processing Book attribute ...%N")
		--	Precursor (a_namespace, a_prefix, a_local_part, a_value)
			if not attached {EAC_XML_TRN_SPLIT} parent as l_parent then
				check false end
			end
			check a_value.count > 4 and then a_value.substring (a_value.count -3, a_value.count).is_equal ("/100") end
			l_str := a_value.substring (1, a_value.count - 4)
			l_value := l_str.to_integer_64

--			print ("%Nsplit:value -- ")
--			print (l_value.out)
--			io.put_new_line
		end

	process_content (a_content: READABLE_STRING_32)
		local
			l_str: STRING
			l_value: INTEGER_64
		do
			if not attached {EAC_XML_TRN_SPLIT} parent as l_parent then
				check false end
			end
			check a_content.count > 4 and then a_content.substring (a_content.count -3, a_content.count).is_equal ("/100") end
			l_str := a_content.substring (1, a_content.count - 4)
			value := l_str.to_integer_64
		end

	value: INTEGER_64

	report_callback (a_callbacks: XML_GNUCASH_CALLBACKS)
		do
			if attached a_callbacks as l_target then
				l_target.update_totals (value)
			end
			if attached {EAC_XML_TRN_SPLIT} parent as l_parent then
				l_parent.set_value (value)
			end
		end


end
