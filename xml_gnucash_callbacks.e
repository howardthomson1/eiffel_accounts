note
	description: "Summary description for {XML_GNUCASH_CALLBACKS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	XML_GNUCASH_CALLBACKS

inherit
	XML_CALLBACKS
--	EAC_SHARED_DATA
	EAC_REPORTER

create
	make

feature {NONE} -- Initialization

	make (a_importer: XML_IMPORTER_GNUCASH)
			-- Initialization for `Current'.
		do
			create xml_tree.make
			create xml_stack.make
			importer := a_importer
		end

	importer: XML_IMPORTER_GNUCASH


feature -- Eiffel object tree resulting from the XML parse

	xml_tree: EAC_XML_FACTORY

	xml_stack: LINKED_STACK [ EAC_XML_OBJECT ]

	debits_total, credits_total: INTEGER_64

	update_totals (a_value: INTEGER_64)
		do
			if not template_transactions_started then
				if a_value < 0 then
					debits_total := debits_total - a_value
				else
					credits_total := credits_total + a_value
				end
			end
		end

feature -- Document

	on_start
			-- Called when parsing starts.
		do
			-- Nothing needed here ... ?
		end

	on_finish
			-- Called when parsing finished.
		do
				-- Nothing needed here ... ?
--			report ("%NBalance: ");
--			report (debits_total.out);
--			report ("  ")
--			report (credits_total.out)
--			report ("%N")
--			check debits_equals_credits: debits_total = credits_total end
		end

	on_xml_declaration (a_version: READABLE_STRING_32; an_encoding: detachable READABLE_STRING_32; a_standalone: BOOLEAN)
			-- XML declaration.
		require else
			a_version_not_void: a_version /= Void
			a_version_not_empty: a_version.count > 0
			a_version_is_valid: a_version.is_valid_as_string_8
			an_encoding_is_valid: an_encoding /= Void implies an_encoding.is_valid_as_string_8
		do
			is_xml_document := True
		end

	is_xml_document: BOOLEAN


	has_gnc_v2_element: BOOLEAN

	set_has_gnc_v2_element
		do
			has_gnc_v2_element := True
		end

feature -- Errors

	on_error (a_message: READABLE_STRING_32)
			-- Event producer detected an error.
		require else
			not_void: a_message /= Void
		do
			report ("on_error ...%N")
--			print_nl_line_number
		end

feature -- Meta

	on_processing_instruction (a_name: READABLE_STRING_32; a_content: READABLE_STRING_32)
			-- Processing instruction.
			--| See http://en.wikipedia.org/wiki/Processing_instruction
		require else
			name_not_void: a_name /= Void
			content_not_void: a_content /= Void
		do
			-- Nothing needed here ...
		end

	on_comment (a_content: READABLE_STRING_32)
			-- Processing a comment.
			-- Atomic: single comment produces single event
		require else
			a_content_not_void: a_content /= Void
		do
			-- Nothing needed here ...
		end

feature -- Tag

	tag_count: INTEGER

	on_start_tag (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32)
			-- Start of start tag.
		require else
			unresolved_namespace_is_void: has_resolved_namespaces implies a_namespace /= Void
			local_part: is_local_part (a_local_part)
		local
			l_object: EAC_XML_OBJECT
			i: INTEGER
		do
			l_object := xml_tree.new_object (a_namespace, a_prefix, a_local_part)
			if xml_stack.count > 0 and then attached xml_stack.item as l_item then
				l_object.set_parent (l_item)
			end
			xml_stack.extend (l_object)
			l_object.set_link (Current)
			l_object.set_target (Current)
		end

	on_attribute (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32; a_value: READABLE_STRING_32)
			-- Start of attribute.
		require else
			unresolved_namespace_is_void: has_resolved_namespaces implies a_namespace /= Void
			local_part: is_local_part (a_local_part)
			a_value_not_void: a_value /= Void
		local
			l_object: EAC_XML_OBJECT
		do
			xml_stack.item.process_attribute (a_namespace, a_prefix, a_local_part, a_value)
		end

	on_start_tag_finish
			-- End of start tag.
		local
			l_str: STRING
		do
		end

	on_end_tag (a_namespace: detachable READABLE_STRING_32; a_prefix: detachable READABLE_STRING_32; a_local_part: READABLE_STRING_32)
			-- End tag.
		require else
			unresolved_namespace_is_void: has_resolved_namespaces implies a_namespace /= Void
			local_part: is_local_part (a_local_part)
		do
			xml_stack.item.report_callback (Current)
			xml_stack.remove
		end

feature -- Content

	on_content (a_content: READABLE_STRING_32)
			-- Text content.
			-- NOT atomic: two on_content events may follow each other
			-- without a markup event in between.
			--| this should not occur, but I leave this comment just in case
		require else
			not_void: a_content /= Void
			not_empty: a_content.count >= 0
		do
			if not xml_stack.is_empty then
				xml_stack.item.process_content (a_content)
			else
--				check content_is_whitespace: a_content.is_whitespace end
			end
		end

	report_account (an_account: EAC_XML_GNC_ACCOUNT)
		do
			if not template_transactions_started then
				if attached an_account.act_id as l_id then
					importer.xml_accounts.extend (an_account, l_id)
				else
					check false end
				end
			end
		end

	report_transaction (a_transaction: EAC_XML_GNC_TRANSACTION)
		do
			if not template_transactions_started then
				if attached a_transaction.id as l_id then
					importer.xml_transactions.extend (a_transaction, l_id)
				else
					check false end
				end
			end
		end

feature -- Change processing after template transactions tag

	template_transactions_started: BOOLEAN

	start_template_transactions
		do
			template_transactions_started := True
		end

end
