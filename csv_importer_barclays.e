note
	description: "Summary description for {IMPORT_BARCLAYS_CSV}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

	remind: "[
		SHA1 signature from the combination of Date, Account, Amount, Subcategory and Memo
		can be identical, for Barclays CSV import, examples being:
			1/ Two transfers on the same day, for the same amount from savings to current
			2/ Two payments to SouthWestRail [Me and janice] on the same day
			3/ Two payments to BUPA for health check [Me and Janice]
			4/ Two payments to Companies House [GW&AE and AE&DA]
	]"

	TODO: "[
		Check for/recover from failure to open or read from file
		Process contents into new class object for each CSV line
	]"

	DONE: "[
		Move scanner setup to once per file, instead of once per line !
	]"

class
	CSV_IMPORTER_BARCLAYS

create
	make

feature

	filename: STRING

	scanner: REGULAR_EXPRESSION

	make (a_filename: STRING)
		do
			filename := a_filename
			create scanner
			scanner.compile ("^([^,]*),([^,]*),([^,]*),([^,]*),([^,]*),(.*)")
		end

	scan
		local
			file: PLAIN_TEXT_FILE
			line: STRING
			i: INTEGER
		do
			create file.make_open_read (filename)
			from
				file.read_line
				line := file.last_string
			until
				file.end_of_file
			loop
				if line.count > 0 and then line.item (line.count) = '%R' then
					line.remove_tail (1)
				end
				print (line)
				io.put_new_line
				process_line (line)
				file.read_line
				i := i + 1
			end
			file.close
		end

	process_line (a_string: STRING)
		local

			i: NATURAL
			s: STRING
			d: DATE
			d_format: STRING
			l_signature: SHA1
		do
			create d.make_now
			create l_signature.make
			d_format := once "[0]dd/[0]mm/yyyy"

			scanner.match (a_string)
			if scanner.has_matched then
				print (once "0: %""); print (scanner.captured_substring (0)); print (once "%""); io.put_new_line
				print (scanner.match_count); io.put_new_line
				l_signature.reset
				from
					i := 1
				until
					i >= scanner.match_count
				loop
					print (i.out); print (once ": %"")
					s := scanner.captured_substring (i)
					print (s)
					print (once "%"")
					if i > 1 then
						l_signature.update_from_string (s)
					end
					if i = 2 then
							-- Check validity of date field
						if d.date_valid (s, d_format) then
							d.make_from_string (s, d_format)
							print (" Date converted as: ")
							print (d.formatted_out (d_format))
						else
							print (" INVALID DATE!")
						end
					end
					io.put_new_line
					i := i + 1
				end
				print ("Signature: "); print (l_signature.digest_as_string)
				print (": "); print (a_string)
				io.put_new_line
			end
		end
end
