note
	description: "Summary description for {EAC_ACCOUNT}."
	author: "Howard Thomson"
	date: "18-April-2020"

	notes: "[
		Assets - Liabilities = Equity
		Assets = Liabilities + Equity
				
		Asset		-ve [We have paid for ...]
		Liability	+ve [We owe someone ...]
		Equity		+ve {not in 'Negative Equity']
		Income		+ve [We benefit from Income]
		Expense		-ve [We have paid out ...]

		Account deletion, multi stage requirement
			Lock the account, to prevent new postings
			Re-assign existing postings ...
			Delete the account permissible when no postings remain
			Add a 'forwarding' link for future re-assigned postings

		Need means of distinguishing stored attributes from transient ones ...
		
		Account attribute: Locked, meaning not open for new transactions
	]"

	TODO: "[
		Add an import_tag, to potentially enable selection and removal of imported accounts, subject to
		there being no additional postings since import.
		
		Add history for changes to the account, which indicates who made the change, and when
		Use archived account records ?
	]"

class
	EAC_ACCOUNT

inherit
	ANY
		redefine
			default_create
		end

create
	default_create,
	make_for_import

feature

	default_create
		do
			id := {UUID_GENERATOR}.generate_uuid.string
		end

	make_for_import (an_id: STRING)
		require
			not_void: an_id /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (an_id)
		do
			id := an_id
		end


feature -- Attributes, stored

	id: STRING
		-- GUID for this account

	transaction_no: INTEGER_64
		-- To be removed ?

	name: detachable STRING
		-- Textual name for this account

	type: INTEGER
		-- Asset, Liability, Income, Expense, Equity ...

	parent_id: detachable STRING
		-- GUID for this accounts parent

	description: detachable STRING
		-- Extended annotation for this account

	transaction_id: INTEGER_64
		-- ID of the transaction that created, or last updated this item
		-- Zero implies newly created locally, not yet applied to the DB

feature

	attached_name: STRING
		do
			if attached name as n then
				Result := n
			else
				Result := " "
			end
		end

	attached_parent_id: STRING
		do
			if attached parent_id as p then
				Result := p
			else
				Result := " "
			end
		end

	attached_description: STRING
		do
			if attached description as d then
				Result := d
			else
				Result := " "
			end

		end

feature -- Attribute set status

	id_is_set: BOOLEAN
		-- Has set_id been called ?

	name_is_set: BOOLEAN
		-- Has set_name been called ?

	type_is_set: BOOLEAN
			-- Has set_type been called ?
		do
			Result := type /= 0
		end

	parent_id_is_set: BOOLEAN

	description_is_set: BOOLEAN

feature -- Attribute setting

	set_name (a_name: detachable STRING)
		do
			name := a_name
			name_is_set := True
		end

	set_type (a_type: INTEGER)
		require
			is_valid_type: valid_type (a_type)
		do
			type := a_type
		end

	set_parent_id (a_uuid: detachable STRING)
		require
			id_not_void: a_uuid /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			parent_id := a_uuid
			parent_id_is_set := True
		end

	set_description (a_description: detachable STRING)
		do
			description := a_description
			description_is_set := True
		end

	set_transaction_id (a_transaction_id: like transaction_id)
		do
			transaction_id := a_transaction_id
		end

feature -- Validity for import

	attributes_set_for_import: BOOLEAN
		do
			Result := id_is_set
			and then name_is_set
			and then type_is_set
			and then
				type /= Type_root implies parent_id_is_set
		end

feature -- Account types, derived from GnuCash

	Type_Root		: INTEGER = 1
	Type_Asset		: INTEGER = 2
	Type_Liability	: INTEGER = 3
	Type_Income		: INTEGER = 4
	Type_Expense	: INTEGER = 5
	Type_Bank		: INTEGER = 6
	Type_Credit		: INTEGER = 7
	Type_Equity		: INTEGER = 8
	Type_Cash		: INTEGER = 9
	Type_Mutual		: INTEGER = 10
	Type_Payable	: INTEGER = 11
	Type_Receivable	: INTEGER = 12
	Type_Stock		: INTEGER = 13
	Type_Trading	: INTEGER = 14

	valid_type (a_type: INTEGER): BOOLEAN
		do
			Result := a_type = Type_Root
			or else a_type = Type_Asset
			or else a_type = Type_Liability
			or else a_type = Type_Income
			or else a_type = Type_Expense
			or else a_type = Type_Bank
			or else a_type = Type_Credit
			or else a_type = Type_Equity
			or else a_type = Type_Cash
			or else a_type = Type_Mutual
			or else a_type = Type_Payable
			or else a_type = Type_Receivable
			or else a_type = Type_Stock
			or else a_type = Type_Trading
		end

	type_as_string: STRING
		require
			valid_attributes: attributes_set_for_import
		do
			inspect type
			when Type_Root 			then Result := "Root"		-- ????	
			when Type_Asset 		then Result := "Asset"
			when Type_Liability 	then Result := "Liability"
			when Type_Income 		then Result := "Income"
			when Type_Expense 		then Result := "Expense"
			when Type_Bank 			then Result := "Bank"
			when Type_Credit 		then Result := "Credit"
			when Type_Equity 		then Result := "Equity"
			when Type_Cash 			then Result := "Cash"
			when Type_Mutual 		then Result := "Mutual"
			when Type_Payable 		then Result := "Payable"
			when Type_Receivable 	then Result := "Receivable"
			when Type_Stock 		then Result := "Stock"
			when Type_Trading 		then Result := "Trading"
			else
				check false end
				Result := "Undefined"
			end
		end

feature

	is_asset: BOOLEAN
		do
			Result := type = Type_Asset
			or else type = Type_Bank
			or else type = Type_Cash
			or else type = Type_Mutual
			or else type = Type_Receivable
			or else type = Type_Stock
			or else type = Type_Trading
		end

	is_liability: BOOLEAN
		do
			Result := type = Type_Liability
			or else type = Type_Credit
			or else type = Type_Equity
			or else type = Type_Cash
			or else type = Type_Mutual
			or else type = Type_Payable
		end

	is_equity: BOOLEAN
		do
			Result := type = Type_Equity
		end

	is_income: BOOLEAN
		do
			Result := type = Type_Income
		end

	is_expense: BOOLEAN
		do
			Result := type = Type_Expense
		end

feature -- Display integration

	is_root: BOOLEAN
		do
			Result := type = Type_root
		end

	grid_x: INTEGER
	grid_y: INTEGER

	set_grid_xy (a_x, a_y: INTEGER)
		do
			grid_x := a_x
			grid_y := a_y
		end

feature {NONE} -- NOT USED

-- These two attributes are not used, but need a means of migrating stored entries
-- which have them to a later set of attributes of this class ...

	balance: INTEGER_64

	cumulative_balance: INTEGER_64

feature

	current_balance (a_include_descendants: BOOLEAN): INTEGER_64
			--TODO: need a cached access for this value
		local
			l_transactions: HASH_TABLE [EAC_TRANSACTION, STRING]
			l_item: EAC_TRANSACTION
			l_id: STRING
		do
			l_transactions := eac_data.eac_transactions
			if a_include_descendants then
				check False end --TODO...
			else
				from l_transactions.start
				until l_transactions.after
				loop
					l_item := l_transactions.item_for_iteration
					if attached l_item.credit_account as l_cr
					and then id.is_equal (l_cr)
					then
						Result := Result + l_item.value
					end
					if attached l_item.debit_account as l_dr
					and then id.is_equal (l_dr)
					then
						Result := Result - l_item.value
					end
					l_transactions.forth
				end
			end
		end

	key_string: STRING
			-- Generate key based on Current and all ancestors thereof
		local
			l_accounts: like eac_data.eac_accounts
			l_account: EAC_ACCOUNT
			done: BOOLEAN
		do
			create Result.make_empty
			l_account := Current
			from
				l_accounts := eac_data.eac_accounts
			until
				done
			loop
				if attached l_account as l then
					Result.prepend (l.attached_name)
					l_accounts.search (l.attached_parent_id)
					if l_accounts.found then
						Result.prepend (" :: ")
						l_account := l_accounts.found_item
					else
						done := True
					end
				end
			end
		end

feature {NONE}

	eac_data: EAC_DATA
		once
			Result := (create {EAC_SHARED_DATA}).eac_data
		end

invariant

end
