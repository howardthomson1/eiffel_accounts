note
	description: "Summary description for {EAC_STORED_DATA}."
	author: "Howard Thomson"
	date: "5-June-2020"
	extended_description: "[
		This is [shared] the repository for EAC data, which will become the shared
		cache for EAC data backed by Postgresql at a later date.
	]"
	TODO: "[
		Plan for multiple identified users
		Plan for more than one set of accounts
	]"

class
	EAC_STORED_DATA

inherit
	SED_STORABLE_FACILITIES

create
	make

feature -- Creation

	make
		local
			l_comparator: EAC_ACCOUNT_COMPARATOR
		do
			create l_comparator
			create accounts.make (l_comparator)
			create transactions.make (l_comparator)
			create transaction_groups.make (l_comparator)
		end

feature -- Stored data

	accounts: DS_AVL_TREE [EAC_ACCOUNT, STRING]
		-- Account records, tagged by unique ID

	transactions: DS_AVL_TREE [EAC_TRANSACTION, STRING]
		-- Transaction records: indexed by ID


	transaction_groups: DS_AVL_TREE [EAC_TRANSACTION, STRING]
		-- Transaction records that are in a group
		-- Indexed by split_group ID + "_" + ID	

	last_transaction_id: INTEGER_64

feature -- Indexes
feature -- Initialization

	repository_init
		require
			--	No other clients connected
			-- Authorized ...
		do
			accounts.wipe_out
			transactions.wipe_out
			transaction_groups.wipe_out
				--	TODO
				--	database.wipe_out
				--
		end

feature -- Update
feature -- Retrieval
feature -- Users / Login and validation
feature -- Store/Retrieve to/from a file

		--	store_directory_path: detachable STRING

		--	set_store_directory (a_directory_path: STRING)
		--		require
		--			is_a_directory: is_directory_path (a_directory_path)
		--		do
		--
		--		end

	last_error: INTEGER

		--TODO: Precede stored data by a package of a version no and signature ...
		--TODO: Error checking ...

	store_to_file (a_file_name: STRING)
		local
			l_mrw: SED_MEDIUM_READER_WRITER
			l_file: RAW_FILE
		do
			create l_file.make_create_read_write (a_file_name)
			create l_mrw.make_for_writing (l_file)
			write_signature_and_version (l_mrw)
			independent_store (Current, l_mrw, True)
				--TODO: Check for writing errors ?
			l_file.close
		end

	load_from_file (a_file_name: STRING)
		local
			l_mrw: SED_MEDIUM_READER_WRITER
			l_file: RAW_FILE
			l_signature_ok: BOOLEAN
			l_retrieved_ok: BOOLEAN
		do
			last_error := 0
			create l_file.make_open_read (a_file_name)
			create l_mrw.make_for_reading (l_file)
			l_signature_ok := read_signature_and_version (l_mrw)
			if l_signature_ok then
				if attached {EAC_STORED_DATA} retrieved_from_medium (l_file) as r then
					accounts := r.accounts
					transactions := r.transactions
				else
					last_error := -1	-- TODO
				end
			else
				last_error := -1	-- TODO
			end
			l_file.close
		end

	write_signature_and_version (a_mrw: SED_MEDIUM_READER_WRITER)
		do
			a_mrw.write_integer_32 (eac_signature_value)
			a_mrw.write_integer_32 (eac_version_value)
		end

	read_signature_and_version (a_mrw: SED_MEDIUM_READER_WRITER): BOOLEAN
		local
			l_signature: INTEGER_32
			l_version: INTEGER_32
		do
			l_signature := a_mrw.read_integer_32
			l_version := a_mrw.read_integer_32
			Result := l_signature = eac_signature_value and l_version = eac_version_value
		end

	eac_signature_value: INTEGER_32 = 0xEAC0EAC0
			-- Signature value at the start of an EAC data file

	eac_version_value: INTEGER_32 = 1

--	is_directory_path (a_path: STRING): BOOLEAN
--			-- Check that the provided path is a directory,
--			-- and that it is sufficiently accessible
--		do
--
--		end

end
