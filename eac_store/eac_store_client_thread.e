note
	description: "Summary description for {EAC_STORE_TEST}."
	author: "Howard Thomson"
	date: "17-May-2020"

	TODO: "[
		This is a distinct thread, for the purposes of dealing with longer lasting
		interactions with the server; it may not be needed ...
		
		Test exit condition: timeout needed on wait for message
	]"


class
	EAC_STORE_CLIENT_THREAD

inherit
	THREAD
		rename
			make as make_thread
		end
	EAC_REPORTER
	EAC_SHARED_DATA

create
	make

feature
	net_address: STRING = "inproc://rot13"

	socket: NNG_REQUEST_SOCKET

--	trace_file: PLAIN_TEXT_FILE

	make --(a_address: STRING)
			-- Setup for this thread
		do
			make_thread
				-- Create Request socket
			create socket
		end

	execute
			-- Entry point for this thread
		local
			l_recvmsg: NNG_MESSAGE
			l_sendmsg: NNG_MESSAGE
			l_value: INTEGER_32
			l_gc_info: GC_INFO
			l_mem_stats: MEM_INFO
			l_gc_flag: BOOLEAN
		do
			print ("Client thread starting ...%N")
			from
				socket.open
				check socket.last_error = 0 and then socket.is_open end

					-- Dial the socket

					-- socket.dial (net_address)
				socket.dial (net_address)
				check socket.last_error = 0 end
			until
				eac_terminate.item
			loop

					-- Allocate a message
				create l_sendmsg.make_size (0);
				check size_on_creation_is_0: l_sendmsg.nng_msg_len (l_sendmsg) = 0 end

					-- Append some predictable data ...
				if l_gc_flag then
					l_sendmsg.append_integer_32 (0x99999999)
				else
					l_sendmsg.append_integer_32 (0xabcd1234)
				end

				check l_sendmsg.last_error = 0 end
				check size_after_append_is_4: l_sendmsg.nng_msg_len (l_sendmsg) = 4 end

					-- Send the request
				socket.send (l_sendmsg)
				check socket.last_error = 0 end

					-- Receive a response
				socket.receive
				l_recvmsg := socket.last_message
				check received_message_size_is_4: l_recvmsg.nng_msg_len (l_recvmsg) = 4 end

					-- Extract the data
				l_value := l_recvmsg.trim_integer_32
				check l_recvmsg.last_error = 0 end
				check size_after_trim_is_0: l_recvmsg.nng_msg_len (l_recvmsg) = 0 end
				check l_value = 0x5678ffff or else l_value = 0x12348765 end

					-- Free the message
				l_recvmsg.msg_free
			end
		end
end
