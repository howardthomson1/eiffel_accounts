note
	description: "Summary description for {EAC_STORE}."
	author: "Howard Thomson"

	Design_notes: "[
		This is the API to access the stored data for the Eiffel Accounts system -- EAC
		
		Initialize a new DB / Connect to [load from] a DB
		
		Accounts:
			Root account
			Immediate descendants of a specified account
			Count of the currently enabled (?) accounts
			Account balance: Most-recent, at-date, end-of-year ...
			New account
			Update account
			Delete [tombstone] an account
		
		Transactions:
			Store a new transaction
			Update a transaction
			Retrieve one, or more transactions
	]"

	TODO: "[
		Make socket address configurable ...
		Need termination request to signal the socket condition_variable ...
		Use a message factory available to the socket ...
		OR fully use the class serialize/deserialize system.
		
		Multiple sockets ... Use NNG Contexts.
	]"

class
	EAC_STORE

inherit
	THREAD
		rename
			make as make_thread
		end

	EAC_NNG_INPROC_URLS
	EAC_MSG_TYPE_IDS
	EAC_SHARED_DATA

	EAC_REPORTER

create
	make

feature

	net_address: STRING = "inproc://eac_test"

	socket: NNG_REPLY_SOCKET

	make
			-- Setup for this thread
		do
			make_thread
			create socket
			eac_data.eac_sockets.force (socket, eac_data.eac_sockets.count + 1)
		end

	execute
			-- Entry point for this thread
		local
			l_msg: NNG_MESSAGE
			l_value: INTEGER_32
--			l_reply: NNG_MESSAGE
		do
				-- Create the Reply socket
			socket.open
			check socket.last_error = 0 and then socket.is_open end
				-- Listen on configured [TODO] address
			socket.listen (net_address)
			from
			until
				eac_terminate.item
			loop
					-- Receive request
				socket.receive
				if socket.signalled then
					-- Do nothing ?
				elseif socket.last_error /= 0 then
					-- Error TODO
				else
					l_msg := socket.last_message

--TODO process incoming message ...
					process_message (l_msg)
					l_msg.msg_free
				end
				if not socket.signalled then
						-- Send reply

-- TODO generate response ...
					if attached reply as l_reply then
						socket.send (l_reply)
					end
					if socket.signalled then
						-- Do nothing ? ...
					elseif socket.last_error /= 0 then
						-- Error! TODO
					else
							-- If message sent OK:
	--					check l_reply.msg_ptr = default_pointer end
					end
				end
			end

-- What happens as the thread terminates ????
			current.join_all
		end


	process_message (a_nng_message: NNG_MESSAGE)
		require
			message_not_void: a_nng_message /= Void
		local
			l_msg: EAC_MESSAGE
			l_msg_type: INTEGER_32
			l_user_name: STRING
			l_password: STRING
			l_url: STRING
--			i: INTEGER_32
			l_reply: EAC_MESSAGE
		do
			create l_msg.from_nng_message (a_nng_message)
			l_msg.set_for_reading
			l_msg_type := l_msg.read_integer_32

			check l_msg_type = {EAC_MSG_TYPE_IDS}.msg_connect end	--TEMP !
			l_user_name := l_msg.read_string_8
			l_password := l_msg.read_string_8
			l_url := l_msg.read_string_8

			report_while_idle ("User_name: " + l_user_name + "%N")
			report_while_idle ("Password: " + l_password + "%N")
			report_while_idle ("Url: " + l_url + "%N")

		-- TODO Check user name ...
		--	Associate this connection with the user -- if accepted


			create l_reply
			l_reply.set_for_writing
			l_reply.write_integer_32 ({EAC_MSG_TYPE_IDS}.msg_connect)
			l_reply.write_footer
			reply := l_reply

		end

	reply: detachable NNG_MESSAGE

	stored_data: detachable EAC_STORED_DATA

end
