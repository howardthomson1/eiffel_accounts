note
	description: "Summary description for {EAC_STORE_CLIENT}."
	author: "Howard Thomson"
	date: "8-June-2020"

	TODO: "[
		Decide when socket is opened/closed
		
		Separate out sequence of request/reply sequencing from
		processing reply when received.
		
		Use do_once_on_idle istead of idle_action
	]"

class
	EAC_STORE_CLIENT

inherit
	EV_SHARED_APPLICATION
	EAC_REPORTER

create
	make

feature

	make
		do
			create socket
--			ev_application.add_idle_action (agent idle_action)
		end

	socket: NNG_REQUEST_SOCKET

	idle_action
		local
			l_time: TIME
		do
			if attached action as a then
				action := Void
				a.call ([])
			end
		end

	action: detachable PROCEDURE []

feature -- Preparation

	connect (a_user_name, a_password, a_url: STRING)
		local
			l_msg: EAC_MESSAGE
--			l_writer: SED_READER_WRITER
--			i: INTEGER_32
		do
			if not socket.is_open then
				socket.open
			end
			socket.dial (a_url)
			if socket.last_error /= 0 then
				report ("Socket dial error ...%N")
				action := agent connect_message_sent	-- TEMP!
			else
				create l_msg
				l_msg.set_for_writing
				l_msg.write_integer_32 ({EAC_MSG_TYPE_IDS}.msg_connect)
				l_msg.write_string_8 (a_user_name)
				l_msg.write_string_8 (a_password)
				l_msg.write_string_8 (a_url)
				l_msg.write_footer	-- Adjust nng_msg_len to match buffer_position
				socket.send_async (l_msg)
				action := agent connect_message_sent
			end
		end

	connect_message_sent
		local
			l_msg: EAC_MESSAGE
		do
			if socket.is_aio_completed then
					-- Initiate asynchronous receive
		--		create l_msg
				socket.receive_async
				action := agent expect_reply
			else
					-- Still waiting ...
				action := agent connect_message_sent
			end
--			report_while_idle ("Connect_message_sent called ...%N")
		end

	expect_reply
		do

		end

	disconnect
		require
			is_open: socket.is_open
		do

		end
end
