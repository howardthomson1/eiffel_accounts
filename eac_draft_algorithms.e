note
	description: "Notepad for update algorithm ..."
	author: "Howard Thomson"
	date: "27-Feb-2021"

	TODO: "[
		Client to subscribe to notifications of updates by other clients ...
		
		Update to database scenarios:
		
		1/ A new simple [single object] addition to the database
		
		2/ An existing simple object replacement in the database
		
		3/ A new split group [multiple object] addition to the database

	]"

	Protocol_reference: "[
		https://en.wikipedia.org/wiki/Timestamp-based_concurrency_control

	]"

class
	EAC_DRAFT_ALGORITHMS

create
	make

feature -- Creation

	make
		local
			l_comparator: EAC_ACCOUNT_COMPARATOR
		do
			create l_comparator
			create current_edit_transactions.make (l_comparator)
		end

feature -- Attributes

	edit_in_progress: BOOLEAN
		-- Flag for updates in progress to apply to the dbms

	current_edit_account: detachable EAC_ACCOUNT

	current_edit_transactions: DS_AVL_TREE [EAC_TRANSACTION, STRING]

feature -- Routines

	new_account: EAC_ACCOUNT
		require
			not_in_progress: not edit_in_progress
		do
			create Result
			current_edit_account := Result
			edit_in_progress := True
		ensure
			in_progress: edit_in_progress
		end

	new_transaction: EAC_TRANSACTION
		require
			not_in_progress: not edit_in_progress
		do
			create Result
			current_edit_transactions.put_new (Result, Result.id)
			edit_in_progress := True
		ensure
			in_progress: edit_in_progress
		end

	edit_account (a_account: EAC_ACCOUNT)
		require
			not_in_progress: not edit_in_progress
		do
			check TODO: false end
			current_edit_account := a_account
			current_edit_account.set_transaction_id (-1)
			edit_in_progress := True
		ensure
			in_progress: edit_in_progress
		end

	edit_transaction (a_transaction: EAC_TRANSACTION)
		require
			not_in_progress: not edit_in_progress
		do
--			check TODO: false end
			if not a_transaction.is_group_type then
				current_edit_transactions.put_new (a_transaction, a_transaction.id)
				a_transaction.set_transaction_id (-1)
			else
				edit_transaction_parts (a_transaction)
			end
			edit_in_progress := True
		ensure
			in_progress: edit_in_progress
		end




	commit
			-- Commit new and updated Accounts and Transactions
			-- New items have a Transaction_ID of Zero
			-- Updated items have a Transaction_ID of -1

			-- Send all new and updated items to the DB
			-- Receive either a Transaction ID, which will be used to update the items
			-- Or an error code, ... ???
		require
			in_progress: edit_in_progress
			valid_for_commit: current_elements_valid_for_commit
		local
--			l_commit_msg:
		do
--			db.start
--			l_transaction := db.last_transaction
--			from
--				
--			until

--			loop

--			end

			edit_in_progress := False
		ensure
			not_in_progress: not edit_in_progress
		end

	abort
		do
			current_edit_account := Void
			current_edit_transactions.wipe_out
			edit_in_progress := False
		ensure
			not_in_progress: not edit_in_progress
		end

feature {NONE} -- Implementation

	edit_transaction_parts (a_transaction: EAC_TRANSACTION)
			-- Add all parts of this transaction to 'current_edit_transactions'
		do
			check false end
		end

feature

	current_elements_valid_for_commit: BOOLEAN
			-- Check that all created and edited objects are 'valid_for_commit'
		local
			l_cursor: DS_AVL_TREE_CURSOR [EAC_TRANSACTION, STRING]
		do
			Result := True
			if attached current_edit_account as l_account then
--				if not l_account.valid_for_commit then
--					Result := False
--				end
			end
			from
				l_cursor := current_edit_transactions.new_cursor
			until
				l_cursor.after
			loop

				l_cursor.forth
			end
		end

--	import_barclays_csv
--		local
--			importer: CSV_IMPORTER_BARCLAYS
--		do
--			create importer.make ("/home/ht/Downloads/Barclays-40904082-from-1-1-2019-to-2-4-2020.csv")
--			importer.scan
--		end

--	open_transactions_window
--		local
--			w: EAC_TRANSACTIONS_WINDOW
--		do
--			create w
--			w.show
--		end

end
