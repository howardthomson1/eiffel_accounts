note
	description: "Summary description for {EAC_TRANSACTION}."
	author: "Howard Thomson"
	project: "Eiffel Accounts"
	copyright: "Copyright (C) 20-April-2020"

	TODO: "[
		How to update an existing record ...
			New record identifiable from transaction_id = 0
			Need to distinguidh between records that already exist and need update,
			as distinct from records that only exist locally
			
		Sort out versioning of transaction records ...
	
		Two memo attributes needed for Type_basic ...
		Change date to INTEGER compact date attribute
		
		Archive 'deleted' records, previous version of 'altered' records ...
	]"
	DONE: "[
		Normalise the representation of a group of transactions ... DONE?
		Convert a GnuCash GUID to an Eiffel UUID valid string ? DONE
		Assign a UUID to 'id' on default_create DONE
	]"

class
	EAC_TRANSACTION

inherit
	ANY
		redefine
			default_create
		end

create
	default_create,
	make_for_import

feature

	default_create
		do
			id := {UUID_GENERATOR}.generate_uuid.string
		ensure then
			not_valid_for_commit: not is_valid_for_commit	-- TEMP !!!!
		end

	make_for_import (an_id: STRING)
		require
			not_void: an_id /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (an_id)
		do
			id := an_id
		ensure
			not_valid_for_commit: not is_valid_for_commit	-- TEMP !!!!
		end

feature -- Attributes stored to DBMS

	id: STRING
		-- Unique GUID key for this transaction

	type: INTEGER
		-- One of the Type constants -- see below

	value: INTEGER_64
		-- UK GBP value in pence [1/00 UK Pound]

	split_group: detachable STRING
		-- GUID which ties each of a goup of transaction components together

	debit_account: detachable STRING
		-- GUID of the Account being Debited

	credit_account: detachable STRING
		-- GUID of the Account being Credited

	description: detachable STRING
		-- Text describing this transaction

	memo: detachable STRING
		-- Additional annotation ...

	other_memo: detachable STRING
		-- Annotation for other side of transaction, if different

	import_tag: detachable STRING
		-- Group GUID tag for a set of imported transactions

	date: INTEGER_32
		-- Date assigned to this transaction
		-- As stored inside an instance of class DATE

--	user: detachable STRING
		-- GUID tag of the logged in user generating this item

--	date_posted: ????

	is_group_lead: BOOLEAN
		-- Is this the 'lead' item for the group
		-- Common features for the group are stored
		-- in this item

	transaction_id: INTEGER_64
		-- ID of the transaction that created, or last updated this item
		-- Zero implies newly created locally, not yet applied to the DB

feature -- Query on initial creation

	is_pre_commit: BOOLEAN
		do
			Result := transaction_id = 0
				and import_tag = Void
		end

feature-- Attribute setting

	set_id (a_uuid: STRING)
		require
			id_not_void: a_uuid /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			create id.make_from_string (a_uuid)
		end

	set_type (a_type: INTEGER)
		require
			valid_type: valid_type (a_type)
		do
			type := a_type
		end

	set_value (a_value: INTEGER_64)
		do
			value := a_value
		end

	set_split_group (a_split_group: STRING)
		require
			is_valid_uuid: {UUID}.is_valid_uuid (a_split_group)
		do
			create split_group.make_from_string (a_split_group)
		end

	set_description (a_string: STRING)
		do
			if a_string.count > 0 then
				create description.make_from_string (a_string)
			end
		end

	set_memo (a_string: STRING)
		do
			create memo.make_from_string (a_string)
		end

	set_other_memo (a_string: STRING)
		do
			create other_memo.make_from_string (a_string)
		end

	set_import_tag (a_string: STRING)
		require
			not_void: a_string /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (a_string)
		do
			create import_tag.make_from_string (a_string)
		end

	set_date (a_date: like date)
		do
			date := a_date
		end

	set_is_group_lead (a_group_lead: BOOLEAN)
		do
			is_group_lead := a_group_lead
		end

feature --{XML_IMPORTER_GNUCASH} -- Attribute setting

	set_debit_account (a_uuid: STRING)
		require
			not_void: a_uuid /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			create debit_account.make_from_string (a_uuid)
		end

	set_credit_account (a_uuid: STRING)
		require
			not_void: a_uuid /= Void
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			create credit_account.make_from_string (a_uuid)
		end

feature -- Debit/Credit account setting

--	set_debit_from

feature -- Validity checking

	is_valid_for_commit: BOOLEAN
			-- Is this record internally consistent for storing
			-- to it's equivalent DBMS record ?
		do
			Result := has_valid_type
			and then (date /= 0)
			and then (type = Type_basic implies (debit_account /= Void and credit_account /= Void))
			and then (type /= Type_Basic implies (debit_account = Void or else credit_account = Void))
			and then (is_group_type implies split_group /= Void)
			and then (id /= Void implies {UUID}.is_valid_uuid (id))
			and then (value /= 0)
			-- TODO ...
		end

	check_is_valid_for_commit
			-- Is this record internally consistent for storing
			-- to it's equivalent DBMS record ?
		do
			check has_valid_type end
			check (date /= 0) end
			check (type = Type_basic implies (debit_account /= Void and credit_account /= Void)) end
			check (type /= Type_Basic implies (debit_account = Void or else credit_account = Void)) end
			check (is_group_type implies split_group /= Void) end
			check (id /= Void implies {UUID}.is_valid_uuid (id)) end
			check (value /= 0) end
			-- TODO ...
		end


	has_valid_id: BOOLEAN
		do
			Result := id /= Void and then not id.is_equal ("Undefined")
		end

	has_valid_type: BOOLEAN
		do
			Result := valid_type (type)
		end

	valid_type (a_type: INTEGER): BOOLEAN
		do
			Result := a_type = Type_basic
			or else a_type = Type_Group
			or else a_type = Type_Initial_Balance
			or else a_type = Type_Confirmation_Balance
			or else a_type = Type_Period_Closing_Balance
			or else a_type = Type_Import_Start

			-- TODO ...
		end

	is_group_type: BOOLEAN
			-- Is this part of a group of transaactions that must be committed together ?
		do
			Result := type /= Type_Basic
		end

	is_in_default_state: BOOLEAN
			-- Has this object been modified since creation ?
		do
			Result := description = Void
			and then value = 0
			and then date = 0
				--TODO ...
		--	and then ...
		end

	reset
			-- Restore to default state
		do
			description := Void
			value := 0
			date := 0
			--TODO ...
		ensure
			is_in_default_state: is_in_default_state
		end

feature -- Attributes for internal processing

	is_commit_ready: BOOLEAN

	set_commit_ready
		require
			may_be_committed: is_valid_for_commit
		do
			is_commit_ready := True
		end

feature -- Transaction types

	Type_Basic: INTEGER = 1
		-- Simple transfer from one account to another
	Type_Group: INTEGER = 2
		-- A Group of records with a common split_group GUID
		-- The records as a Group must balance for Debits and Credits
	Type_Initial_Balance: INTEGER = 3
		-- A Group of records, also with a common split_group GUID
		-- If it exists, always the earliest [by date] recorded transaction
	Type_Confirmation_Balance: INTEGER = 4
		-- Can be 'Locked', preventing modification to previous transactions
		-- Unlocking may require appropriate 'permission'
	Type_Period_Closing_Balance: INTEGER = 5
		-- A 'closing' balancing transaction Group to end a financial period
		-- Income and Expenditure reversal totals corresponding to the
		-- Surplus/Deficit or Profit/Loss account for that period
	Type_Import_Start: INTEGER = 6
--	Type_Import_

feature -- Display

	is_debit (a_uuid: STRING): BOOLEAN
			-- is_debit in the context of the specific account for a_uuid
		require
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			if attached debit_account as d and then d.is_equal (a_uuid) then
				Result := True
			end
		end

invariant
	has_unique_id: id /= Void
	valid_id: {UUID}.is_valid_uuid (id)
	valid_group_id: attached split_group as sg implies {UUID}.is_valid_uuid (sg)
	valid_debit_account: attached debit_account as da implies {UUID}.is_valid_uuid (da)
	valid_credit_account: attached credit_account as ca implies {UUID}.is_valid_uuid (ca)
	commit_validity: is_commit_ready implies is_valid_for_commit
	not is_group_lead implies description = Void
	not is_group_lead implies memo = Void

	attached debit_account as dr and attached credit_account as cr
		implies not dr.is_equal (cr)

	attached debit_account or else attached credit_account
note

--	invariants_to_

end
