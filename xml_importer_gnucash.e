note
	description: "Importer for Uncompressed GnuCash XML Data File"
	author: "Howard Thomson"
	date: "12-April-2020"
	TODO: "[
		Be more robust in checking for validity of GnuCash file entries
		Before opening a different Gnucash file, wipeout any previous data

		Need uncompress option ...
	
		Need dialogues to:
			Select a start date, and possible end date for transactions to import
			Enable / Disable auto creation of new [non-matching] Accounts
			Map import acounts onto existing accounts
			Confirm consistency requirements before committing to data import
	]"

class
	XML_IMPORTER_GNUCASH

inherit
	EAC_REPORTER

create
	make

feature {NONE} -- Initialization


	gnucash_callbacks: XML_GNUCASH_CALLBACKS

	make
			-- Initialization for `Current'.
		do
			create xml_accounts.make (1000)
			create xml_transactions.make (1000)
			create eac_accounts.make (1000)
			create eac_transactions.make (1000)
			create xml_parser.make
			create gnucash_callbacks.make (Current)
			xml_parser.set_callbacks (gnucash_callbacks)
			gnucash_callbacks.set_associated_parser (xml_parser)
		end

feature -- XML parsed data

	xml_accounts: HASH_TABLE [EAC_XML_GNC_ACCOUNT, STRING]
		-- XML imported GnuCash Accounts, keyed on GUID

	xml_transactions: HASH_TABLE [EAC_XML_GNC_TRANSACTION, STRING]
		-- XML imported Transactions, keyed on XXX


	eac_accounts: HASH_TABLE [EAC_ACCOUNT, STRING]
		-- Native accounts, indexed by GUID

	eac_transactions: HASH_TABLE [EAC_TRANSACTION, STRING]
		-- Native transactions, indexed by group GUID
		-- Where no. splits = 2, indexed by transaction GUID
		--	 In this case, there is only on native record
		-- Where no. splits >=3, indexed by split GUID
		--   In this case there is one record per split

feature

	xml_parser: XML_STANDARD_PARSER

	parse_successful: BOOLEAN

	parse_from_file (a_filename: STRING)
			-- TEMP test Zlib expansion file => file
		local
			l_rescued: BOOLEAN
			zlib: ZLIB
			gz_handle: POINTER
			in_buffer: MANAGED_POINTER
			in_string: STRING_8
			l_allocated: INTEGER_32
			l_chunk: INTEGER_32
			l_pos: INTEGER_32
			xml_stream: XML_STRING_INPUT_STREAM
		do
			if not l_rescued then
				from
					create zlib
					gz_handle := zlib.gzopen (a_filename, "rb")
					l_chunk := 64 * 1024
					create in_buffer.make (l_chunk)
					zlib.gzread (gz_handle, in_buffer.item + l_pos, l_chunk)
					check zlib.last_operation >= 0 end
					l_pos := l_pos + zlib.last_operation
				until
					zlib.last_operation < l_chunk
				loop
					in_buffer.resize (l_pos + l_chunk)
					zlib.gzread (gz_handle, in_buffer.item + l_pos, l_chunk)
					check zlib.last_operation >= 0 end
					l_pos := l_pos + zlib.last_operation
				end
				zlib.gzclose (gz_handle)
				create in_string.make_from_c_byte_array (in_buffer.item, l_pos)
				create xml_stream.make (in_string)
				xml_parser.parse_from_stream (xml_stream)
				if xml_parser.error_occurred
				or else xml_parser.truncation_reported
				or else not gnucash_callbacks.is_xml_document
				or else not gnucash_callbacks.has_gnc_v2_element
				then
					report ("GnuCash XML Parsing error !%N")
				else
					parse_successful := True
				end
--				update_xml_account_balances
				import_from_xml
			end
		rescue
			l_rescued := TRUE
		end

--	update_xml_account_balances
--			-- Process xml transactions against the xml accounts to provide account balances ...
--		local
--			l_accounts: HASH_TABLE [EAC_XML_GNC_ACCOUNT, STRING]
--			l_transactions: HASH_TABLE [EAC_XML_GNC_TRANSACTION, STRING]

--			l_account: EAC_XML_GNC_ACCOUNT
--			l_transaction: EAC_XML_GNC_TRANSACTION

--			l_splits: HASH_TABLE [EAC_XML_TRN_SPLIT, STRING]
--			l_split: EAC_XML_TRN_SPLIT
--			l_value: INTEGER_64
--		do
--			l_accounts := xml_accounts
--			l_transactions := xml_transactions
--			from l_transactions.start
--			until l_transactions.after
--			loop
--				l_transaction := l_transactions.item_for_iteration
--				if attached l_transaction.splits as l_xml_splits then
--					l_splits := l_xml_splits.splits
--					from l_splits.start
--					until l_splits.after
--					loop
--						l_split := l_splits.item_for_iteration
--						l_value := l_split.value
--						if l_value /= 0 then
--							if attached l_split.account as l_account_id then
--								l_accounts.search (l_account_id)
--								if l_accounts.found then
--									l_account := l_accounts.found_item
--									if attached l_account as l_act then
--										l_act.update_balance ( - l_value)	-- ???
--									end
--								else
--									check false end
--								end
--							end
--						end
--						l_splits.forth
--					end
--				end
--				l_transactions.forth
--			end
--		end


	import_from_xml --(a_gnucash_file_importer: XML_IMPORTER_GNUCASH)
			-- Process the XML data from GNUCash into EAC representation
		local
			l_accounts: HASH_TABLE [EAC_XML_GNC_ACCOUNT, STRING]
			l_item: EAC_XML_GNC_ACCOUNT
			l_new_account: EAC_ACCOUNT
			l_parent_account: EAC_ACCOUNT
			l_grid_row: EV_GRID_ROW
			l_new_row: EV_GRID_ROW
			l_row_index: INTEGER
			l_balance: INTEGER_64
			l_credits, l_debits: INTEGER_64
			l_cumulative_credits, l_cumulative_debits: INTEGER_64
			l_new_id: STRING_8
		do
			l_accounts := xml_accounts
			from l_accounts.start
			until l_accounts.after
			loop
				l_item := l_accounts.item_for_iteration
				if attached l_item.act_id as l_id then
					l_new_id := uuid_insert_separators(l_id)
				else
					check false end
					l_new_id := ""
				end
				create l_new_account.make_for_import (l_new_id)
					-- Set name
				if attached l_item.act_name as l_name then
					l_new_account.set_name (l_name.twin)
				end
					-- Set type
				if attached l_item.act_type as l_type then
					if l_type.is_equal ("ROOT") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_root)
					elseif l_type.is_equal ("ASSET") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_asset)
					elseif l_type.is_equal ("LIABILITY") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_liability)
					elseif l_type.is_equal ("INCOME") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_income)
					elseif l_type.is_equal ("EXPENSE") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_expense)
					elseif l_type.is_equal ("BANK") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_bank)
					elseif l_type.is_equal ("CREDIT") then
						l_new_account.set_type ({EAC_ACCOUNT}.type_credit)
					elseif l_type.is_equal 	("EQUITY") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Equity)
					elseif l_type.is_equal 	("CASH") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Cash)
					elseif l_type.is_equal 	("MUTUAL") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Mutual)
					elseif l_type.is_equal 	("PAYABLE") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Payable)
					elseif l_type.is_equal 	("RECEIVABLE") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Receivable)
					elseif l_type.is_equal 	("STOCK") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Stock)
					elseif l_type.is_equal 	("TRADING") then
						l_new_account.set_type ({EAC_ACCOUNT}.Type_Trading)
					else
						check false end
					end
				end
					-- Set parent
				if attached l_item.act_parent as l_parent then
					l_new_account.set_parent_id (uuid_insert_separators(l_parent))
				end
					-- Set description
				if attached l_item.act_description as l_description then
					l_new_account.set_description (l_description.twin)
				end
					-- Set code: TODO ?

					-- Add new account to EAC_DATA
				if l_new_account.attributes_set_for_import then
						-- Has the new accounts' parent already been found ?
					eac_accounts.search (l_new_account.attached_parent_id)
--					if eac_accounts.found  then
--						l_parent_account := eac_accounts.found_item
--					else
----						set_item (1, 1, create {EV_GRID_LABEL_ITEM}.make_with_text ("ROOT"))
----						l_new_account.set_grid_xy (1, 1)
--					end

					eac_accounts.extend (l_new_account, l_new_account.id)
				else
					report ("Check False !%N")
					check false end
				end
				l_accounts.forth
			end
			import_xml_transactions
		end

-- An XML transaction with two 'splits' maps into a single EAC_TRANSACTION, with the ID of
-- XML transaction. For three or more splits, the ID becomes the split_group id, and the
-- distinct split IDs become the IDs of the individual EAC_TRANSACTIONS

	import_xml_transactions
		local
			l_transactions: HASH_TABLE [EAC_XML_GNC_TRANSACTION, STRING]
			l_item: EAC_XML_GNC_TRANSACTION
			l_eac: EAC_TRANSACTION
			l_split_count: INTEGER
			l_import_tag: UUID
			l_group_tag: UUID
			l_split, l_split_2: EAC_XML_TRN_SPLIT
			l_same_account: BOOLEAN
			i: INTEGER
			l_reported: BOOLEAN
			l_date: DATE
			l_date_string: STRING
			l_date_format_string: STRING
		do
			l_date_format_string := "yyyy-[0]mm-[0]dd";

			create l_import_tag
			create l_date.make_now
				-- TODO Save import_tag elsewhere ...
			l_transactions := xml_transactions
			from l_transactions.start
			until l_transactions.after
			loop
				l_reported := False
				l_item := l_transactions.item_for_iteration
				l_same_account := False
				if attached l_item.id as l_id then
					if attached l_item.splits as l_splits
					and then attached l_splits.splits as l_sp then
						l_split_count := l_sp.count
						if l_split_count >= 2 then
							l_sp.start; l_split := l_sp.item_for_iteration
							l_sp.forth; l_split_2 := l_sp.item_for_iteration
							if attached l_split.account as l_ac_1
							and then attached l_split_2.account as l_ac_2
							and then l_ac_1.is_equal (l_ac_2) then
								l_same_account := True
							end
						end
							-- This would seem to be a bug in GnuCash ...
						if l_split_count = 1 then
								-- TODO Report invalid GnuCash file!
							report ("Split count = 1: ")
							l_sp.start; l_split := l_sp.item_for_iteration
							if attached l_split.id as l_d then
								report (l_id)
							end
							if l_split.value /= 0 then
								report ("  value /= 0 !")
							end
							report ("%N")
						elseif l_split_count = 2
						and then not l_same_account
						then
							l_sp.start; l_split := l_sp.item_for_iteration
							l_sp.forth; l_split_2 := l_sp.item_for_iteration
								-- Type_Basic
								-- Create retaining the existing UUID
							create l_eac.make_for_import (uuid_insert_separators(l_id))
								-- Set type
							l_eac.set_type ({EAC_TRANSACTION}.Type_basic)
								-- Set value
							l_eac.set_value (l_split.value.abs)
								--	.set_debit_account
								--	.set_credit_account
							if attached l_split.account as ac then
								if l_split.value > 0 then
									l_eac.set_debit_account (uuid_insert_separators(ac))
								else
									l_eac.set_credit_account (uuid_insert_separators(ac))
								end
							end
							if attached l_split_2.account as ac then
								if l_split_2.value > 0 then
									l_eac.set_debit_account (uuid_insert_separators(ac))
								else
									l_eac.set_credit_account (uuid_insert_separators(ac))
								end
							end
								-- set_date ...
							if attached l_item.date_posted as dp then
								create l_date_string.make_from_string (dp)
								l_date_string.keep_head (l_date_string.index_of (' ', 1))
								l_date.make_from_string (l_date_string, l_date_format_string)
								l_eac.set_date (l_date.ordered_compact_date)
							end
								-- set_description
							if attached l_item.description as d then
								l_eac.set_description (d)
							end
								-- set_memo
							if attached l_split.memo as m then
								l_eac.set_memo (m)
								if attached l_split_2.memo as m2 then
										l_eac.set_other_memo (m2)
								end
							end

						--	.set_import_tag
						--	TODO

						--	check l_eac.is_valid_for_commit end
							if not l_eac.is_valid_for_commit and not l_reported then
								report ("Transaction NOT ready for commit ...%N")
								if attached l_item.id as l_item_id then
									report ("   id = " + l_item_id + "%N")
								end
								l_reported := True
							else
									--	Commit ...
								check not eac_transactions.has (l_eac.id) end
								eac_transactions.extend (l_eac, l_eac.id)
							end
						else	-- Type_Group
		--					report ("Type_group ...%N")
		--					report ("Split count: " + l_split_count.out + "%N")
--====================================================================
					--		check false end
							from
								i := 1
								l_sp.start
								create l_group_tag
							until i > l_split_count
							loop
								l_split := l_sp.item_for_iteration
								if attached l_split.id as l_split_id then
									create l_eac.make_for_import (uuid_insert_separators(l_split_id))
										--	.set_type
									l_eac.set_type ({EAC_TRANSACTION}.type_group)
										--	.set_value
									l_eac.set_value (l_split.value.abs)
										-- Set account
									if attached l_split.account as l_ac then
										if l_split.value > 0 then
											l_eac.set_debit_account (uuid_insert_separators (l_ac))
										else
											l_eac.set_credit_account (uuid_insert_separators (l_ac))
										end
									end
										--	.set_date
									if attached l_item.date_posted as dp then
										create l_date_string.make_from_string (dp)
										l_date_string.keep_head (l_date_string.index_of (' ', 1))
										l_date.make_from_string (l_date_string, l_date_format_string)
										l_eac.set_date (l_date.ordered_compact_date)
									end
									if i = 1 then
											--	.set_is_group_lead
										l_eac.set_is_group_lead (True)
											-- set_description
										if attached l_item.description as d then
											l_eac.set_description (d)
										end
											--	.set_memo


									end

										--	.set_import_tag
								--	TODO

										--	.set_split_group (from l_item.id)
									l_eac.set_split_group (l_group_tag.out)


								--	check l_eac.is_valid_for_commit
								--	add l_eac to commit group
									eac_transactions.extend (l_eac, l_eac.id)
									i := i + 1
									l_sp.forth
								else
									check false end
								end
							end



					--		create l_group_tag
					--		l_eac.set_split_group (l_group_tag.out)
						--	Commit the group
						end
					end
				end
				l_transactions.forth
			end

		end


	uuid_insert_separators (a_string: STRING): STRING
			-- Convert GNUCash UUID to Eiffel UUID
			-- by inserting '-' separators at the usual
			-- 8-4-4-4-12 grouping points
		require
			valid_uuid_count_no_separators: a_string.count = 32
		do
			create Result.make_from_string (a_string)
			Result.insert_character ('-', 9)
			Result.insert_character ('-', 14)
			Result.insert_character ('-', 19)
			Result.insert_character ('-', 24)
			check is_valid_uuid: {UUID}.is_valid_uuid (Result) end
		end


end
