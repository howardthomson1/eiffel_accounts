note
	description: "Summary description for {EAC_DESIGN_WINDOW}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_DESIGN_WINDOW

inherit
	EV_TITLED_WINDOW

create
	make

feature

	make
		local
			tw: EV_WINDOW
			label: EV_LABEL
		do
			create vb
			make_with_title ("Design Window")
			extend (vb)
			if attached (create {EV_APPLICATION}).windows as w
			then
				from w.start
				until w.after
				loop
					tw := w.item
					if tw /= Current then
						create label.make_with_text (tw.title)
						vb.extend (label)
					end
					w.forth
				end
			end
		end

feature -- Attributes

	vb: EV_VERTICAL_BOX


end
