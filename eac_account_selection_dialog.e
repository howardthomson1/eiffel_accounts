note
	description: "Summary description for {EAC_ACCOUNT_SELECTION_DIALOG}."
	author: "Howard Thomson"
	date: "$25-Aug-2021$"
	revision: "$Revision$"

	TODO: "[
		Everything !
		
		Expand tree ...
		Resize window to display full width of tree
	]"

class
	EAC_ACCOUNT_SELECTION_DIALOG

inherit
	EV_DIALOG
		redefine
			initialize,
			create_interface_objects,
			show_modal_to_window
		end
	EAC_SHARED_DATA
		undefine
			default_create, copy
		end
	EAC_REPORTER
		undefine
			default_create, copy
		end

create
	default_create,
	make

feature {NONE} -- Attributes

	grid: EAC_ACCOUNT_SELECTION_GRID

feature

	selected_account_id: detachable STRING

feature {NONE}

	make (a_uuid: STRING)
		require
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			default_create
			selected_account_id := a_uuid
		end

	create_interface_objects
		do
			create grid
			Precursor
		end

	initialize
		do
			setup_grid
			extend (grid)
			Precursor
			key_press_actions.extend (agent on_key_press)
			set_minimum_size (200,200)
		end

	show_modal_to_window (a_window: EV_WINDOW)
		do
			check data /= Void end
			Precursor (a_window)
		end

	set_selected_account_id (a_uuid: STRING)
		require
			is_valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		do
			selected_account_id := a_uuid
				-- TODO
			-- Set selected grid entry ...
			-- Expand tree to make selected entry visible
		end

	on_key_press (a_key: EV_KEY)
		local
			l_rows: ARRAYED_LIST [EV_GRID_ROW]
		do
			if a_key.code.is_equal ({EV_KEY}.key_escape) then
				hide
			elseif a_key.code.is_equal ({EV_KEY}.key_enter) then
				if grid.has_selected_row then
					l_rows := grid.selected_rows
					if l_rows.count = 1 then
						if attached {STRING} l_rows.i_th (1).data as key then
								-- Map account text key to uuid ...
							if attached avl_accounts_tree as t then
								if t.has (key) then
									if attached {EAC_ITEM_TR_ACCOUNT} data as d then
										d.set_account (t.item (key).id)
										selected_account_id := t.item (key).id
									end
								end
							end
						end
					end
				end
				hide
			end
		end

-- =======================================================================================
-- This code to migrate to a new descendant of EAC_GRID_COMMON ...

	setup_grid
		do
			grid.enable_tree
			grid.set_minimum_size (300, 300)
			grid.enable_row_separators
			grid.enable_column_separators
			grid.enable_single_row_selection
			grid.set_row_height (grid.row_height + 2)
			setup_columns
			grid.row_select_actions.extend (agent row_selected)

			map_accounts
			grid.expand_all_tree
		end

	row_selected (a_row: EV_GRID_ROW)
			-- A row has been selected -- Assign to selected_account_id
		do
			if attached {STRING} a_row.data as s_data
			and then attached avl_accounts_tree as tree
			and then tree.has (s_data)
			then
				selected_account_id := tree.item (s_data).id
			else
--				check data_attached: a_row.data /= Void end
				check a_row.index > 1 implies a_row.data /= Void end
			end
		end


	setup_columns
		do
			grid.insert_new_column (1)
			grid.insert_new_column (2)
			grid.insert_new_column (3)
				-- Assign corresponding headings ...
			grid.column (1).set_title ("Name")
			grid.column (2).set_title ("Type")
			grid.column (3).set_title ("Description")

				-- Centre column headings ...
			grid.column (1).header_item.align_text_center
			grid.column (2).header_item.align_text_center
			grid.column (3).header_item.align_text_left
		end


	avl_accounts_tree: detachable DS_AVL_TREE [EAC_ACCOUNT, STRING]

	map_accounts
			-- Map the Accounts into the accounts tree
		local
			l_account: EAC_ACCOUNT
			l_parent_account: EAC_ACCOUNT
			l_grid_row: EV_GRID_ROW
			l_new_row: EV_GRID_ROW
			l_row_index: INTEGER
			l_balance: INTEGER_64
			l_credits, l_debits: INTEGER_64
			l_cumulative_credits, l_cumulative_debits: INTEGER_64
			l_new_id: STRING_8
			l_cursor: like eac_data.eac_accounts.new_cursor

			l_root_account: EAC_ACCOUNT
			l_loop_acount: detachable EAC_ACCOUNT


			l_tree: DS_AVL_TREE [EAC_ACCOUNT, STRING]
			l_comparator: EAC_ACCOUNT_COMPARATOR
			l_key_string: STRING

		do
			create l_key_string.make_empty
			create l_comparator
			create l_tree.make (l_comparator)
			if grid.row_count > 0 then
				grid.remove_rows (1, grid.row_count)
			end
			if attached eac_data.eac_accounts as l_accounts then
					-- First setup the account references, using the parent_id attributes
				l_cursor := l_accounts.new_cursor
				from l_cursor.start
				until l_cursor.after
				loop
					l_account := l_cursor.item
					l_key_string := account_key_string (l_account, l_accounts)
					l_tree.put_new (l_account, l_key_string)
					l_cursor.forth
				end
				avl_accounts_tree := l_tree

--					-- Populate the grid from the ordered tree ...
				from l_tree.start
				until l_tree.after
				loop
					l_account := l_tree.item_for_iteration
					if l_account.is_root then
						grid.set_item (1, 1, create {EV_GRID_LABEL_ITEM}.make_with_text ("ROOT"))
						l_account.set_grid_xy (1, 1)
					else
--===
						if attached l_account.parent_id as attached_parent_id then
							l_accounts.search (attached_parent_id)
							if l_accounts.found  then
								l_parent_account := l_accounts.found_item
								if attached l_parent_account as attached_parent then

									if attached grid.item (attached_parent.grid_x, attached_parent.grid_y) as attached_item then
										l_grid_row := attached_item.row
										l_row_index := l_grid_row.subrow_count + 1
										l_grid_row.insert_subrow (l_row_index)
										l_new_row := l_grid_row.subrow (l_row_index)
										l_new_row.set_item (1, create {EV_GRID_LABEL_ITEM}.make_with_text (l_account.attached_name))
										l_new_row.set_item (2, create {EV_GRID_LABEL_ITEM}.make_with_text (l_account.type_as_string))
										l_new_row.set_item (3, create {EV_GRID_LABEL_ITEM}.make_with_text (l_account.attached_description))
										l_new_row.set_data (l_tree.key_for_iteration)
										if attached {EV_GRID_LABEL_ITEM	} l_new_row.item (2) as l_new_item then
											l_new_item.align_text_center
										end
										l_account.set_grid_xy (1, l_new_row.index)
									else
								--		report ("No item found for x,y of parent account...%N")
									end
								else
								--	report ("No attached parent ...%N")
								end
							end
						end
--===
					end
					l_tree.forth
				end
			end


		end

-- Duplicated code from EAC_ACCOUNT_WINDOW:
	account_key_string (a_account: EAC_ACCOUNT; a_account_table: HASH_TABLE [EAC_ACCOUNT, STRING]): STRING
			-- Generate key based on a_account and all ancestors thereof
		local
			l_cursor: like eac_data.eac_accounts.new_cursor
			l_accounts: like a_account_table
			l_account: EAC_ACCOUNT
			done: BOOLEAN
		do
			create Result.make_empty
			l_account := a_account
			from
				l_accounts := a_account_table
			until
				done
			loop
				if attached l_account as l then
					Result.prepend (l.attached_name)
					l_accounts.search (l.attached_parent_id)
					if l_accounts.found then
						Result.prepend (" :: ")
						l_account := l_accounts.found_item
					else
						done := True
					end
				end
			end
		end


invariant
	valid_account_id: attached selected_account_id as id implies {UUID}.is_valid_uuid (id)
end
