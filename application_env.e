note
	description: "{
Environment-specific values and routines
}"
	system: "Amalasoft Eiffel LIbrary"
	source: "Amalasoft Corporation"
	license: "Eiffel Forum V2"
	copyright: "Copyright (c) 2012 Amalasoft Corporation.  All Rights Reserved"
	date: "$Date: 2012/07/04 $"
	revision: "$Revision: 001$"
	howto: "See comments at bottom of class."
	history: "See comments at bottom of class."


class APPLICATION_ENV

inherit
	AEL_V2_ENV
		redefine
			application_root_type
		end

--|========================================================================
feature {NONE} -- Common core
--|========================================================================

	--| application db, for example

	application_root_type: APPLICATION_ROOT
		do
			check False then end
		end

	application: APPLICATION_ROOT
		do
			Result := application_root
		end

	env_implementation: APPLICATION_ENV_IMP
		once
			create Result
		end

--|========================================================================
feature -- Log and debug
--|========================================================================

	log_and_print (fmt: STRING; args: detachable ANY)
			-- Write formatted message to log and to output stream
		local
			msg: STRING
		do
			msg := aprintf (fmt, args)
			logit (0, msg)
--			if not debug_to_log then
--				print (msg)
--			end
		end

--|========================================================================
feature {NONE} -- Shared constants
--|========================================================================

	--| Application-specific constants, onces, etc

--|----------------------------------------------------------------------
--| History
--|
--| 001 04-July-2012
--|     Created original module (for Eiffel 7.1)
--|----------------------------------------------------------------------
--| How-to
--|
--| This class is inherited by AEL_V2_ENV.
--| Add features here that are common to multiple classes within the
--| application.
--| DO NOT put -**attributes** here as they will be duplicated in each 
--| descendent!
--|----------------------------------------------------------------------

end -- class APPLICATION_ENV
