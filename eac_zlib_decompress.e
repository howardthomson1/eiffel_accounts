note
	description: "Summary description for {EAC_ZLIB_DECOMPRESS}."
	author: "Howard Thomson"
	date: "28-Dec-2020"

	TODO: "[
		Use agents for selecting read and write actions ...
		Use agents to select algorith alternatives gzip/inflate
	]"

class
	EAC_ZLIB_DECOMPRESS

inherit
	XML_INPUT_STREAM

	EAC_REPORTER
create
	make

feature -- Creation routines -- provide source I/O Medium

	make
		do
			name := "Undefined_name"
				-- Initialize zlib for decompression
				-- Initialize zstream structure, inflate state
			create zstream.make
			zstream.set_available_input (0)
			create zlib

			  	-- Setup a default chunk size
			chunk_size := default_chunk
				-- Initialize buffers
			create input_buffer.make_from_array  (create {ARRAY[NATURAL_8]}.make_filled (0, 1, chunk_size))
			create output_buffer.make_from_array (create {ARRAY[NATURAL_8]}.make_filled (0, 1, chunk_size))

			zlib.inflate_init (zstream)
			check zlib.last_operation >= 0 end
		end

feature -- I/O assignment and setup

	In_medium: INTEGER = 1
	In_memory: INTEGER = 2
	In_string: INTEGER = 3

	Out_medium: INTEGER = 1
	Out_memory: INTEGER = 2
	Out_string: INTEGER = 3

	input_select: INTEGER
		-- 0 => Unset
		-- 1 => From an IO_MEDIUM
		-- 2 => From a MANAGED_POINTER
		-- 3 => From a STRING

	output_select: INTEGER
		-- 0 => Unset
		-- 1 => To an IO_MEDIUM
		-- 2 => To a MANAGED_POINTER
		-- 3 => To a STRING

	input_medium: detachable IO_MEDIUM

	set_input_medium (a_medium: IO_MEDIUM)
		do
			input_medium := a_medium
			input_select := In_medium
		end

	input_memory: detachable MANAGED_POINTER

	set_input_memory (a_memory: MANAGED_POINTER)
		do
			input_memory := a_memory
			input_select := In_memory
		end

	input_string: detachable READABLE_STRING_32

	set_input_string (a_string: READABLE_STRING_32)
		do
			input_string := a_string
			input_select := In_string
		end

	output_medium: detachable IO_MEDIUM

	set_output_medium (a_medium: IO_MEDIUM)
		do
			output_medium := a_medium
			output_select := 1
		end

	output_memory: detachable MANAGED_POINTER

	set_output_memory (a_memory: MANAGED_POINTER)
		do
			output_memory := a_memory
			output_select := 2
		end

	output_string: detachable STRING_32

	set_output_string (a_string: READABLE_STRING_32)
		do
			output_string := a_string
			output_select := 3
		end

	set_process_push
		do

		end

	set_process_pull
		do

		end

	process
			-- Inflate from assigned input to assigned output
		do
			check false end
		end





feature -- Implementation of XML_INPUT_STREAM		

	name: READABLE_STRING_32

	set_name (a_name: READABLE_STRING_32)
		do
			name := a_name
		end

	end_of_input: BOOLEAN

	is_open_read: BOOLEAN
			-- Can items be read from input stream?
		do
			Result := input_select /= 0
		end

	index: INTEGER
			-- Current position in the input stream

	line: INTEGER
			-- Current line number

	column: INTEGER
			-- Current column number

	last_character_code: NATURAL_32
			-- Last read character

	read_character_code
			-- Read a character's code
			-- and keep it in `last_character_code'
		do
			report ("read_character_code called ...")
			read_character_code_implementation
			index := index + 1
		end

feature	{NONE}

	last_read_index: INTEGER

	read_character_code_implementation
			-- XML_INPUT_STREAM implementation of read_character-code
			-- Read one character and assign to last_character_code
		require
			input_assigned: is_open_read
		local
			l_avail: INTEGER
		do
				-- If there is data to collect from the zlib output buffer,
				-- collect from there, else ensure the zlib input buffer is
				-- not empty and run inflate; collect data from output buffer.

				-- Check if there is data to collect in the output buffer
			l_avail := (output_buffer.count - zstream.available_output) - last_read_index
			if l_avail > 0 then
				check false end -- TEMP
				last_character_code := output_buffer.read_natural_8 (last_read_index)
				last_read_index := last_read_index + 1
			else
					-- Update output
				last_read_index := 0
				zstream.set_next_output (output_buffer.item)
				zstream.set_available_output (output_buffer.count)
				if zstream.available_input = 0 then
					read
					zstream.set_next_input (input_buffer.item)
					zstream.set_available_input (last_read_elements)
					check zstream.available_input /= 0
					or else end_of_input end
				end
				zlib.inflate (zstream,  {ZLIB_CONSTANTS}.Z_no_flush)
				check
					available_output: (output_buffer.count - zstream.available_output) - last_read_index > 0
				end
				last_character_code := output_buffer.read_natural_8 (last_read_index)
				last_read_index := last_read_index + 1
			end
			trace_collected_output
		ensure
			not end_of_input -- TEMP!
		end

	trace_collected_output
		local
			c: CHARACTER_8
		do
--			if not attached trace_string then
--				create trace_string.make_empty
--			end
			c := last_character_code.to_character_8
--			if attached trace_string as s then
--				s.extend (c)
--				if c = '%N' then
--					report (s)
--					s.wipe_out
--				end
--			end
			report ("trace_collected_output: " + c.out + "%N")
		end

	trace_string: detachable STRING_32


--------------------

	read
		require
			input_selected: input_select /= 0
		do
			inspect input_select
			when In_medium then read_medium
			when In_memory then read_memory
			when In_string then read_string
			end
		end

	write (a_amount: INTEGER)
		require
			output_selected: output_select /= 0
		do
			inspect output_select
			when Out_medium then write_medium (a_amount)
			when Out_memory then write_memory (a_amount)
			when Out_string then write_string (a_amount)
			end
		end

	read_medium
		do
			if attached input_medium as l_input_medium then
				last_read_elements := io_medium_read (l_input_medium)
			end
		end

	write_medium (a_amount: INTEGER)
			-- <Precursor>
		do
			if attached output_medium as l_medium then
				last_write_elements := io_medium_write (output_buffer, a_amount, l_medium)
			end
		end

	read_memory
		do
			check false end
		end

	write_memory (a_amount: INTEGER)
		require
			attached_memory: attached input_memory
		do
			check false end
		end

	read_string
		do
			check false end
		end

	write_string (a_amount: INTEGER)
		require
			attached_input_string: attached input_string
		do
			check false end
		end

	is_connected: BOOLEAN
		do
			Result := is_open_read
		end


	close
		do
			if attached input_medium as l_medium then
				l_medium.close
				input_medium := Void
			end
			if attached output_medium as l_medium then
				l_medium.close
				output_medium := Void
			end
			input_memory := Void
			input_string := Void
			output_memory := Void
			output_string := Void
		end

	io_medium_read (a_medium: IO_MEDIUM): INTEGER
			-- Read the a_string by character until end of string or the number of elements (Chunk) was reached.
			-- Return the number of elements read.
		local
			l_index: INTEGER
			l_string: STRING
		do
			a_medium.read_stream (chunk_size)
			l_string := a_medium.last_string
			from
				l_index := 1
			until
				l_index > l_string.count
			loop
				input_buffer.put_character (l_string.at (l_index), l_index - 1)
				l_index := l_index + 1
			end
			if l_string.count < chunk_size then
				end_of_input := True
			end
			Result := l_index - 1
		end

	io_medium_write  (a_buffer: MANAGED_POINTER; a_amount: INTEGER; a_dest: IO_MEDIUM): INTEGER
		local
			l_index: INTEGER
		do
			from
				l_index := 1
			until
				l_index > a_amount or l_index > a_buffer.count
			loop
				a_dest.put_character (a_buffer.read_character (l_index - 1))
				l_index := l_index + 1
			end
			Result := l_index - 1
		end

feature {NONE} -- Implementation

	last_read_elements: INTEGER
			-- Number of elements read by `read'.

	last_write_elements: INTEGER
			-- Number of elements written by `write'.

--------------------

-----------

	input_buffer: MANAGED_POINTER
		-- Input buffer.

	output_buffer: MANAGED_POINTER
		-- Output buffer

	chunk_size: INTEGER
		 -- the buffer size for feeding data to and pulling data from the zlib routines.

	default_chunk: INTEGER = 16384
		 -- default buffer size	

	zlib: ZLIB
		-- ZLIB Low level API

	zstream: ZLIB_STREAM
		-- structure used to pass information to zlib routines	




invariant
	input_select = 1 implies attached input_medium
	input_select = 2 implies attached input_memory
	input_select = 3 implies attached input_string
	output_select = 1 implies attached output_medium
	output_select = 2 implies attached output_memory
	output_select = 3 implies attached output_string
-- Unstable:
	last_read_index >= 0 and last_read_index <= output_buffer.count

end
