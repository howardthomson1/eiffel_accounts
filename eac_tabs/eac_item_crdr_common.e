note
	description: "Common ancestor to EAC_ITEM_TR_CREDIT / DEBIT"
	author: "Howard Thomson"
	date: "$17-Sep-2021$"
	revision: "$Revision$"

	TODO: "[
		Maybe, make the initial '�' sign not part of the editable text ?
		When entering number into credit field, redraw debit field, and vice-versa
	]"

deferred class
	EAC_ITEM_CRDR_COMMON

inherit
	EAC_ITEM_TR_COMMON
		redefine
			max_text_size,
			key_press_string_event,
			update_field
		end

	EAC_PRINT
		undefine default_create, copy end

feature -- Creation

	make (a_transaction: EAC_TRANSACTION; a_transactions_window: EAC_TRANSACTIONS_WINDOW)
		do
			transaction := a_transaction
			transactions_window := a_transactions_window
			default_create
			expose_actions.extend (agent custom_expose (?, Current))
			add_events
			set_right_aligned
			set_required_width (exposure_font.string_width (text) + 10)
			set_is_tab_navigatable (True)	-- Move ?
		end

feature

	max_text_size: INTEGER
		once
			Result := ("�1,000,000,000").count
		end

	key_press_string_event (a_string: STRING_32)
			-- Permit either a digit or a '.' to be added to the
			-- current numneric string.
			--TODO?: Check if the resultant string would then
			-- be invalid ?
		local
			c: CHARACTER_32
		do
			c := a_string.item (1)

--			if c = '.' then
--				if not  then
--					
--				end
--				Precursor (a_string)
--			elseif c.is_digit then
--				
--				Precursor (a_string)
--			end
--			
			if c.is_digit or else c = '.' then
				Precursor (a_string)
			end
		end

	valid_text (a_string: STRING): BOOLEAN
			-- Is 'a_string' valid content for this field ?
		local
			s: STRING
			i: INTEGER
		do
			report ("valid_text (" + a_string + "): ")
			if a_string.count = 0 then
				Result := False
			else
				create s.make_from_string (a_string)
				s.left_adjust
				s.right_adjust
				if s.item (1) = '�' then
					s.remove_head (1)
					s.left_adjust
				end
				s.replace_substring_all (",", "")
					-- Either there is no '.' or
					-- the number of digits after the '.' does not exceed 2
				if s.has ('.') then
					i := s.index_of ('.', 1)
					if i >= s.count - 2 then
						Result := True
					end
				else
					check s.is_number_sequence end
					Result := True
				end
			end
			report_nl (Result.out)
		end

	update_field (a_string: STRING)
			-- Update the associated record from this text
		local
			s: STRING
			i64: INTEGER_64
			i: INTEGER
		do
			if a_string.count = 0 then
				-- Do nothing
			else
				create s.make_from_string (a_string)
				s.left_adjust
				s.right_adjust
				if s.item (1) = '�' then
					s.remove_head (1)
					s.left_adjust
				end
				s.replace_substring_all (",", "")
				if s.has ('.') then
					from
						i := s.count - s.index_of ('.', 1)
					until
						i = 2
					loop
						s.append_character ('0')
						i := i + 1
					end
					s.replace_substring_all (".", "")
				else
					s.append ("00")
				end
				if s.is_integer_64 then
					i64 := s.to_integer_64
					check i64 /= 0 end
					transaction.set_value (i64)
					update_dr_cr
					transactions_window.assign_current_edit_text (Current)
					transactions_window.update_balances_from (current.row.index)
				end
			end
		end

	update_dr_cr
			-- Update debit_account and credit_account
			-- to match this column entry
		deferred
		end

end
