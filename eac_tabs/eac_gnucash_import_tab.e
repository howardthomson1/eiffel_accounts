note
	description: "Summary description for {EAC_GNUCASH_IMPORT_TAB}."
	author: "Howard Thomson"
	date: "27-May-2020"
	TODO: "[
		Enclose the transactions grid in an EV_VERTICAL_BOX (?), below a summary box for the selected
		account, and selection boxes for start/end date of displayed transactions to import.
	]"

class
	EAC_GNUCASH_IMPORT_TAB

inherit
	EV_HORIZONTAL_SPLIT_AREA
		redefine
			create_interface_objects,
			initialize
		end

create
	default_create

feature {NONE}

	create_interface_objects
		do
			create accounts_grid.make
			create transactions_box
		end

	initialize
		do
			Precursor
			extend (accounts_grid)
			extend (transactions_box)
--			accounts_grid.disable_item_tab_navigation
		end

feature

	accounts_grid: EAC_ACCOUNTS_WINDOW

	transactions_box: EAC_GNUCASH_IMPORT_BOX

	set_parent_notebook (a_parent: EV_NOTEBOOK)
		require
			parent_not_void: a_parent /= Void
		do
			a_parent.extend (Current)
			a_parent.set_item_text (Current, "Accounts")
		end


end
