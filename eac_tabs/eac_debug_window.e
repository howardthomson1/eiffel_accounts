note
	description: "EAC -- Debug monitor window"
	author: "Howard Thomson"
	date: "11-May-2021"

class
	EAC_DEBUG_WINDOW

inherit
	EV_TITLED_WINDOW

create
	make

feature

	make
		do
			create vbox
			extend_boxes
			default_create
			set_title ("EAC Debug Window -- Transactions")
			extend (vbox)
		end

feature -- Attributes

	vbox: EV_VERTICAL_BOX

feature -- Update routine(s)

	extend_boxes
		local
			i: INTEGER
			l: EV_LABEL
		do
			from i := 1
			until i > 10
			loop
				create l.make_with_text ("Default text ...")
				vbox.extend (l)
				l.align_text_left
				i := i + 1
			end
		end

	update (a_window: EAC_TRANSACTIONS_WINDOW)
		local
			t: STRING
		do
			update_ith (1, "Current edit text: ", a_window.current_edit_text)
			update_ith (2, "Current edit position: ", a_window.current_edit_position.out)
			if attached a_window.current_edit_field as f then
				t := f.column.index.out + " " + f.row.index.out
			else
				t := "Void"
			end
			update_ith (3, "Current field position: ", t)
			if attached a_window.current_edit_transaction as tx then
				t := tx.id
			else
				t := "Void"
			end
			update_ith (4, "Current transaction id: ", t)

			if attached a_window.current_edit_transaction as tx then
				t := tx.is_valid_for_commit.out
			else
				t := "Void"
			end
			update_ith (5, "Is valid for commit: ", t)
			if attached a_window.current_edit_transaction as tx then
				t := tx.is_in_default_state.out
			else
				t := "Void"
			end
			update_ith (6, "Is in default state: ", t)
		end

	update_ith (i: INTEGER; a_label: STRING; a_text: STRING)
		local
			l_fill: STRING
		do
			if a_label.count < 25 then
				create l_fill.make_filled (' ', 25 - a_label.count)
			else
				l_fill := ""
			end
			if attached {EV_LABEL} vbox.i_th (i) as l then
				l.set_text (a_label + l_fill + a_text)
			end
		end

end
