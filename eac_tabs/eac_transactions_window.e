note
	description: "Summary description for {EAC_TRANSACTIONS_WINDOW}."
	author: "Howard Thomson"
	date: "13-May-2020"

	TODO: "[
		Incomplete/invalid field coloured red when not the current field
			Coloured green ahen valid/complete, prior to data commit
			Neutral colour for current edit field
		Resolve incomplete editing of an already stored transaction
			Check failure in fill_grid
		Tab at end of 'new'transaction commits the transaction' if valid ...
		Move current field editing to custom field classes.
			Class specific editing for date, account, currency ...
		Initial cursor position at end of text ?
		Need cursor to be visible between left hand edge and start of text
		Date format choice yyyy-mm-dd vs dd-mm-yyyy, or even US format !
		
		Commit update on current edit transaction on leaving the last field
			Check validity and commit processing before selecting next field

		Delete entries -- Update balances elsewhere [Accounts window ...]
		Exclude the 'new' row from the 'selected' items
		Popup menu changes, dynamic adaptation, popup position+cursor position
		Highlight 'imported' transactions, prior to commit
		Edit existing entries 'Edit ...'
		Add new entries 'Add ...'
		Multiple selection move to another account 'Move ...'
		Jump 'Jump'


		Two Description columns !
		Fix 'date' ordering: INTEGER.out will not do!

		Initial copy from EAC_GNUCASH_IMPORT_TAB
		Use colour to distinguish 'types' of displayed transaction ...

		'SPLIT' transactions need further support ...
			Use tree option iff the current display has a split transaction

		Blank cells need explicit " " items to make selection work ...

		Re-think running totals ? [Different sort order / edited records ...]

		Selection: Maybe select field for edit only after selecting a transaction ?
		
		Distinct background colour for uncomitted import transactions.
	]"

	Keyboard_mouse_sequences: "[
		Entry into a field for edit happens as a result of:
			Default on window mapping
			Mouse click selection
			Keyboard Tab or Return
		Exit from the 'current' field

		Note that I need restore capability for all data fields, not just the
		one currently being edited ...
		Take a copy of the transaction for restore ?
	]"


class
	EAC_TRANSACTIONS_WINDOW

inherit
	EAC_GRID_COMMON
--		redefine
--			initialize
--		end
	EV_KEY_CONSTANTS
		undefine default_create, copy end

create
	make

feature -- Creation

	make
		do
			create running_balances.make_empty
			create current_edit_text.make_empty
			current_edit_position := 1
			create debug_window.make
			create account_selector_dialogue
			default_create
			make_common
			enable_multiple_row_selection
			disable_selection_key_handling
		--	disable_selection_on_click
		--	enable_item_tab_navigation
			setup_start_end_dates

--			key_press_actions.extend (agent key_press)
			set_focus
			debug_window.show
		end

--feature {EV_ANY}

--	initialize
--		do
--			Precursor
----			set_default_key_processing_handler (Void)
----			implementation.drawable.set_default_key_processing_handler (Void)
--		end

feature -- Constants

	col_date		: INTEGER = 1
	col_num			: INTEGER = 2
	col_description	: INTEGER = 3
	col_account		: INTEGER = 4
	col_xxx			: INTEGER = 5
	col_debits		: INTEGER = 6
	col_credits		: INTEGER = 7
	col_balance		: INTEGER = 8

feature -- Attributes

	running_balances: ARRAY [ INTEGER_64 ]
		-- Sequential balance figres for the currently assigned account

	current_edit_transaction: detachable EAC_TRANSACTION
		-- The transaction currently being created/updated

	current_edit_field: detachable EAC_ITEM_TR_COMMON
		-- The current editable field

	next_edit_field: detachable EAC_ITEM_TR_COMMON
		-- If not Void then the new attachment for current_edit_field
		-- after completion of leave processing for the current field

	current_edit_text: STRING
		-- Text currently being 'edited'

	current_edit_position: INTEGER
		-- Current cursor position in 'curent_edit_text

--	current_edit_pattern: detachable STRING
--		-- Pattern constraint on current edit field

	current_import_tag: detachable STRING


	current_import_from_date: detachable DATE
	current_import_end_date: detachable DATE
		-- Date filter for current 'import' data

	debug_window: EAC_DEBUG_WINDOW

	account_selector_dialogue: EAC_ACCOUNT_SELECTION_DIALOG

feature {EAC_ITEM_TR_COMMON} -- Attribute setters

	set_current_edit_position (a_position: INTEGER)
		do
			current_edit_position := a_position
		end

feature -- Setup

	setup_columns
		do
			insert_new_column (Col_date)
			insert_new_column (Col_num)
			insert_new_column (Col_description)
			insert_new_column (col_account)
			insert_new_column (col_xxx)
			insert_new_column (col_debits)
			insert_new_column (col_credits)
			insert_new_column (col_balance)
				-- Assign corresponding headings
			column (Col_date).set_title ("Date ")
			column (Col_num).set_title ("Num ")
			column (Col_description).set_title ("Description ")
			column (col_account).set_title ("Account ")
			column (col_xxx).set_title ("??? ")
			column (col_debits).set_title ("Debits ")
			column (col_credits).set_title ("Credits ")
			column (col_balance).set_title ("Balance ")

				-- Set column heading alignments
			column (Col_date).header_item.align_text_center
			column (Col_num).header_item.align_text_center
			column (Col_description).header_item.align_text_left
			column (col_account).header_item.align_text_right
			column (col_xxx).header_item.align_text_right
			column (col_debits).header_item.align_text_right
			column (col_credits).header_item.align_text_right
			column (col_balance).header_item.align_text_right
				--TODO: Make user selectable
			column (Col_num).hide
			column (Col_xxx).hide
		end

feature --

	application: EV_APPLICATION
			-- Available to test for status of Shift/Ctrl/Alt etc
		once
			create Result
		end

	last_key_press: detachable EV_KEY

	key_press_grid_item (a_key: EV_KEY)
		do
				-- Either the key press modifies the current item,
				-- or it leaves to select a different one ...
				-- or it reverts to previous
			if a_key.is_printable then
				-- Processing moved to 'key_press_string_event' below ...
			else
				last_key_press := a_key
				inspect a_key.code

				when key_back_space, key_delete, key_left, key_right, key_escape then
					if attached current_edit_field as f then
						f.key_press_event (a_key)
					end

-- TODO 'Leave' response to differ if an incomplete record is currently being created ?

				when key_up, key_down then
					setup_next_edit_field (a_key)
			--		if check_leave_current_transaction then
						update_from_current_field
						edit_field (next_edit_field)
			--		end

				when key_tab then
					setup_next_edit_field (a_key)
					if True then --check_leave_current_transaction then	-- ????
						update_from_current_field
						edit_field (next_edit_field)
					end

				when key_enter then
			--		if check_leave_current_transaction then
						update_from_current_field
			--		end
				else
				end
			end

			if attached current_edit_field as f then
				f.redraw
			end
			set_focus
			debug_window.update (Current)
		end

	key_press_string_event (a_string: STRING_32)
		require
			a_string.count = 1
		do
			if attached current_edit_field as f then
				f.key_press_string_event (a_string)
			end
		end

	check_leave_current_transaction: BOOLEAN
			-- Is the current field in an acceptable state to leave it and
			-- move to another, implying updating the associated record ?
		do
			if attached current_edit_transaction as t then
				if t.is_in_default_state then
					Result := True
				elseif t.is_valid_for_commit then
					Result := True
				end
			end
		end

	setup_next_edit_field (a_key: EV_KEY)
			-- Assign to 'next_edit_field' in preparation for making it the
			-- current field, subject to current field validation etc
		local
			r, c: INTEGER
			n: like next_edit_field
			l_row: EV_GRID_ROW
			l_item: EAC_TRANSACTION
		do
			if attached current_edit_field as f then
				inspect a_key.code
				when key_up then
					if f.row.index > 1 then
						if attached {EAC_ITEM_TR_COMMON} item (f.column.index, f.row.index - 1) as nef then
							next_edit_field := nef
						end
					end
				when key_down then
					if f.row.index < row_count then
						if attached {EAC_ITEM_TR_COMMON} item (f.column.index, f.row.index + 1) as nef then
							next_edit_field := nef
						end
					end
				when key_tab then
					if not application.shift_pressed then
							-- Next field to the right, or start of next row
						from r := f.row.index; c := f.column.index; n := Void
						until
							r > row_count or n /= Void
						loop
							if c < column_count then
								c := c + 1
							else
								c := 1; r := r + 1
							end
							if r <= row_count
							and then attached {EAC_ITEM_TR_COMMON} item (c, r) as e
							and then e.is_editable then
								n := e
							end
						end
						next_edit_field := n
						if n = Void and f.row.index = row_count then
								-- Create a new transaction row
							report ("Create new transaction ...%N")


							create l_item
							insert_new_row (row_count + 1)
							l_row := row (row_count)

							l_row.set_item (col_date,        create {EAC_ITEM_TR_DATE}	 	 .make (l_item, Current))
					--		l_row.set_item (col_num,         create {EAC_ITEM_TR_XXX}		 .make (l_item, Current))
							l_row.set_item (col_description, create {EAC_ITEM_TR_DESCRIPTION}.make (l_item, Current))
							l_row.set_item (col_account,     create {EAC_ITEM_TR_ACCOUNT}	 .make (l_item, Current))
					--		l_row.set_item (col_xxx,         create {EAC_ITEM_TR_XXX}		 .make (l_item, Current))
							l_row.set_item (col_debits,      create {EAC_ITEM_TR_DEBIT}		 .make (l_item, Current))
							l_row.set_item (col_credits,     create {EAC_ITEM_TR_CREDIT}	 .make (l_item, Current))
							l_row.set_item (col_balance,     create {EAC_ITEM_TR_BALANCE} 	 .make (l_item, Current))

							update_balances_from (row_count)

							--TODO
						--	Add l_item to uncommitted/incomplete transactions ...	

--=================================================================================================================							
						end
					else
							-- Next field to the left [previous], or end of previous row
						from r := f.row.index; c := f.column.index; n := Void
						until
							r < 1 or n /= Void
						loop
							if c > 1 then
								c := c - 1
							else
								c := column_count; r := r - 1
							end
							if r >= 1
							and then attached {EAC_ITEM_TR_COMMON} item (c, r) as e
							and then e.is_editable then
								n := e
							end
						end
						next_edit_field := n
					end
				when key_enter then
					-- No movement ...
				else
					-- Fall through ...
				end
			end
		end



	update_from_current_field
		do
			if attached current_edit_field as f then
				if f.valid_text (current_edit_text) then
					f.update_field (current_edit_text)
				else
					--TODO ???
				end
			end
		end




	update_status_bar
		do
--			report ("Update status bar ...%N")
			if attached {EAC_MAIN_WINDOW} main_window as m_window then
				m_window.update_status_bar ("Updated ... !")
			end
		end

feature -- Container in which displayed

	set_parent_notebook (a_parent: EV_NOTEBOOK)
		require
			parent_not_void: a_parent /= Void
		do
			a_parent.extend (Current)
			a_parent.set_item_text (Current, "Import Transactions")
		end

feature

--	TODO for mouse_click
--	1/ Distinguish between clicking in the header, on an item, or in black space
--	2/ Customise the popup menu for the above three cases
--	3/ Distinguish left-click / right-click

	mouse_click (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
			--
		local
			l_item: EV_GRID_ITEM
			l_row: EV_GRID_ROW
			l_height: INTEGER
--			l_row: EV_GRID_ROW
		do
			l_height := header.height
			l_item := item_at_virtual_position (a_x, a_y - l_height + (vertical_scroll_bar.value))
			l_row := row_at_virtual_position (a_y - l_height, False)
			if a_button = 1 then
--				if
--					True -- Check for Shift key
--				then
--					l_item.enable_select
				if attached {EAC_ITEM_TR_COMMON} l_item as eac_item then
						-- Action deferred to item specific response ...
					edit_field (eac_item)
				end

			elseif a_button = 3 then
				popup_x := a_x
				popup_y := a_y
				if attached {EAC_ITEM_TR_COMMON} l_item as eac_item then
						-- Action deferred to item specific response ...
					eac_item.mouse_click_3
					edit_field (eac_item)
				else
					popup_menu.show
				end
			end
			debug_window.update (Current)

--			l_row := row (1)
--			if attached l_row as r then
--				r.enable_select
--			end


		end


	item_pointer_press (a_x, a_y, a_screen_x, a_screen_y: INTEGER; a_item: EAC_ITEM_TR_COMMON)
		do
			edit_field (a_item)

			report ("Mouse click in item:%N"
				+ "x: " + a_x.out + " y: " + a_y.out + "%N"
				+ "screen_x: " + a_screen_x.out + " screen_y: " + a_screen_y.out + "%N"
				+ "viewable_y_offset: " + viewable_y_offset.out + "%N"
				+ "scroll bar value: " + vertical_scroll_bar.value.out + "%N"
		--		+ "Edit item x/y: " + eac_item.virtual_y_position
				)

			debug_window.update (Current)
		end

	popup_x, popup_y: INTEGER

	popup_menu: EV_MENU
		local
			l_window: EV_WINDOW
			l_bar: EV_MENU_BAR
			l_menu: EV_MENU
			l_item: EV_MENU_ITEM
		once
			create Result
			l_menu := Result
			create l_item.make_with_text ("New ..."); l_menu.extend (l_item)
--				l_item.select_actions.extend (agent new_transaction)



			create l_item.make_with_text ("Edit ..."); l_menu.extend (l_item)
				l_item.select_actions.extend (agent edit_field)
			create l_item.make_with_text ("Delete ..."); l_menu.extend (l_item)
				l_item.select_actions.extend (agent delete_transactions)
			create l_item.make_with_text ("Resize Columns to Content"); l_menu.extend (l_item)
				l_item.select_actions.extend (agent resize_columns)
		end

	edit_field (a_field: detachable EAC_ITEM_TR_COMMON)
		local
			l_beep: EV_BEEP
		do
			report ("edit_field called ...%N")
			if attached {EAC_ITEM_TR_COMMON} a_field as l then
				if attached current_edit_field as e
				and then e /= a_field
				then
					report ("Making current edit field Void ...%N")
					current_edit_field := Void
					current_edit_text.make_empty
					e.redraw
				end
				if l.is_editable then
					report ("Assigning new edit field ...%N")

					if current_edit_field /= l then
						create l_beep; l_beep.ok
					end
					current_edit_field := l
					current_edit_transaction := a_field.transaction
					assign_current_edit_text (l)
					l.redraw
				end
			end
		end

	assign_current_edit_text (a_field: EAC_ITEM_TR_COMMON)
		do
			create current_edit_text.make_from_string (a_field.text)
			current_edit_position := 1 + current_edit_text.count
		end

	selected_transactions: ARRAYED_LIST [ EAC_TRANSACTION ]
		do
			-- TODO
			create Result.make (selected_rows.count)
		end

	delete_transactions
			-- Delete selected transactions
			--
			-- TODO: Add selected transactions to a local array ... [Use 'selected_transactions' ...]
			--	Check for each item that deletion is permitted
			--	Process the deletions
			--	Update the display ... [Transactions, and account balances]
			-- Separate transaction deletion from row removal ?
		local
			l_selected_rows: ARRAYED_LIST [ EV_GRID_ROW ]
			i, n: INTEGER
			r: EV_GRID_ROW
		do
			l_selected_rows := selected_rows
			n := l_selected_rows.count
			from i := 1
			until i > n
			loop
				r := l_selected_rows.i_th (i)
				if attached {EAC_ITEM_TR_COMMON} r.item (col_date) as l_item
				then
					report ("Transaction to delete found ...%N")
						-- Delete the transaction 't' from the data store
					delete_a_transaction (l_item.transaction)
						-- Remove the row
					remove_row (r.index)
				end
				i := i + 1
			end
				-- Update the display
				--	Transactions balances
			update_balances
				--	Accounts balances
			--TODO
		end

	delete_a_transaction (a_transaction: EAC_TRANSACTION)
		local
			l_transactions: like eac_data.eac_transactions
		do
			l_transactions := eac_data.eac_transactions
			l_transactions.search (a_transaction.id)
			if l_transactions.found then
				check False end
				--TODO: mark as 'deleted' to then synchronize with the DB Server ...

--				l_transactions.remove (a_transaction.id)
			else
					-- Transaction not found
				check False end
			end
		end



	file_dialog: EV_FILE_OPEN_DIALOG
		once
			create Result
		end

	resize_columns
			-- TODO: Resize to min(sizeof_content, sizeof_heading): DONE ?
			-- Leftmost and rightmost column resizing does not work correctly ...
		local
			i: INTEGER
			c: EV_GRID_COLUMN
			w, rw, cw, crw: INTEGER
		do
			from i := 1 until i > 8 loop
				c := column (i)
				w := c.width
				rw := c.required_width_of_item_span (1, row_count)
					+ {EAC_ITEM_TR_COMMON}.left_margin + {EAC_ITEM_TR_COMMON}.right_margin
				cw := c.header_item.width
				crw := c.header_item.minimum_width
				c.set_width (w.max (rw.max (cw.max (crw))))
				i := i + 1
			end
		end

	date_format_string: STRING = "yyyy-[0]mm-[0]dd";

	current_account: detachable EAC_ACCOUNT

	fill_grid (a_account: EAC_ACCOUNT; a_transactions: HASH_TABLE [EAC_TRANSACTION, STRING])
		local
			l_tree: DS_AVL_TREE [EAC_TRANSACTION, STRING]
			l_comparator: EAC_ACCOUNT_COMPARATOR
			l_account_id: STRING
			l_transactions: HASH_TABLE [EAC_TRANSACTION, STRING]
			l_item: EAC_TRANSACTION
			l_key_string: STRING
			l_item_found: BOOLEAN
			l_transaction_account: STRING
			l_debits_total, l_credits_total: INTEGER_64
			l_balance: INTEGER_64
			l_account_name: STRING
			l_row: EV_GRID_ROW
			i: INTEGER
			l_display_date: DATE
			l_first_date, l_last_date: DATE
			is_debit: BOOLEAN
			is_split: BOOLEAN
			l_formatter: KL_STRING_OUTPUT_STREAM

			l_grid_item: EV_GRID_DRAWABLE_ITEM
		do
			current_account := a_account
			current_edit_field := Void
			current_edit_position := 1
			create l_display_date.make_now
			create l_key_string.make_empty
			create l_formatter.make (l_key_string)
			create l_first_date.make_now
			create l_last_date.make_now
			if row_count > 0 then
				remove_rows (1, row_count)
			end
			create l_comparator
			create l_tree.make (l_comparator)
			l_account_id := a_account.id
			l_transactions := a_transactions
				-- Populate the tree with transactions linked to the specified account
--============================================================================================
-- TODO: Changes needed for 'group' transactions ...
-- First: Identify a group transaction
-- What gets added to the tree ?

			from
				l_transactions.start
			until
				l_transactions.after
			loop
				l_item := l_transactions.item_for_iteration
--TODO: The check below is failing, immediately after GnuCash import, as of 22-2-2022
				l_item.check_is_valid_for_commit	--.is_valid_for_commit end
				is_split := False
-- ======================================= Type Group =============================================
				if attached l_item.split_group then
						-- Type-group
					is_split := True
				end

		--		else
						-- Type_basic
	 					-- Check for matching Debit Account
					if attached l_item.debit_account as l_dr
					and then l_dr.is_equal (l_account_id) then
						if attached l_item.date as l_date then
								-- Set Key String
							setup_key_string (l_key_string, l_date, l_item.id)
							l_item_found := True
						end
					end
						-- Check for matching Credit Account
					if attached l_item.credit_account as l_cr
					and then l_cr.is_equal (l_account_id) then
						if attached l_item.date as l_date then
								-- Set Key String
							setup_key_string (l_key_string, l_date, l_item.id)
							l_item_found := True
						end
					end
					if l_item_found then
						--======================================================TODO

						if l_tree.has (l_key_string) then
							report ("BUG: Conflicting key for AVL_TREE !%N")
				--			report_avl_tree_keys (l_tree)
							check false end
						else
							l_tree.put_new (l_item, l_key_string.twin)
						end
						l_item_found := False
					end
		--		end
				l_transactions.forth
			end
				-- In sorted order from the tree, populate the grid
				-- Track the balance en-route
			from
				l_tree.start
				l_balance := 0
				i := 1
			until
				l_tree.after
			loop
				l_item := l_tree.item_for_iteration
				is_split := False
				if attached l_item.split_group then
					is_split := True
				end
					-- Check whether this transaction is either already 'committed'
					-- or falls within the import date window
				if display_transaction (l_item) then
					insert_new_row (i)
					l_row := row (i)

					l_row.set_item (col_date,        create {EAC_ITEM_TR_DATE}	 	 .make (l_item, Current))
			--		l_row.set_item (col_num,         create {EAC_ITEM_TR_XXX}		 .make (l_item, Current))
					l_row.set_item (col_description, create {EAC_ITEM_TR_DESCRIPTION}.make (l_item, Current))
					l_row.set_item (col_account,     create {EAC_ITEM_TR_ACCOUNT}	 .make (l_item, Current))
			--		l_row.set_item (col_xxx,         create {EAC_ITEM_TR_XXX}		 .make (l_item, Current))
					l_row.set_item (col_debits,      create {EAC_ITEM_TR_DEBIT}		 .make (l_item, Current))
					l_row.set_item (col_credits,     create {EAC_ITEM_TR_CREDIT}	 .make (l_item, Current))
					l_row.set_item (col_balance,     create {EAC_ITEM_TR_BALANCE} 	 .make (l_item, Current))

						-- Track earliest and latest date for the transactions
					if i = 1 then
						l_first_date.set_internal_ordered_compact_date (l_item.date)
						l_last_date.set_internal_ordered_compact_date (l_item.date)
					else
						if l_item.date < l_first_date.ordered_compact_date then
							l_first_date.set_internal_ordered_compact_date (l_item.date)
						end
						if l_item.date > l_last_date.ordered_compact_date then
							l_last_date.set_internal_ordered_compact_date (l_item.date)
						end
					end
				end


				l_tree.forth
				i := i + 1
			end
				-- New 'editable transaction as the bottom row of the display ...
				-- Retrieve incomplete transactions ...
			create l_item
			check newly_created_transaction_not_valid_for_commit: not l_item.is_valid_for_commit end
			l_item.set_type (l_item.type_basic)
			l_item.set_debit_account (a_account.id)
			current_edit_transaction := l_item
			insert_new_row (row_count + 1)
			l_row := row (row_count)
			l_row.set_item (col_date,        create {EAC_ITEM_TR_DATE}	 	 .make (l_item, Current))
	--		l_row.set_item (col_num,         create {EAC_ITEM_TR_XXX}		 .make (l_item, Current))
			l_row.set_item (col_description, create {EAC_ITEM_TR_DESCRIPTION}.make (l_item, Current))
			l_row.set_item (col_account,     create {EAC_ITEM_TR_ACCOUNT}	 .make (l_item, Current))
	--		l_row.set_item (col_xxx,         create {EAC_ITEM_TR_XXX}		 .make (l_item, Current))
			l_row.set_item (col_debits,      create {EAC_ITEM_TR_DEBIT}		 .make (l_item, Current))
			l_row.set_item (col_credits,     create {EAC_ITEM_TR_CREDIT}	 .make (l_item, Current))
			l_row.set_item (col_balance,     create {EAC_ITEM_TR_BALANCE} 	 .make (l_item, Current))



				-- Update fields in adjacent box ...
				-- 	Debits total
				-- 	Credits total
				-- 	First date
				-- 	Last date
			if attached {EAC_GNUCASH_IMPORT_BOX} parent as p then
				if l_tree.count > 0 then
					p.date_from.set_text (l_first_date.formatted_out (date_format_string))
					p.date_to.set_text (l_last_date.formatted_out (date_format_string))
					p.credits_total.set_text (formatted_currency_string (l_credits_total))
					p.debits_total.set_text (formatted_currency_string (l_debits_total))
						-- No. of items ...
				else
					p.date_from.set_text ("")
					p.date_to.set_text ("")
					p.credits_total.set_text ("")
					p.debits_total.set_text ("")
				end
			end
			update_balances
-- TODO: Ensure that 'set_required_width' has been called for each EV_GRID_DRAWABLE_ITEM
			resize_columns
			set_focus
		end

--	check_stored_item_is_valid (a_item:EAC_TRANSACTION)
--		do
--			if not a_item.is_valid_for_commit then
--				report ("is_valid_for_commit false:%N")
--				report ("has_valid_type: " + a_item.has_valid_type.out +"%N")
--				report ("date /= 0: " + (a_item.date /= 0).out + "%N")
--				report ("(type = Type_basic implies (debit_account /= Void and credit_account /= Void)): "
--					+ (a_item.type = a_item.Type_basic implies (a_item.debit_account /= Void and a_item.credit_account /= Void)).out
--					+ "%N")
--				report ("(type /= Type_Basic implies (debit_account = Void or else credit_account = Void)): "
--					+ (a_item.type /= a_item.Type_Basic implies (a_item.debit_account = Void or else a_item.credit_account = Void)).out
--					+ "%N")
--				report ("(is_group_type implies split_group /= Void): "
--					+ (a_item.is_group_type implies a_item.split_group /= Void).out
--					+ "%N")
--				report ("(id /= Void implies {UUID}.is_valid_uuid (id)): "
--					+ (a_item.id /= Void implies {UUID}.is_valid_uuid (a_item.id)).out
--					+ "%N")
--				report ("(value /= 0): "
--					+ (a_item.value /= 0).out
--					+ "%N")
--			end
--		end

feature -- Import data constaints


	setup_start_end_dates
			-- TEMP: Need a dialog to assign/remove these dates ...
		do
--			create current_import_end_date.make_day_month_year (5, 4, 2019)
		end


--TODO: Assign current_import_tag on import, set Void on Commit

	display_transaction (a_transaction: EAC_TRANSACTION): BOOLEAN
			-- Is a_transaction to be displayed ?
			-- TODO: Maybe exclude from balances etc, but display in difference colour ...
		do
--			if attached current_import_tag as ct
--			and then attached a_transaction.import_tag as tt
--			and then ct.is_equal (tt)
--			then
			if True then
				Result := True
				if attached current_import_from_date as d then
					if a_transaction.date < d.ordered_compact_date then
						Result := False
					end
				end
				if attached current_import_end_date as d then
					if a_transaction.date > d.ordered_compact_date then
						Result := False
					end
				end
			else
				Result := True
			end
		end

--	set_item_required_width (a_grid_item: EV_GRID_DRAWABLE_ITEM; a_column: INTEGER)
--		do
--			-- TODO
--		end


     close_2 (p: PROCEDURE [EV_DRAWABLE, EV_GRID_DRAWABLE_ITEM]; a: EV_GRID_DRAWABLE_ITEM): PROCEDURE [EV_DRAWABLE]
             -- Close argument #2 of `p` with `a`.
         do
             Result := agent curry (p, ?, a)
         end

     curry (p: PROCEDURE [EV_DRAWABLE, EV_GRID_DRAWABLE_ITEM]; a1: EV_DRAWABLE; a2: EV_GRID_DRAWABLE_ITEM)
             -- Call `p` with arguments `a1` and `a2`.
         do
             p (a1, a2)
         end

	setup_key_string (a_key_string: STRING; a_date: INTEGER; an_id: STRING)
			-- Setup Key String
		local
			l_stream: KL_STRING_OUTPUT_STREAM
			l_hex_string: STRING
		do
			a_key_string.wipe_out
			create l_hex_string.make (10)
			create l_stream.make (l_hex_string)
			{UT_INTEGER_FORMATTER}.put_hexadecimal_integer (l_stream, a_date, False)
			if l_hex_string.count < 8 then
				l_hex_string.prepend ("00000000")
				l_hex_string.keep_tail (8)
			end
			a_key_string.append (l_hex_string)
			a_key_string.append ("--")
			a_key_string.append (an_id)
		end

	description_truncated (s: STRING): STRING
		do
			if s.count <= 50 then	-- TODO: make configurable ...
				Result := s
			else
				create Result.make_from_string (s)
				Result.keep_head (50)
			end
		end

	update_balances
		do
			update_balances_from (1)
		end

--TODO: rethink when running_balances is created ...
-- and how balances need only be recalculated forward from the
-- index of the item being updated, or created ...

	update_balances_from (a_row_index: INTEGER)
			-- Re-generate running_balances array from the transaction items
			-- TODO correctness for split transactions ...
		require
			valid_row_index: a_row_index <= row_count
		local
			n: INTEGER
			i: INTEGER
			gi: EV_GRID_ITEM
			b: INTEGER_64
			t: EAC_TRANSACTION
		do
			n := row_count
			if n > running_balances.count then
				running_balances.conservative_resize_with_default (0, 1, n)
			end

			from
				i := a_row_index
				if i > 1 then
					b := running_balances.item (i - 1)
				end
			until i > n
			loop
				gi := item (col_balance, i)
				if attached {EAC_ITEM_TR_BALANCE} gi as gi_b then
					t := gi_b.transaction
					if gi_b.is_debit then
						b := b - t.value
					else
						b := b + t.value
					end
				end
				running_balances.put (b, i)
				i := i + 1
			end
--TODO: this is insufficient ! Need other account balances
--	updated as well ...
			update_account_balance (b)
		end

	update_account_balance (a_new_balance: INTEGER_64)
		do
			report ("'update_account_balance' TODO ...%N")
		end

--feature -- TEMP to check on DATE string validity checking

--	report_valid_input_dates -- TEMP !!!
--		local
--			l_date: DATE
--		do
--			if not report_valid_input_dates_done then
--				create l_date.make_now
--				report_if_date_valid (l_date, "2021-06-08", "yyyy-[0]mm-[0]dd")
--				report_if_date_valid (l_date, "2021-6-8", "yyyy-[0]m-[0]d")
--				report_if_date_valid (l_date, "2021-6-8", "yyyy-[m]m-[d]d")
--				report_if_date_valid (l_date, "2021-6-8", "yyyy-mm-dd")

--				report_if_date_valid (l_date, "08-06-2021", "[0]dd-[0]mm-yyyy")
--				report_if_date_valid (l_date, "8-6-2021", "dd-mm-yyyy")
--				report_if_date_valid (l_date, "8-6-2021", "[0]d-[0]m-yyyy")
--				report_if_date_valid (l_date, "8-6-2021", "[d]d-[m]m-yyyy")


--			end
--			report_valid_input_dates_done := True
--		end

--	report_if_date_valid (a_date: DATE; a_date_string, a_date_format_string: STRING)
--		do
--			report (a_date_string + " : "
--				+ a_date_format_string + " : "
--				+ a_date.date_valid (a_date_string, a_date_format_string).out + "%N")
--		end

--	report_valid_input_dates_done: BOOLEAN

invariant

	attached current_import_tag as t implies {UUID}.is_valid_uuid (t)

	has_current_edit_implies_has_current_transaction:
				current_edit_field /= Void implies current_edit_transaction /= Void
	valid_edit_position: current_edit_position <= current_edit_text.count + 1
	current_edit_position >= 1
end

