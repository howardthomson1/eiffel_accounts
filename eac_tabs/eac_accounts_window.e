note
	description: "Summary description for {EAC_ACCOUNTS_TAB}."
	author: "Howard Thomson"

	REMIND: "[
		Transient attribute syntax:
			transient_attribute: detachable STRING note option: transient attribute end
			See: https://www.eiffel.org/node/333    -- Transient attributes
	]"

	TODO: "[
		Accounts balances do not reflect the restriction on import transactions
	
		Separate functionality of processing xml_accounts and xml_transactions from mapping the
		resulting EAC_ACCOUNTs and EAC_TRANSACTIONs to the GUI ... [import_from_xml and import_xml_transactions]
		
		For 'no transactions' associated with an account, blank the relevant summary boxes.
		Check for conflict with already committed accounts/transactions
		Avoid screen and system lockup when menu pops up after a long [processing] delay
		Transactions pane to the right of the tree accounts display
		Account summary pane above the transactions, minimisable ...
		Progress bar for data import ...
		The ROOT of the tree should not be visible, only the first level branches and below.
		Fixed width font for GUID column
		Why some 'NIL' entries, and some 0.00 entries ?
		Show credits/debits totals for totals including hidden subtree items
			Displayed figures should add up to displayed totals !
		Colour highlight transactions by type
		Avoid row selection by mouse click when popping up menu, and also when
			dismissing menu by clicking outside of it's boundaries
		'Empty' cells in EV_GRID: clicking on an empty cell deselects, and cannot
			be used to make a row selection ...
	]"
	DONE: "[
		Add mouse scroll-wheel response. DONE
		Add column headings -- DONE
		Add Debits & Credits -- DONE
		Add right-click: adjust column width to fit content ... DONE
		Currency formatting for debit/credit fields DONE
		Selection should be entire row, not just one field. DONE ?
		Add Totals entry at the bottom DONE
	]"

class
	EAC_ACCOUNTS_WINDOW

inherit
	EAC_GRID_COMMON
		redefine
			setup_actions
		end
create
	make

feature -- Initialisation

	make
		do
			default_create
			make_common
			map_accounts
			eac_windows.set_accounts_window (Current)
		end

	setup_columns
		do
			insert_new_column (1)
			insert_new_column (2)
			insert_new_column (3)
			insert_new_column (4)
			insert_new_column (5)
				-- Assign corresponding headings ...
			column (1).set_title ("Name")
			column (2).set_title ("Type")
			column (3).set_title ("Description")
			column (4).set_title ("Debits")
			column (5).set_title ("Credits")

				-- Centre column headings ...
			column (1).header_item.align_text_center
			column (2).header_item.align_text_center
			column (3).header_item.align_text_left
			column (4).header_item.align_text_right
			column (5).header_item.align_text_right
		end

	update_status_bar
		do
			if attached {EAC_MAIN_WINDOW} main_window as m_window then
				m_window.update_status_bar ("GnuCash Import ... !")
			end
		end

	setup_actions
		do
			Precursor
			row_select_actions.extend (agent row_selected)
		end

	row_selected (a_row: EV_GRID_ROW)
			-- A row has been selected -- remap the transactions box to match
			-- TODO: Use a shared class to access other windows ...
		do
			if attached {STRING} a_row.data as s_data
			and then attached {EV_HORIZONTAL_SPLIT_AREA} parent as p
			and then attached {EAC_GNUCASH_IMPORT_BOX} p.second as l_box
			and then attached avl_accounts_tree as tree then
				l_box.select_account_id (tree.item (s_data).id, eac_accounts, eac_transactions)
			end
		end


feature -- Container in which displayed

	set_parent_notebook (a_parent: EV_NOTEBOOK)
		require
			parent_not_void: a_parent /= Void
		do
			a_parent.extend (Current)
			a_parent.set_item_text (Current, "GnuCash Import")
		end

feature --

	mouse_click (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
			-- TODO: enable/disable sensitive instead of adding/removing menu items ?
		local
			i: INTEGER
		do
			if a_button = 3 then
				if attached menu_item_expand_tree as m_item then
					i := popup_menu.index_of (m_item, 1)
					if a_x > column (1).width then
						if i > 0 then
							popup_menu.remove_i_th (i)
						end
					else
						if not popup_menu.has (m_item) then
							popup_menu.extend (m_item)
						end
					end
				end
				popup_menu.show
			end

		end

	popup_menu: EV_MENU
		local
			l_window: EV_WINDOW
			l_bar: EV_MENU_BAR
			l_menu: EV_MENU
			l_item: EV_MENU_ITEM
		once
			create Result
			l_menu := Result
			create l_item.make_with_text ("Resize Columns to Content"); l_menu.extend (l_item)
				l_item.select_actions.extend (agent resize_columns)
			create l_item.make_with_text ("Expand all tree"); l_menu.extend (l_item)
				l_item.select_actions.extend (agent expand_all_tree)
				menu_item_expand_tree := l_item
			create l_item.make_with_text ("Open File ..."); l_menu.extend (l_item)
				l_item.select_actions.extend (agent open_file_for_import)
			create l_item.make_with_text ("Open transactions tab ..."); l_menu.extend (l_item)
				l_item.select_actions.extend (agent open_transaction_import_tab)
			create l_item.make_with_text ("Hide/Show Debits/Credits"); l_menu.extend (l_item)
				l_item.select_actions.extend (agent show_hide_debits_credits)
			--	l_item.disable_sensitive
		end

	menu_item_expand_tree: detachable EV_MENU_ITEM

	resize_columns
		local
			c: EV_GRID_COLUMN
			w: INTEGER
		do
			column (1).resize_to_content
			column (2).resize_to_content
			column (3).resize_to_content
			c := column (3)
				-- TODO more appropriate test here
			if column (4).is_displayed then
				column (4).resize_to_content
				column (5).resize_to_content
				c := column (5)
			end
				-- Re-size the LHS of the splitter to accommodate the new size
			w := c.virtual_x_position + c.width
			if attached {EAC_GNUCASH_IMPORT_TAB} parent as p then
--				p.
			end
		end

	show_hide_debits_credits
		local
			l_row_count: INTEGER
		do
			l_row_count := row_count
			if l_row_count > 0 and column_count > 0 then
				if column (4).is_displayed then
					column (4).hide				-- Debits column
					column (5).hide				-- Credits column
					row (l_row_count).hide		-- Balance row
					row (l_row_count - 1).hide	-- Separator row, above balance
				else
					column (4).show
					column (5).show
					row (l_row_count).show
					row (l_row_count - 1).show
				end
				adjust_vertical_split
			end
		end

	adjust_vertical_split
		local
			i: INTEGER
		do
			if attached {EV_VERTICAL_SPLIT_AREA} parent as p then
				i := width
				if i >= p.minimum_split_position
				and i <= p.maximum_split_position then
					p.set_split_position (i)
				end
			end
		end

	open_transaction_import_tab
		local
			l_tab: EAC_TRANSACTIONS_WINDOW
		do
			create l_tab.make
			if  attached {EAC_MAIN_WINDOW} main_window as mw then
--				report ("Attached main window")
				if attached mw.notebook as nb then
				--	nb.extend (l_tab)
					l_tab.set_parent_notebook (nb)
				end
			end
		end

	open_file_for_import
		do
			file_dialog.set_title ("Open GnuCash file for import ...")
			file_dialog.open_actions.extend (agent process_xml_file)
			if attached {EAC_MAIN_WINDOW} main_window as m_window then
				file_dialog.show_modal_to_window (m_window)
			end
		end

	file_dialog: EV_FILE_OPEN_DIALOG
		once
			create Result
		end

--TODO: wipe_out not appropriate for import adding, selectively, to existing data

	process_xml_file
		local
			l_gnucash_file_importer: XML_IMPORTER_GNUCASH
		do
				-- Wipeout existing GRID data
			wipe_out
				-- Setup column headings
			setup_columns

				-- wipeout XML data ...
--			eac_data.xml_accounts.wipe_out
--			eac_data.xml_transactions.wipe_out
				-- Import new XML data ...
			create l_gnucash_file_importer.make
			l_gnucash_file_importer.parse_from_file (file_dialog.file_name)
			if l_gnucash_file_importer.parse_successful then

--TODO: Generate an group import tag, and assign it to the newly created objects
--	imported from XML. Add them to the data set. Update the transactions window
--	with the new tag ...

		--		eac_accounts := l_gnucash_file_importer.eac_accounts
		--		eac_transactions := l_gnucash_file_importer.eac_transactions
				eac_data.set_accounts (l_gnucash_file_importer.eac_accounts)
				eac_data.set_transactions (l_gnucash_file_importer.eac_transactions)

				map_accounts
			else
				report ("TODO: report XML parsing error ...%N")
			end
		end



	eac_accounts:  HASH_TABLE [EAC_ACCOUNT, STRING]
		-- Native accounts, indexed by GUID
		do
			Result := eac_data.eac_accounts
		end

	eac_transactions:  HASH_TABLE [EAC_TRANSACTION, STRING]
		-- Native transactions, indexed by group GUID
		-- Where no. splits = 2, indexed by transaction GUID
		--	 In this case, there is only on native record
		-- Where no. splits >=3, indexed by split GUID
		--   In this case there is one record per split
		do
			result := eac_data.eac_transactions
		end

	avl_accounts_tree: detachable DS_AVL_TREE [EAC_ACCOUNT, STRING]

	map_accounts
			-- Map the Accounts into the accounts tree
		local
			l_account: EAC_ACCOUNT
			l_parent_account: EAC_ACCOUNT
			l_grid_row: EV_GRID_ROW
			l_new_row: EV_GRID_ROW
			l_row_index: INTEGER
			l_balance: INTEGER_64
			l_credits, l_debits: INTEGER_64
			l_cumulative_credits, l_cumulative_debits: INTEGER_64
			l_new_id: STRING_8
			l_cursor: like eac_accounts.new_cursor

			l_root_account: EAC_ACCOUNT
			l_loop_acount: detachable EAC_ACCOUNT


			l_tree: DS_AVL_TREE [EAC_ACCOUNT, STRING]
			l_comparator: EAC_ACCOUNT_COMPARATOR
			l_key_string: STRING

		do
			create l_key_string.make_empty
			create l_comparator
			create l_tree.make (l_comparator)
			if row_count > 0 then
				remove_rows (1, row_count)
			end
			if attached eac_accounts as l_accounts then
					-- First setup the account references, using the parent_id attributes
				l_cursor := l_accounts.new_cursor
				from l_cursor.start
				until l_cursor.after
				loop
					l_account := l_cursor.item
					l_key_string := account_key_string (l_account, l_accounts)
					l_tree.put_new (l_account, l_key_string)
					l_cursor.forth
				end
				avl_accounts_tree := l_tree

					-- Populate the grid from the ordered tree ...
				from l_tree.start
				until l_tree.after
				loop
					l_account := l_tree.item_for_iteration
					if l_account.is_root then
						set_item (1, 1, create {EV_GRID_LABEL_ITEM}.make_with_text ("ROOT"))
						l_account.set_grid_xy (1, 1)
					else

						if attached l_account.parent_id as attached_parent_id then
							l_accounts.search (attached_parent_id)
							if l_accounts.found  then
								l_parent_account := l_accounts.found_item
								if attached l_parent_account as attached_parent then

									if attached item (attached_parent.grid_x, attached_parent.grid_y) as attached_item then
										l_grid_row := attached_item.row
										l_row_index := l_grid_row.subrow_count + 1
										l_grid_row.insert_subrow (l_row_index)
										l_new_row := l_grid_row.subrow (l_row_index)
										l_new_row.set_item (1, create {EV_GRID_LABEL_ITEM}.make_with_text (l_account.attached_name))
										l_new_row.set_item (2, create {EV_GRID_LABEL_ITEM}.make_with_text (l_account.type_as_string))
										l_new_row.set_item (3, create {EV_GRID_LABEL_ITEM}.make_with_text (l_account.attached_description))
										l_new_row.set_data (l_tree.key_for_iteration)
										if attached {EV_GRID_LABEL_ITEM	} l_new_row.item (2) as l_new_item then
											l_new_item.align_text_center
										end
--TODO...
--										l_balance := l_account.balance
										if l_balance > 0 then
											l_credits := l_credits + l_balance
											l_new_row.set_item (4, create {EV_GRID_LABEL_ITEM}.make_with_text ("NIL"))
											l_new_row.set_item (5, create {EV_GRID_LABEL_ITEM}.make_with_text (formatted_currency_string (l_balance)))
										else
											l_debits := l_debits - l_balance
											l_new_row.set_item (4, create {EV_GRID_LABEL_ITEM}.make_with_text (formatted_currency_string ( - l_balance)))
											l_new_row.set_item (5, create {EV_GRID_LABEL_ITEM}.make_with_text ("NIL"))
										end
											-- Update cumulative running totals
--TODO...
--										l_balance := l_account.cumulative_balance
										if l_balance > 0 then
											l_cumulative_credits := l_cumulative_credits + l_balance
										else
											l_cumulative_debits := l_cumulative_debits - l_balance
										end
										l_account.set_grid_xy (1, l_new_row.index)
										if attached {EV_GRID_LABEL_ITEM	} l_new_row.item (4) as l_new_item then
											l_new_item.align_text_right
										end
										if attached {EV_GRID_LABEL_ITEM	} l_new_row.item (5) as l_new_item then
											l_new_item.align_text_right
										end
									else
										report ("No item found for x,y of parent account...%N")
									end
								else
									report ("No attached parent ...%N")
								end
							end
						end
					end
					l_tree.forth
				end
			end

				-- Add bottom row on grid for the totals
			if row_count >= 1 and then attached item (1, 1) as root_item then
				l_grid_row := root_item.row
				l_row_index := l_grid_row.subrow_count + 1

				l_grid_row.insert_subrow (l_row_index)
				l_new_row := l_grid_row.subrow (l_row_index)
				l_new_row.set_item (1, create {EV_GRID_LABEL_ITEM}.make_with_text ("=============="))
				l_new_row.set_item (4, create {EV_GRID_LABEL_ITEM}.make_with_text ("=============="))
				l_new_row.set_item (5, create {EV_GRID_LABEL_ITEM}.make_with_text ("=============="))
					--TODO: Find a way to make this row non-selectable!
					-- Also for the Balance figure row [below]
		--		l_new_row.set_selectable (False)

				l_row_index := l_row_index + 1
				l_grid_row.insert_subrow (l_row_index)
				l_new_row := l_grid_row.subrow (l_row_index)
				l_new_row.set_item (1, create {EV_GRID_LABEL_ITEM}.make_with_text ("BALANCE"))
				l_new_row.set_item (4, create {EV_GRID_LABEL_ITEM}.make_with_text (formatted_currency_string (l_debits)))
				l_new_row.set_item (5, create {EV_GRID_LABEL_ITEM}.make_with_text (formatted_currency_string (l_credits)))
			end
			if row_count > 1 then
				update_displayed_balances
				expand_all_tree
				resize_columns
				unset_tabable
			end
		end

	unset_tabable
		local
			i, j: INTEGER
		do
			from i := 1
			until i > row_count
			loop
				from j := 1
				until j > 5
				loop
					if attached item (j, i) as l_item then
						l_item.set_is_tab_navigatable (False)
					end
					j := j + 1
				end
				i := i + 1
			end
		end

	account_key_string (a_account: EAC_ACCOUNT; a_account_table: HASH_TABLE [EAC_ACCOUNT, STRING]): STRING
			-- Generate key based on a_account and all ancestors thereof
		local
			l_cursor: like eac_accounts.new_cursor
			l_accounts: like a_account_table
			l_account: EAC_ACCOUNT
			done: BOOLEAN
		do
			create Result.make_empty
			l_account := a_account
			from
				l_accounts := a_account_table
			until
				done
			loop
				if attached l_account as l then
					Result.prepend (l.attached_name)
					l_accounts.search (l.attached_parent_id)
					if l_accounts.found then
						Result.prepend (" :: ")
						l_account := l_accounts.found_item
					else
						done := True
					end
				end
			end
		end

	update_displayed_balances
			-- Update the displayed balance of each account
			-- For entries that are not tree-expanded, display the total including children
			-- TODO: make same allowance for date resriction as transactions window !
		local
			i, j, n: INTEGER
			l_row: EV_GRID_ROW
			l_account: EAC_ACCOUNT
			l_balance: INTEGER_64
			l_credits, l_debits: INTEGER_64
		do
			from
				i := 1
				n := row_count - 2
			until
				i > n
			loop
				l_row := row (i)
				if attached {STRING} l_row.data as id
				and then attached avl_accounts_tree as tree
				then
					l_balance := tree.item (id).current_balance (False)
					if l_balance < 0 then
						if attached {EV_GRID_LABEL_ITEM} l_row.item (4) as l_item then
							l_item.set_text (formatted_currency_string (- l_balance))
							l_debits := l_debits + (- l_balance)
						end
					else
						if attached {EV_GRID_LABEL_ITEM} l_row.item (5) as l_item then
							l_item.set_text (formatted_currency_string (l_balance))
							l_credits := l_credits + l_balance
						end
					end
					j := 1
				end
				i := i + 1	-- ????
			end
			l_row := row (row_count)
			if attached {EV_GRID_LABEL_ITEM} l_row.item (4) as l_item then
				l_item.set_text (formatted_currency_string (l_debits))
			end
			if attached {EV_GRID_LABEL_ITEM} l_row.item (5) as l_item then
				l_item.set_text (formatted_currency_string (l_credits))
			end
			check accounts_balance: l_debits = l_credits end

		end

--	displayed_account_balance (a_account: EAC_ACCOUNT; a_include_descendants: BOOLEAN): INTEGER_64
--		local
--			l_account_key: STRING
--			l_cursor: like avl_accounts_tree.new_cursor
--			last_key: STRING
--			l_key: STRING
--			done: BOOLEAN
--		do
--			if attached avl_accounts_tree as tree then
--				l_account_key := account_key_string (a_account, eac_data.eac_accounts)
--				l_cursor := tree.new_cursor
--				l_cursor.go_at_or_after_key (l_account_key)
--				Result := transactions_balance (l_cursor.item)
--				if not a_include_descendants then
--					-- Result already set
--				else
--					from
--						last_key := l_account_key
--						l_cursor.forth
--					until done loop
--						if l_cursor.after then
--							done := True
--						else
--							last_key := l_key
--							l_key := account_key_string (l_cursor.item, eac_data.eac_accounts)
--							if not is_prefix_of (l_account_key, l_key) then
--								done := True
--							else
--								Result := Result + transactions_balance (l_cursor.item)
--							end
--							l_cursor.forth
--						end
--					end
--				end
--			end
--		end

--	transactions_balance (an_account: EAC_ACCOUNT): INTEGER_64
--		do
--			Result := an_account.current_balance (False)
--		end

	is_prefix_of (a_string, a_prefixed_string: STRING): BOOLEAN
			-- is 'a_string' a substring of 'a_prefixed_string' ?
		local
			l_string: STRING
		do
			create l_string.make_from_string (a_prefixed_string)
			l_string.keep_head (a_string.count)
			Result := a_string.is_equal (l_string)
		end

	uuid_insert_separators (a_string: STRING): STRING
			-- Convert GNUCash UUID to Eiffel UUID
			-- by inserting '-' separators at the usual
			-- 8-4-4-4-12 grouping points
		require
			valid_uuid_count_no_separators: a_string.count = 32
		do
			create Result.make_from_string (a_string)
			Result.insert_character ('-', 9)
			Result.insert_character ('-', 14)
			Result.insert_character ('-', 19)
			Result.insert_character ('-', 24)
			check is_valid_uuid: {UUID}.is_valid_uuid (Result) end
		end

end
