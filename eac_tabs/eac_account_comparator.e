note
	description: "Summary description for {EAC_ACCOUNT_COMPARATOR}."
	author: "Howard Thomson"
	date: "5-June-2020"
	TODO: "[
		Rename class -- EAC_STRING_COMPARATOR ?
	]"
class
	EAC_ACCOUNT_COMPARATOR

inherit
	KL_COMPARATOR [STRING]

feature

	attached_less_than (u, v: attached STRING): BOOLEAN
			-- Is `u' considered less than `v'?
		do
			if u.is_less (v) then
				Result := True
			end
		end
end
