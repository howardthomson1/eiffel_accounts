note
	description: "Summary description for {EAC_ITEM_TR_BALANCE}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

	TODO: "[
		Separate out 'text' from calculation of the balance to be displayed
		Change the process for choosing the text colour dislayed
		For debit/negative balances, show coloured and bracketed figure ...
	]"

class
	EAC_ITEM_TR_BALANCE

inherit
	EAC_ITEM_TR_COMMON
		redefine
			is_editable
		end

	EAC_PRINT
		undefine default_create, copy end

create
	make

feature -- Creation

	make (a_transaction: EAC_TRANSACTION; a_transactions_window: EAC_TRANSACTIONS_WINDOW)
		do
			transaction := a_transaction
			transactions_window := a_transactions_window
			default_create
			expose_actions.extend (agent custom_expose (?, Current))
			add_events
			set_right_aligned
			set_required_width (exposure_font.string_width (text) + 10)
		end

feature

	text: STRING
			--TODO: Better method needed for identifying the current row!
		local
			i: INTEGER
			l_balance: INTEGER_64
			f: STRING
		do
			if is_parented then
				i := row.index
				l_balance := transactions_window.running_balances.item (i)
				f := formatted_currency_string (l_balance)
				if l_balance >= 0 then
					Result := f
				else
					Result := "(" + f + ")"
				end
			else
				Result := "Undefined"
			end
		end

	default_background_text: STRING = "Balance"

	is_editable: BOOLEAN
			-- Is the field editable ?
		do
			Result := False
		end

	valid_text (a_string: STRING): BOOLEAN
			-- Is 'a_string' valid content for this field ?
		do
			Result := True -- Non-editable field
		end

invariant

--	data = Void


end
