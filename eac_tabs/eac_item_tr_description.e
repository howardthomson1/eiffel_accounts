note
	description: "Summary description for {EAC_ITEM_TR_DESCRIPTION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_ITEM_TR_DESCRIPTION

inherit
	EAC_ITEM_TR_COMMON
		redefine
			max_text_size,
			update_field
		end

create
	make

feature -- Creation

	make (a_transaction: EAC_TRANSACTION; a_transactions_window: EAC_TRANSACTIONS_WINDOW)
		do
			transaction := a_transaction
			transactions_window := a_transactions_window
			default_create
			expose_actions.extend (agent custom_expose (?, Current))
			add_events
			set_left_aligned
			set_required_width (exposure_font.string_width (text) + 10)
			set_is_tab_navigatable (True)	-- Move ?
		end

feature

	text: STRING
		local
			is_split: BOOLEAN
		do
			if attached transaction.split_group then
				is_split := True
			end
			if is_split then
				Result := "SPLIT"
			elseif attached transaction.description as d then
				Result := description_truncated (d)
			else
				Result := ""
			end
		end

	default_background_text: STRING = "Description"

	description_truncated (s: STRING): STRING
		do
			if s.count <= 50 then
				Result := s
			else
				create Result.make_from_string (s)
				Result.keep_head (50)
			end
		end

	max_text_size: INTEGER
			--TODO: Make this configurable ...
		do
			Result := 50
		end

	update_field (a_text: STRING)
			-- Update the associated record from this text
		do
			transaction.set_description (create {STRING}.make_from_string (a_text))
		end


	valid_text (a_string: STRING): BOOLEAN
			-- Is 'a_string' valid content for this field ?
		local
			s: STRING
		do
			Result := True
		end


end
