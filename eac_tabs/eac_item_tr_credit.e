note
	description: "Summary description for {EAC_ITEM_TR_CREDIT}."
	author: "Howard Thomson"
	date: "$17-Sep-2021$"
	revision: "$Revision$"

class
	EAC_ITEM_TR_CREDIT

inherit
	EAC_ITEM_CRDR_COMMON

create
	make

feature

	text: STRING
		do
			if not is_debit
			and then transaction.value /= 0 then
				Result := formatted_currency_string (transaction.value)
			else
				Result := ""
			end
		end

	default_background_text: STRING = "Credit"

	update_dr_cr
			-- Update debit_account and credit_account
			-- to match this column entry
		local
			t: EAC_TRANSACTION
		do
				-- 1: No account yet assigned
				-- 2: Credit  account matches current_account.id
				-- 3: Debit account matches current_account.id

			t := transaction
			if attached transactions_window.current_account as ac then
				check ac.id /= Void end
				if t.debit_account = Void or else t.credit_account = Void then
						-- Case 1: Do nothing, account not yet known
				elseif attached t.credit_account as cr and then cr.is_equal (ac.id) then
						-- Case 2: Already assigned as Credit: Do nothing
				else
						-- Case 3: Switch debit a/c to credit a/c
					if attached t.credit_account as cr then
						t.set_debit_account (cr)
					end
					t.set_credit_account (ac.id)
				end
			end
		end

end
