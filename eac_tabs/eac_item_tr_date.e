note
	description: "Customised EV_GRID_ITEM to display [and edit ?] the date for an EAC Transaction"
	author: "Howard Thomson"
	date: "6-April-2021"

	TODO: "[
		Configurable default for new transaction:
			1/ Blank field [default text initially displayed]
			2/ Todays' date
			3/ Last entered date for this session
			4/ Configurable date ...
		Warn about possible [probable] date errors
		Check for locked earlier periods, and invalid date preceding such periods
		
		First character typed to reset entry field to new character only ... ?
		
		Auto detect dd.mm.yyyy vs yyyy.mm.dd ...
		
		Invalid date entered, warn [red highlight] not default to existing ...
		
		For 't' and 'l' move to next field
	]"

class
	EAC_ITEM_TR_DATE

inherit
	EAC_ITEM_TR_COMMON
		redefine
			custom_expose,
			max_text_size,
			update_field,
			key_press_string_event
		end

create
	make

feature -- Creation

	make (a_transaction: EAC_TRANSACTION; a_transactions_window: EAC_TRANSACTIONS_WINDOW)
		do
			transaction := a_transaction
			transactions_window := a_transactions_window
			default_create
--			data := a_data
			expose_actions.extend (agent custom_expose (?, Current))
			add_events
			set_center_aligned
			set_required_width (exposure_font.string_width (text))

			pointer_double_press_actions.extend (agent pointer_double_press (?,?,?,?,?,?,?,?,Current))

			set_is_tab_navigatable (True)
		end

	pointer_double_press (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER; a_item: like Current)
		local
			l_date_dialog: AEL_V2_CALENDAR_DIALOG
			l_new_date: INTEGER
		do
				--TODO: Sort out AEL APPLICATION_ROOT initialization on startup ...
				-- Crash on dialog update/confirmation ...
			create l_date_dialog.make_now ("Transaction date")
			if attached transactions_window.main_window as mw then
				l_date_dialog.show_modal_to_window (mw)
			end
			if l_date_dialog.has_changed then
				if attached l_date_dialog.committed_value as dv then
					display_date.set_internal_ordered_compact_date (dv.ordered_compact_date)
					transaction.set_date (dv.ordered_compact_date)
					current.transactions_window.current_edit_text.copy (text)
				end
			end
		end

feature

	text: STRING
		do
			if transaction.date /= 0 then
				display_date.set_internal_ordered_compact_date (transaction.date)
				Result := display_date.formatted_out (date_format_string)
			else
				Result := ""
			end
		end

	default_background_text: STRING = "Date"

	date_format_string: STRING = "yyyy-[0]mm-[0]dd";

	date_input_format_string: STRING = "yyyy-mm-dd";

	display_date: DATE
		once
			create Result.make_now
		end

	custom_expose (a_drawable: EV_DRAWABLE; a_grid_item: like Current)
		local
			w, h: INTEGER
			x, y: INTEGER
			d: INTEGER
		do
			Precursor (a_drawable, a_grid_item)

--			if is_current_edit_field then
--					-- Draw the target for the popup selector ...
--				w := width
--				h := height
--				d := 10
--				x := w - 12
--				y := h // 2
--				a_drawable.draw_point (x, y)
--				a_drawable.draw_ellipse (x - (d // 2), y - (d // 2), d, d)
--			end
		end

	max_text_size: INTEGER
		once
			Result := ("28-Feb-2021").count
		end

	valid_text (a_string: STRING): BOOLEAN
			-- Is 'a_string' valid content for this [date] field ?
		local
			s: STRING
		do
			create s.make_from_string (a_string)
			s.left_adjust
			s.right_adjust
--			if s.is_number_sequence then
				Result := True
--			else
--				report ("Invalid date: " + a_string + "%N")
--			end
		end

	update_field (a_text: STRING)
			-- Update the associated record from this text
		local
			l_date: DATE
			l_text: STRING
		do
--			report_valid_date_inputs

			create l_text.make_from_string (a_text)
			replace_date_separators (l_text)
			create l_date.make_now
			if l_date.date_valid (l_text, date_input_format_string) then
				l_date.make_from_string (l_text, date_input_format_string)
				transaction.set_date (l_date.ordered_compact_date)
				transactions_window.assign_current_edit_text (Current)
			end
		end

	replace_date_separators (s: STRING)
		local
			i: INTEGER
		do
			from i := 1
			until i > s.count
			loop
				if s.item (i) = '/'
				or else s.item (i) = '.' then
					s.put ('-', i)
				end
				i := i + 1
			end
		end

--	report_valid_date_inputs
--		do
--			transactions_window.report_valid_input_dates
--		end

	key_press_string_event (a_string: STRING_32)
		local
			l_date: DATE
		do
			if a_string.item (1) = 't' or else a_string.item (1) = 'T' then
					-- Today's date ...
				create l_date.make_now
				transaction.set_date (l_date.ordered_compact_date)
				last_ordered_compact_date := l_date.ordered_compact_date
				transactions_window.assign_current_edit_text (Current)
				redraw
			elseif a_string.item (1) = 'l' or else a_string.item (1) = 'L' then
					-- Last date entered ...
				if last_ordered_compact_date /= 0 then
					transaction.set_date (last_ordered_compact_date)
					transactions_window.assign_current_edit_text (Current)
					redraw
				end
			else
				Precursor (a_string)
			end
		end


--TODO: Does this need to be moved to the Transactions Window ?
	last_ordered_compact_date: INTEGER_32

end
