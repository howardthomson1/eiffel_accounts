note
	description: "Summary description for {EAC_GNUCASH_IMPORT_BOX}."
	author: "Howard Thomson"
	date: "27-May-2020"
	test: "from"
	TODO: "[
		Add new section, above the transactions grid, showing additional detail
		on the currently selected account, and selecting boxes on date from 
		Add fields for: 
			Balance 
			Balance, starting from a [the] prior committed 'Trial Balance'
			No. of transactions
			Select 'from' date and, 'to' date
			
		Check before committing the import, that there are no opening/closing TBs within
		the selected from/to dates.
		
		Make the reporting text fields non-enterable with the edit cursor. Different widget ?
			Need to be distinct from the labels.
	]"

class
	EAC_GNUCASH_IMPORT_BOX

inherit
	EV_VERTICAL_BOX
		redefine
			create_interface_objects,
			initialize,
			is_in_default_state
		end
	EAC_REPORTER
		undefine
			copy, is_equal, default_create
		end
	EAC_SHARED_DATA
		undefine
			copy, is_equal, default_create
		end

create
	default_create

feature {NONE}

	create_interface_objects
		do
				-- Components of the static area ...
				--Fields
			create fixed
			create date_from
			create date_to
			create account_name
			create account_type
			create account_description
			create debits_total
			create credits_total
				-- Labels
			create label_date_from.make_with_text ("First date:")
			create label_date_to.make_with_text ("Last date:")
			create label_account_name.make_with_text ("Account name:")
			create label_account_type.make_with_text ("Type:")
			create label_account_description.make_with_text ("Description:")
			create label_debits_total.make_with_text ("Total Debits:")
			create label_credits_total.make_with_text ("Total Credits")

				-- The set of transactions ...
			create transactions_grid.make
				-- TEMP !!!
			create temp_widget.make_with_text ("####")

			create label_select_from.make_with_text ("Select from:")
			create label_select_to.make_with_text ("Select to:")
			create select_from
			create select_to
		end

	initialize
		do
			Precursor
			build_fixed
			extend (fixed)
			disable_homogeneous
			disable_item_expand (fixed)
			extend (transactions_grid)

			fixed.pointer_motion_actions.extend (agent pointer_moved)
			fixed.pointer_button_press_actions.extend (agent button_press)
			fixed.pointer_button_release_actions.extend (agent button_release)

			assign_widget_agents
		end

feature -- Elements of the static area above the transactions grid

	fixed: EV_FIXED
		-- Fields
--	date_from: EV_TEXT_FIELD
--	date_to: EV_TEXT_FIELD

--	account_name: EV_TEXT_FIELD
--	account_type: EV_TEXT_FIELD
--	account_description: EV_TEXT_FIELD

--	debits_total: EV_TEXT_FIELD
--	credits_total: EV_TEXT_FIELD

--	select_from: EV_TEXT_FIELD
--	select_to: EV_TEXT_FIELD
--###################################
	date_from: EV_LABEL
	date_to: EV_LABEL

	account_name: EV_LABEL
	account_type: EV_LABEL
	account_description: EV_LABEL

	debits_total: EV_LABEL
	credits_total: EV_LABEL

	select_from: EV_LABEL
	select_to: EV_LABEL


		-- Labels
	label_date_from: EV_LABEL
	label_date_to: EV_LABEL

	label_account_name: EV_LABEL
	label_account_type: EV_LABEL
	label_account_description: EV_LABEL

	label_debits_total: EV_LABEL
	label_credits_total: EV_LABEL

	temp_widget: EV_LABEL

	label_select_from: EV_LABEL
	label_select_to: EV_LABEL

--TEMP
--	date_test: AEL_V2_CALENDAR
--	date_test: EV_LABEL

	build_fixed
			-- Setup the static area, above the transactions grid
		local
			x, y: INTEGER
			dy: INTEGER
		do
				-- Add the items to the EV_FIXED area
				-- Fields
			fixed.extend (date_from)
			fixed.extend (date_to)
			fixed.extend (account_name)
			fixed.extend (account_type)
			fixed.extend (account_description)
			fixed.extend (debits_total)
			fixed.extend (credits_total)
				--Labels
			fixed.extend (label_date_from)
			fixed.extend (label_date_to)
			fixed.extend (label_account_name)
			fixed.extend (label_account_type)
			fixed.extend (label_account_description)
			fixed.extend (label_debits_total)
			fixed.extend (label_credits_total)

			fixed.extend (temp_widget)
			temp_widget.hide

			fixed.extend (select_from)
			fixed.extend (select_to)
			fixed.extend (label_select_from)
			fixed.extend (label_select_to)

--			fixed.extend (date_test)
--			fixed.set_item_position (date_test, 500, 80)

				-- Assign positions ...
				--
			fixed.set_item_position (label_date_from, 0, 0)
			fixed.set_item_position (date_from,     110, 0)
			fixed.set_item_position (label_date_to, 210, 0)
			fixed.set_item_position (date_to,       290, 0)

			fixed.set_item_position (label_account_name,   0, 25)
			fixed.set_item_position (account_name,       110, 25)
			fixed.set_item_position (label_account_type, 370, 25)
			fixed.set_item_position (account_type,       430, 25)

			x := 0; y := 50; dy := 25

			fixed.set_item_position (label_account_description, x, y); y := y + dy

			fixed.set_item_position (label_debits_total, x, y); y := y + dy
			fixed.set_item_position (label_credits_total, x, y); y := y + dy
				--
			x := 110; y := 50; dy := 25
			fixed.set_item_position (account_description, x, y); y := y + dy

			fixed.set_item_position (debits_total, x, y); y := y + dy
			fixed.set_item_position (credits_total, x, y); y := y + dy

			fixed.set_item_position (label_select_from, 220, 80)
			fixed.set_item_position (label_select_to, 220, 105)
			fixed.set_item_position (select_from, 320, 80)
			fixed.set_item_position (select_to, 320, 105)

				-- Set widths
			fixed.set_item_width (date_from, 100)
			fixed.set_item_width (date_to, 100)
			fixed.set_item_width (account_name, 250)
			fixed.set_item_width (account_type, 100)
			fixed.set_item_width (account_description, 300)
			fixed.set_item_width (debits_total, 100)
			fixed.set_item_width (credits_total, 100)

			fixed.set_item_width (select_from, 100)
			fixed.set_item_width (select_to, 100)

				-- Disable data entry for display-only boxes
--			date_from.disable_edit
--			date_to.disable_edit
--			account_name.disable_edit
--			account_type.disable_edit
--			account_description.disable_edit
--			debits_total.disable_edit
--			credits_total.disable_edit

--			date_from.disable_capture
--			date_to.disable_capture
--			account_name.disable_capture
--			account_type.disable_capture
--			account_description.disable_capture
--			debits_total.disable_capture
--			credits_total.disable_capture

			-- TEMP!
		end

feature -- Agents

	pointer_moved (a_x, a_y: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
		local
			l_status: STRING
			wx, wy: INTEGER
		do
			create l_status.make_empty
			l_status.append ("Mouse position x: " + a_x.out + " y: " + a_y.out)
			if attached {EAC_MAIN_WINDOW} main_window as m_window then
				m_window.update_status_bar (l_status)
			end
			if button_is_pressed and then attached selected_widget as w then
				wx := w.x_position; wy := w.y_position
				fixed.set_item_position (w, wx + a_x - last_x, wy + a_y - last_y)
			end
			last_x := a_x
			last_y := a_y
		end

	last_x, last_y: INTEGER

	button_press (a_x, a_y, button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
		local
			l_widget: detachable EV_WIDGET
			wx, wy: INTEGER
		do
			l_widget := widget_in_fixed_at (a_x, a_y)
			if button = 3 and l_widget /= Void then
				button_is_pressed := True
				selected_widget := l_widget
				wx := l_widget.x_position; wy := l_widget.y_position
				l_widget.hide
				temp_widget.show
				fixed.set_item_position (temp_widget, wx, wy)
			end
		end

	button_is_pressed: BOOLEAN

	selected_widget: detachable EV_WIDGET

	hidden_widget: detachable EV_WIDGET

	button_release (a_x, a_y, button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
		local
			l_widget: detachable EV_WIDGET
			wx, wy: INTEGER
		do
			if attached selected_widget as sw then
				temp_widget.hide
				wx := temp_widget.x_position
				wy := temp_widget.y_position
				sw.show
				fixed.set_item_position (sw, wx, wy)
			end
			selected_widget := Void
			button_is_pressed := False
		end

	widget_in_fixed_at (a_x, a_y: INTEGER): detachable EV_WIDGET
		local
			c: like fixed.new_cursor
			w: EV_WIDGET
		do
			from c := fixed.new_cursor
			until c.after or else Result /= Void
			loop
				w := c.item
				if a_x >= w.x_position
				and a_x < (w.x_position + w.width)
				and a_y >= w.y_position
				and a_y < (w.y_position + w.height)
				then
					Result := w
				end
				c.forth
			end
		end

	assign_widget_agents
		do
			alter_widget (date_from)
			alter_widget (date_to)

			alter_widget (account_name)
			alter_widget (account_type)
			alter_widget (account_description)

			alter_widget (debits_total)
			alter_widget (credits_total)
				-- Labels
			alter_widget (label_date_from)
			alter_widget (label_date_to)

			alter_widget (label_account_name)
			alter_widget (label_account_type)
			alter_widget (label_account_description)

			alter_widget (label_debits_total)
			alter_widget (label_credits_total)

		end

	alter_widget (a_widget: EV_WIDGET_ACTION_SEQUENCES)
		do
			a_widget.pointer_button_press_actions.extend (agent button_press)
			a_widget.pointer_button_release_actions.extend (agent button_release)
			a_widget.pointer_motion_actions.extend (agent pointer_moved)
		end

	main_window: detachable EV_WINDOW
			-- TODO: find a better way to do this ?
			-- See EV_WIDGET_IMP.top_level_window in Win32 19.12 release ...
		local
			l_widget: EV_WIDGET
			l_window: EV_WINDOW
		do
			from l_widget := Current
			until Result /= Void or else l_widget = Void
			loop
				if attached l_widget.parent as lp then
					l_widget := lp
					if attached {EV_WINDOW} lp as lw then
						Result := lw
					end
				end
			end
		end

	transactions_grid: EAC_TRANSACTIONS_WINDOW

	is_in_default_state: BOOLEAN
			-- TODO: figure out why this is necessary ...
		do
			Result := True
		end

feature -- data retrieval

	select_account_id (a_id: STRING; a_accounts: HASH_TABLE [EAC_ACCOUNT, STRING]; a_transactions: HASH_TABLE [EAC_TRANSACTION, STRING])
			-- Given an 'id', a GUID for a specific account, display the
			-- account details and transactions for that account
		local
			l_account: EAC_ACCOUNT
		do
			a_accounts.search (a_id)
			if a_accounts.found then
				l_account := a_accounts.found_item
				if attached l_account as ac then
					account_name.set_text (ac.attached_name)
					account_type.set_text (ac.type_as_string)
					account_description.set_text (ac.attached_description)
						-- Fill transactions in grid
					transactions_grid.fill_grid (ac, a_transactions)

					report_font_details
				end
			end
		end

	report_font_details
		local
			f: EV_FONT
		do
			if not font_details_reported then
				f := account_name.font
				report ("Reporting EAC_GNUCASH_IMPORT_BOX font values ...%N")
				report (f.weight.out + " : weight%N")
				report (f.family.out + " : family%N")
				report (f.name + " : name%N")
				report (f.shape.out + " : shape%N")
				font_details_reported := True
			end
		end

	font_details_reported: BOOLEAN

end
