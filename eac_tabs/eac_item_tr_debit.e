note
	description: "Summary description for {EAC_ITEM_TR_DEBIT}."
	author: "Howard Thomson"
	date: "$30-Aug-2021$"
	revision: "$Revision$"

	TODO: "[
		Ensure that account selection does NOT select the current account !
	]"

class
	EAC_ITEM_TR_DEBIT

inherit
	EAC_ITEM_CRDR_COMMON

create
	make

feature

	text: STRING
		do
			if is_debit
			and then transaction.value /= 0 then
				Result := formatted_currency_string (transaction.value)
			else
				Result := ""
			end
		end

	default_background_text: STRING = "Debit"

	update_dr_cr
			-- Update debit_account and credit_account
			-- to match this column entry
		local
			t: EAC_TRANSACTION
		do
				-- 1: No account yet assigned, debit_account [or credit_account] matches selection account
				-- 2: Debit  account matches current_account.id
				-- 3: Credit account matches current_account.id




			t := transaction
			if attached current.transactions_window.current_account as ac then
				check ac.id /= Void end
				if t.debit_account = Void or else t.credit_account = Void then
						-- Case 1: Do nothing, account not yet known
				elseif attached t.debit_account as dr and then dr.is_equal (ac.id) then
						-- Case 2: Already assigned as Debit: Do nothing
				else
						-- Case 3: Switch credit a/c to Debit a/c
					if attached t.debit_account as dr then
						t.set_credit_account (dr)
					end
					t.set_debit_account (ac.id)
				end
			end
		end


end
