note
	description: "Summary description for {EAC_ITEM_TR_ACCOUNT}."
	author: "Howard Thomson"
	date: "11-April-2021"

	TODO: "[
		On update, when it is as yet undecided as to whather this is a debit or a credit, what happens ?
			Initially, both debit_account and credit_account are Void
			On selection of account, assign to both debit_account and credit_account,
				with value = 0
			On value entry, depending on debit or credit column, update the appropriate
				entry to correspond to this [currently displayed] account

		Move to next field on account selection ...
	]"

class
	EAC_ITEM_TR_ACCOUNT

inherit
	EAC_ITEM_TR_COMMON
		redefine
			custom_expose,
--			update_field,
			key_press_string_event
		end

create
	make

feature -- Creation

	make (a_transaction: EAC_TRANSACTION; a_transactions_window: EAC_TRANSACTIONS_WINDOW)
		do
			transaction := a_transaction
			transactions_window := a_transactions_window
			default_create
			expose_actions.extend (agent custom_expose (?, Current))
			add_events
			set_left_aligned
			set_required_width (exposure_font.string_width (text) + 10)
			set_is_tab_navigatable (True)
			set_tooltip (tooltip_text)
		end

	tooltip_text: STRING = "[
		'/' or '?' for selection dialog
		'l' or 'L' for last account entered
	]"

feature

	text: STRING
		do
			if is_debit then
				Result := credit_account_name
			else
				Result := debit_account_name
			end
		end

	default_background_text: STRING = "Account"

	custom_expose (a_drawable: EV_DRAWABLE; a_grid_item: like Current)
		local
			w, h: INTEGER
			x, y: INTEGER
			d: INTEGER
		do
			Precursor (a_drawable, a_grid_item)

--			if is_current_edit_field then
--					-- Draw the target for the popup selector ...
--				w := width
--				h := height
--				d := 10
--				x := w - 12
--				y := h // 2
--				a_drawable.draw_point (x, y)
--				a_drawable.draw_ellipse (x - (d // 2), y - (d // 2), d, d)
--			end
		end

	valid_text (a_string: STRING): BOOLEAN
			-- Is 'a_string' valid content for this field ?
		local
			s: STRING
		do
			Result := True
		end

--	update_field (a_text: STRING)
--			-- Update the associated record from this text
--		local
--			l_account: EAC_ACCOUNT
--		do
--			-- TODO ...
--		end

	key_press_string_event (a_string: STRING_32)
		do
			if 	a_string.item (1) = '/' or else a_string.item (1) = '?' then
				show_account_selector_dialogue
			elseif 	a_string.item (1) = 'l' or else a_string.item (1) = 'L' then
					-- Last account entered ...
				--TODO
				if attached transactions_window.account_selector_dialogue.selected_account_id  as id then
					set_account (id)
				end
			end
		end

	show_account_selector_dialogue
		do
			if attached transactions_window.main_window as mw then
				transactions_window.account_selector_dialogue.set_data (Current)
				transactions_window.account_selector_dialogue.show_modal_to_window (mw)
			end
		end

	set_account (a_uuid: STRING)
		require
			valid_uuid: {UUID}.is_valid_uuid (a_uuid)
		local
			t: EAC_TRANSACTION
		do
			t := transaction
			if attached current.transactions_window.current_account as ac
			and then attached ac.id as id
			and then id.is_equal (a_uuid) then
				-- Do nothing, cannot select same as current account !
			else


				if t.debit_account = Void and t.credit_account = Void then
						-- Initial assignment to account
					t.set_debit_account (a_uuid)
				else
					check current.transactions_window.current_account /= Void end
					if attached transactions_window.current_account as ca then
						check ca.id /= Void end
						if attached t.debit_account as dr  and then dr.is_equal(ca.id) then
							t.set_credit_account (a_uuid)
						else
							t.set_debit_account (a_uuid)
						end
					end
				end
				transactions_window.assign_current_edit_text (Current)






			end
		end
end
