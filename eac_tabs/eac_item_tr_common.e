note
	description: "Summary description for {EAC_ITEM_TR_COMMON}."
	author: "Howard Thomson"
	date: "11-April-2021"

	TODO: "[
		Redo draw_background ...
			For import from bank etc, colour indicate account selections that need
			to change from pre-defined import assignment ...
		
		Left and right margins, and remove leading and trailing added spaces
		currently providing those margins. Fix code to minimum width to
		include the margin space !
	
		Adjust text display position to allow for date/account popup target
		
		Display target account
		
		Split transactions
		
		Use header alignment setting, instead of per item setting
		
		Set cursor position from click in the field ...
		
		Italic font for both existing, and new records in open fields ...
	]"

	Edit_field_interaction: "[
		To be a valid transaction, the date, description, amount, and
		debit and credit account IDs must be complete, with the simple
		sequence being:
			Date: enter a valid date [there may be a default]
			Description: text to type
			Account: If both debit/credit are still void, assign to debit
			Amount
	]"

deferred class
	EAC_ITEM_TR_COMMON

inherit
	EV_GRID_DRAWABLE_ITEM
		rename data as data_renamed end
--		redefine parent end
	EAC_REPORTER
		undefine default_create, copy end
	EV_TEXT_ALIGNMENT_CONSTANTS
--		rename data as data_renamed
		undefine default_create, copy end
	EV_KEY_CONSTANTS
--		rename data as data_renamed
		undefine default_create, copy end

feature -- Attributes

	transactions_window: EAC_TRANSACTIONS_WINDOW

	transaction: EAC_TRANSACTION

feature -- Deferred

	text: STRING
		deferred
		end

	valid_text (a_string: STRING): BOOLEAN
			-- Is 'a_string' valid content for this field ?
			-- Redefine as needed ...
		deferred
--			Result := True
		end

	default_background_text: STRING
		deferred
		end

	display_text: STRING
		do
			if transactions_window.current_edit_field = Current
			then
				Result := transactions_window.current_edit_text
			elseif transaction.is_in_default_state
			then
				Result := default_background_text
			else
				Result := text
			end
		end


	max_text_size: INTEGER
			-- If >0 then this is the maximum size for this field
			-- Redefine in descendants ...
		do
			Result := 0
		end

	update_field (a_text: STRING)
			-- Update the associated record from this text
		require
			non_void_text: a_text /= Void
			valid_text: valid_text (a_text)
		do
		end

feature -- Events

	add_events
		do
			pointer_button_press_actions.extend (agent pointer_press (?,?,?,?,?,?,?,?,Current))
--			key_press_actions.extend (agent key_press)
			if attached transactions_window as tw then
				key_press_actions.extend (agent tw.key_press_grid_item)
				key_press_string_actions.extend (agent tw.key_press_string_event)
			end
		end

	pointer_press (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER; a_item: like Current)
		do
			if attached transactions_window as w then
				w.item_pointer_press (a_x, a_y, a_screen_x, a_screen_y, a_item)
			end
		end

	mouse_click_3
		do
			report ("mouse_click_3: " + text + "%N")
		end

--	draw_background (a_drawable: EV_DRAWABLE)
--		do
--			if is_current_edit_field then
--				a_drawable.set_foreground_color (stock_colors.magenta)
--			elseif is_current_edit_item then
--				a_drawable.set_foreground_color (stock_colors.grey)
--			elseif is_selected then
--				a_drawable.set_foreground_color (stock_colors.yellow)
--			elseif attached {EAC_TRANSACTION} data as d and then
--			not d.is_in_default_state and then
--			not d.is_valid_for_commit then
--				a_drawable.set_foreground_color (stock_colors.red)	-- TODO: ...
--			else
--				a_drawable.set_foreground_color (stock_colors.white)
--			end
--			a_drawable.fill_rectangle (0, 0, width, height)
--		end

	draw_background (a_drawable: EV_DRAWABLE)
			-- Draw background to this editable item:
			--	White for existing completed entries
			--	White for current edit item
			--	Grey for new record, no data entered
			--	Red for incomplete/invalid data entered
			--	Green for valid data entered
			-- ??? for import account needing re-allocation
		local
			l_is_current_edit_field: BOOLEAN
			l_is_new_record: BOOLEAN
			l_default_state: BOOLEAN
			l_valid_for_commit: BOOLEAN
		do
--			if attached {EAC_TRANSACTION} data as d then
				l_default_state := transaction.is_in_default_state
				l_valid_for_commit := transaction.is_valid_for_commit
--			end

			if is_current_edit_field then
				a_drawable.set_foreground_color (stock_colors.white)
			elseif is_current_edit_item then
				a_drawable.set_foreground_color (stock_colors.white)
			elseif l_default_state then
				a_drawable.set_foreground_color (stock_colors.grey)
--			elseif False then
--				a_drawable.set_foreground_color (stock_colors.dark_magenta)
			else
				a_drawable.set_foreground_color (stock_colors.white)
			end



--				if valid_text (transactions_window.current_edit_text) then
--					a_drawable.set_foreground_color (stock_colors.green)
--		--		elseif  then
--				else
--					a_drawable.set_foreground_color (stock_colors.magenta)
--				end

--			elseif is_current_edit_item then
--				a_drawable.set_foreground_color (stock_colors.grey)
--			elseif is_selected then
--				a_drawable.set_foreground_color (stock_colors.yellow)
--			elseif attached {EAC_TRANSACTION} data as d and then
--			not d.is_in_default_state and then
--			not d.is_valid_for_commit then
--				a_drawable.set_foreground_color (stock_colors.red)	-- TODO: ...
--			else
--				a_drawable.set_foreground_color (stock_colors.white)
--			end
			a_drawable.fill_rectangle (0, 0, width, height)
		end

	is_debit: BOOLEAN
		do
			if
		--	attached {EAC_TRANSACTION} data as d
		--	and then
			attached transaction.debit_account as l_dr
			and then attached transactions_window as w
			and then attached w.current_account as ca
			then
				if l_dr.is_equal (ca.id) then
					Result := True
				end
			end
		end

	debit_account_name: STRING
		do
			if
		--	attached {EAC_TRANSACTION} data as d
		--	and then
			attached transaction.debit_account as l_dr
			then
				Result := account_name (l_dr)
			else
				Result := ""
			end
		end

	credit_account_name: STRING
		do
			if
		--	attached {EAC_TRANSACTION} data as d
		--	and then
			attached transaction.credit_account as l_cr
			then
				Result := account_name (l_cr)
			else
				Result := ""
			end
		end

	account_name (a_uuid: STRING): STRING
			-- Concatenated name components of this account
		local
			l_str: STRING
			l_parent_uuid: detachable STRING
		do
			create l_str.make (25)
			if attached transactions_window as w
			and then attached w.eac_data.eac_accounts as a
			then
				from
					a.search (a_uuid)
					if a.found then
						if attached a.found_item as i then
							if attached i.name as n then
								l_str.prepend (n)
							end
							l_parent_uuid := i.parent_id
						end
					end
				until l_parent_uuid = Void
				loop
					a.search (l_parent_uuid)
					if a.found then
						if attached a.found_item as i then
							if i.type /= {EAC_ACCOUNT}.type_root then
								if attached i.name as n then
									l_str.prepend ("::")
									l_str.prepend (n)
								end
								l_parent_uuid := i.parent_id
							else
								l_parent_uuid := Void
							end
						end
					end
				end
			end
			Result := l_str
		end


feature -- Alignment

	text_alignment: INTEGER

feature -- Alignment queries

	is_left_aligned: BOOLEAN
			-- Is `Current' left aligned?
		do
			Result := text_alignment = {EV_TEXT_ALIGNMENT_CONSTANTS}.Ev_text_alignment_left
		end

	is_center_aligned: BOOLEAN
			-- Is `Current' center aligned?
		do
			Result := text_alignment = {EV_TEXT_ALIGNMENT_CONSTANTS}.Ev_text_alignment_center
		end

	is_right_aligned: BOOLEAN
			-- Is `Current' right aligned?
		do
			Result := text_alignment = {EV_TEXT_ALIGNMENT_CONSTANTS}.Ev_text_alignment_right
		end

feature -- Alignment setting

	set_left_aligned
			-- Ensure `Current' is left aligned
		do
			text_alignment := {EV_TEXT_ALIGNMENT_CONSTANTS}.Ev_text_alignment_left
		end

	set_center_aligned
			-- Ensure `Current' is center aligned
		do
			text_alignment := {EV_TEXT_ALIGNMENT_CONSTANTS}.Ev_text_alignment_center
		end

	set_right_aligned
			-- Ensure `Current' is right aligned
		do
			text_alignment := {EV_TEXT_ALIGNMENT_CONSTANTS}.Ev_text_alignment_right
		end

feature -- Margins, left and right

	left_margin: INTEGER = 4

	right_margin: INTEGER = 4

feature -- Implementation

	custom_expose (a_drawable: EV_DRAWABLE; a_grid_itemXX: like Current)
			--TODO: find a simpler solution to using string_width on a substring ...
			-- Fix the right margin !
		local
			w: INTEGER
			t: STRING
			l_font: EV_FONT
			l_indent: INTEGER
			l_edit_position: INTEGER
		do
			l_edit_position := 1
				-- Draw the background
			draw_background (a_drawable)
				-- Draw the text
			a_drawable.set_foreground_color (stock_colors.black)
			t := display_text
			l_font := exposure_font

			if t.count = 0 then
				t := default_background_text
				l_font := italic_feint_font
			end

			if is_current_edit_field then
					--TODO distinguish between partially filled current text, and default 'empty' text
				if attached transactions_window as tw then
					t := tw.current_edit_text
					if t.count /= 0 then
						l_font := exposure_font
					end
					l_edit_position := tw.current_edit_position
				end
			end
			a_drawable.set_font (l_font)
			w := l_font.string_width (t)
			if is_left_aligned then
				a_drawable.draw_text_top_left (left_margin + 0, 0, t)
				l_indent := 0
			elseif is_center_aligned then	-- ???
				if w >= width then
					w := 0
				else
					w := (width - w) // 2
				end
				a_drawable.draw_text_top_left (left_margin + w, 0, t)
				l_indent := w
			else -- is_right_aligned
					w := width - w - right_margin
			--		check width >= (left_margin + ) end
					a_drawable.draw_text_top_left (left_margin + w, 0, t)
					l_indent := w
			end
				-- Draw the cursor
			if is_current_edit_field then
					--TODO: Adapt for current edit cursor position ...
				if l_edit_position > 1 then
					w := l_indent
					if t.count > 0 and (l_edit_position - 1) > 0 then
						w := w + font_substring_width (l_font, t, 1, l_edit_position - 1)
					end
				else
					w := l_indent
				end
				a_drawable.set_line_width (2)
				a_drawable.draw_straight_line (left_margin + w, 0, left_margin + w, a_drawable.height)
			end
		end

	font_substring_width (a_font: EV_FONT; a_string: STRING; a_from, a_to: INTEGER): INTEGER
			-- Return the font.string_width value for the substring of a_string
			-- between a_from and a_to
		require
			count_non_zero: a_string.count > 0
			a_to_ge_a_from: a_to >= a_from
			yyy: (a_to - a_from + 1) <= a_string.count
		local
			s: STRING
		do
			if a_from = 1 and a_to = a_string.count then
				Result := a_font.string_width (a_string)
			else
				s := a_string.substring (a_from, a_to)
				Result := a_font.string_width (s)
			end
		end

	is_current_edit_item: BOOLEAN
		do
			if attached transactions_window as w then
				if w.current_edit_transaction = transaction then
					Result := True
				end
			end
		end

	is_current_edit_field: BOOLEAN
		do
			if attached transactions_window as w then
				if w.current_edit_field = Current then
					Result := True
				end
			end
		end

	is_editable: BOOLEAN
			-- Is the field editable ?
			-- Default True, redefine in descendants if false ...
		do
			Result := True
		end

	exposure_font: EV_FONT
		once ("PROCESS")
			create Result.default_create
			Result.set_weight (7)
			Result.set_family (3)
			Result.set_shape (10)
		end

	italic_feint_font: EV_FONT
		once ("PROCESS")
			create Result.default_create
			Result.set_weight (Result.weight_thin)
			Result.set_family (3)
			Result.set_shape (Result.shape_italic)
		end

	stock_colors: EV_STOCK_COLORS
			-- Once access to EiffelVision2 stock colors
		once
			create Result
		end

	is_header_aligned_left: BOOLEAN
		local
			h: EV_GRID_HEADER_ITEM
			c: EV_GRID_COLUMN
		do
			if attached parent as p then
				c := p.column_at_virtual_position (virtual_x_position)
				if attached c then
					Result := c.header_item.is_left_aligned
				end
			end
		end

feature -- Key event processing

	key_press_event (a_key: EV_KEY)
		local
			current_edit_position: INTEGER
			current_edit_text: STRING
		do
			check not a_key.is_printable then end

			if attached transactions_window as w then
				current_edit_text := w.current_edit_text
				current_edit_position := w.current_edit_position

				inspect a_key.code

				when key_back_space then
					if current_edit_position > 1 then
						current_edit_text.replace_substring ("", current_edit_position -1, current_edit_position -1)
						w.set_current_edit_position (current_edit_position - 1)
					end
				when key_delete then
					if current_edit_position = 1 and current_edit_text.count = 1 then
						current_edit_text.wipe_out
					elseif current_edit_position < current_edit_text.count + 1 then
						current_edit_text.replace_substring ("", current_edit_position, current_edit_position)
					end
				when key_left then
					if current_edit_position > 1 then
						w.set_current_edit_position (current_edit_position - 1)
					end
				when key_right then
					if current_edit_position <= current_edit_text.count then
						w.set_current_edit_position (current_edit_position + 1)
					end
				when key_escape then
					if attached transactions_window.last_key_press as kp and then kp.code = key_escape then
							-- Reset associated transaction to defaults
						transaction.reset
							-- redraw as needed ...
						--TODO
					else
							-- Restore to original content ...
						current_edit_text.copy (text)
					end
				end
			end
		end

	key_press_string_event (a_string: STRING_32)
		require
			a_string.count = 1
		local
			l_max: INTEGER
			current_edit_position: INTEGER
			current_edit_text: STRING
		do
			if attached transactions_window as w then
				current_edit_text := w.current_edit_text
				current_edit_position := w.current_edit_position
				if a_string.item (1) /= '%N' then
					l_max := max_text_size
					if (l_max = 0 or else l_max > current_edit_text.count)
					and then a_string.item (1) /= '%T'
					then
						current_edit_text.insert_string (a_string, current_edit_position)
					--	current_edit_position := current_edit_position + 1
						w.set_current_edit_position (current_edit_position + 1)
						redraw
					end
					w.set_focus
					w.debug_window.update (w)
				end
			end
		end


invariant
	valid_text_alignment: valid_alignment (text_alignment)
end
