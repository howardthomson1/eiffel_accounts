
class APPLICATION_ROOT

inherit
	AEL_V2_APPLICATION
 		redefine
 			attach_application_root
 		--	create_main_window
 		end

create
	make_and_launch
--default_create,
create {AEL_APPLICATION_ENV}
	make_invalid

--|========================================================================
feature {NONE} -- Core initialization
--|========================================================================

 	attach_application_root
 			-- Force attachement at earliest opportunity (from initialize)
 		do
 			Precursor
 			application_root_cell.put (Current)
 		end

-- 	create_main_window
-- 			-- Create the main_window object
-- 		do
-- 			create main_window.make
-- 		end

end -- class APPLICATION_ROOT
