note
	description: "Extension of EV_KEY"
	author: "Howard Thomson"
	date: "16-May-2021"

	extended_description: "[
		Facilities for mapping keycodes to characters/strings, taking
		the Shift key into account ...
	]"

	TODO: "[
		Map'#' to '~' ...
		
		
		#   Key event: 4 : 3
		~   Key event: 56 : `

		'   Key event: 55 : ' 
		@   Key event: 3 : 2

		3   Key event: 4 : 3
		�   No event !!!

		2   Key event: 3 : 2 
		"   Key event: 55 : ' 

		`   Key event: 56 : `
		�   No event !!!
	]"

class
	EAC_KEYMAP

inherit
	EV_KEY_CONSTANTS

feature

	mapped_key (a_key: EV_KEY): CHARACTER_32
		require
			is_printable: a_key.is_printable
		do
			if not application.shift_pressed then
				Result := a_key.text.item (1)
			else
				Result := shift_key_map.item (a_key.code)
			end
		end

	mapped_key_string (a_key: EV_KEY): STRING
		require
			is_printable: a_key.is_printable
		do
			if not application.shift_pressed then
				Result := a_key.text
			else
				create Result.make_empty
				Result.append_character (shift_key_map.item (a_key.code).to_character_8)
			end
		end


feature {NONE}

	application: EV_APPLICATION
		once
			create Result
		end

	shift_key_map: ARRAY [ CHARACTER_32 ]
		once
			create Result.make_filled (' ', 1, Key_menu)


			Result.put ({CHARACTER_32} ')', Key_0)
			Result.put ({CHARACTER_32} '!', Key_1)
			Result.put ({CHARACTER_32} '"', Key_2)
			Result.put ({CHARACTER_32} '�', Key_3)
			Result.put ({CHARACTER_32} '$', Key_4)
			Result.put ({CHARACTER_32} '%%', Key_5)
			Result.put ({CHARACTER_32} '^', Key_6)
			Result.put ({CHARACTER_32} '&', Key_7)
			Result.put ({CHARACTER_32} '*', Key_8)
			Result.put ({CHARACTER_32} '(', Key_9)
--			Result.put ({CHARACTER_32} "NumPad 0", Key_numpad_0)
--			Result.put ({CHARACTER_32} "NumPad 1", Key_numpad_1)
--			Result.put ({CHARACTER_32} "NumPad 2", Key_numpad_2)
--			Result.put ({CHARACTER_32} "NumPad 3", Key_numpad_3)
--			Result.put ({CHARACTER_32} "NumPad 4", Key_numpad_4)
--			Result.put ({CHARACTER_32} "NumPad 5", Key_numpad_5)
--			Result.put ({CHARACTER_32} "NumPad 6", Key_numpad_6)
--			Result.put ({CHARACTER_32} "NumPad 7", Key_numpad_7)
--			Result.put ({CHARACTER_32} "NumPad 8", Key_numpad_8)
--			Result.put ({CHARACTER_32} "NumPad 9", Key_numpad_9)
--			Result.put ({CHARACTER_32} "NumPad +", Key_numpad_add)
--			Result.put ({CHARACTER_32} "NumPad /", Key_numpad_divide)
--			Result.put ({CHARACTER_32} "NumPad *", Key_numpad_multiply)
--			Result.put ({CHARACTER_32} "NumLock", Key_num_lock)
--			Result.put ({CHARACTER_32} "NumPad -", Key_numpad_subtract)
--			Result.put ({CHARACTER_32} "NumPad .", Key_numpad_decimal)
--			Result.put ({CHARACTER_32} "F1", Key_f1)
--			Result.put ({CHARACTER_32} "F2", Key_f2)
--			Result.put ({CHARACTER_32} "F3", Key_f3)
--			Result.put ({CHARACTER_32} "F4", Key_f4)
--			Result.put ({CHARACTER_32} "F5", Key_f5)
--			Result.put ({CHARACTER_32} "F6", Key_f6)
--			Result.put ({CHARACTER_32} "F7", Key_f7)
--			Result.put ({CHARACTER_32} "F8", Key_f8)
--			Result.put ({CHARACTER_32} "F9", Key_f9)
--			Result.put ({CHARACTER_32} "F10", Key_f10)
--			Result.put ({CHARACTER_32} "F11", Key_f11)
--			Result.put ({CHARACTER_32} "F12", Key_f12)
--			Result.put ({CHARACTER_32} "Space", Key_space)
--			Result.put ({CHARACTER_32} "BackSpace", Key_back_space)
--			Result.put ({CHARACTER_32} "Enter", Key_enter)
--			Result.put ({CHARACTER_32} "Esc", Key_escape)
--			Result.put ({CHARACTER_32} "Tab", Key_tab)
--			Result.put ({CHARACTER_32} "Pause", Key_pause)
--			Result.put ({CHARACTER_32} "CapsLock", Key_caps_lock)
--			Result.put ({CHARACTER_32} "ScrollLock", Key_scroll_lock)
			Result.put ({CHARACTER_32} '<', Key_comma)
			Result.put ({CHARACTER_32} '+', Key_equal)
			Result.put ({CHARACTER_32} '>', Key_period)
			Result.put ({CHARACTER_32} ':', Key_semicolon)
			Result.put ({CHARACTER_32} '{', Key_open_bracket)
			Result.put ({CHARACTER_32} '}', Key_close_bracket)
			Result.put ({CHARACTER_32} '?', Key_slash)
			Result.put ({CHARACTER_32} '|', Key_backslash)
			Result.put ({CHARACTER_32} '@', Key_quote)
			Result.put ({CHARACTER_32} '�', Key_backquote)
			Result.put ({CHARACTER_32} '_', Key_dash)
--			Result.put ({CHARACTER_32} "Up", Key_up)
--			Result.put ({CHARACTER_32} "Down", Key_down)
--			Result.put ({CHARACTER_32} "Left", Key_left)
--			Result.put ({CHARACTER_32} "Right", Key_right)
--			Result.put ({CHARACTER_32} "PageUp", Key_page_up)
--			Result.put ({CHARACTER_32} "PageDown", Key_page_down)
--			Result.put ({CHARACTER_32} "Home", Key_home)
--			Result.put ({CHARACTER_32} "End", Key_end)
--			Result.put ({CHARACTER_32} "Insert", Key_insert)
--			Result.put ({CHARACTER_32} "Del", Key_delete)
--			Result.put ({CHARACTER_32} "a", Key_a)
--			Result.put ({CHARACTER_32} "b", Key_b)
--			Result.put ({CHARACTER_32} "c", Key_c)
--			Result.put ({CHARACTER_32} "d", Key_d)
--			Result.put ({CHARACTER_32} "e", Key_e)
--			Result.put ({CHARACTER_32} "f", Key_f)
--			Result.put ({CHARACTER_32} "g", Key_g)
--			Result.put ({CHARACTER_32} "h", Key_h)
--			Result.put ({CHARACTER_32} "i", Key_i)
--			Result.put ({CHARACTER_32} "j", Key_j)
--			Result.put ({CHARACTER_32} "k", Key_k)
--			Result.put ({CHARACTER_32} "l", Key_l)
--			Result.put ({CHARACTER_32} "m", Key_m)
--			Result.put ({CHARACTER_32} "n", Key_n)
--			Result.put ({CHARACTER_32} "o", Key_o)
--			Result.put ({CHARACTER_32} "p", Key_p)
--			Result.put ({CHARACTER_32} "q", Key_q)
--			Result.put ({CHARACTER_32} "r", Key_r)
--			Result.put ({CHARACTER_32} "s", Key_s)
--			Result.put ({CHARACTER_32} "t", Key_t)
--			Result.put ({CHARACTER_32} "u", Key_u)
--			Result.put ({CHARACTER_32} "v", Key_v)
--			Result.put ({CHARACTER_32} "w", Key_w)
--			Result.put ({CHARACTER_32} "x", Key_x)
--			Result.put ({CHARACTER_32} "y", Key_y)
--			Result.put ({CHARACTER_32} "z", Key_z)
--			Result.put ({CHARACTER_32} "Shift", Key_shift)
--			Result.put ({CHARACTER_32} "Ctrl", Key_ctrl)
--			Result.put ({CHARACTER_32} "Alt", Key_alt)
--			Result.put ({CHARACTER_32} "Left Meta", Key_left_meta)
--			Result.put ({CHARACTER_32} "Right Meta", Key_right_meta)
--			Result.put ({CHARACTER_32} "Menu", Key_menu)
		end

end
