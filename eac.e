note
	description: "Root class for the Eiffel Accounting system"
	author: "Howard Thomson"
	TODO: "[
		Make optional startup of the storage thread
		
		Ensure storage thread exits when GUI finishes ...
		Move the XML/Gnucash-data import activity into a new thread.
	]"
class
	EAC

inherit

	THREAD_CONTROL
	EXECUTION_ENVIRONMENT
	MEMORY

create
	make

feature

	dbm: EAC_STORE
--	dbm_client: EAC_STORE_CLIENT

	gui: EAC_GUI


	make
		do
				-- Start other threads ...
			create dbm.make; dbm.launch
			sleep (10_000)
--			create dbm_client.make; dbm_client.launch
				-- GUI thread
			create gui.make; gui.launch
				-- Wait for threads to terminate
			join_all
		end

end
