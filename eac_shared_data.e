note
	description: "Summary description for {EAC_SHARED_DATA}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

class
	EAC_SHARED_DATA

feature

	eac_data: EAC_DATA
		once
			create Result
		end

	eac_terminate: CELL [BOOLEAN]
			-- Flag for all threads to coordinate process termination
		once ("process")
			create Result.put (False)
		end

	eac_windows: EAC_WINDOWS
		once
			create Result
		end
end
