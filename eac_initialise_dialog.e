note
	description: "Dialog to initialise a new Accounts Datastore"
	author: "Howard Thomson"
	date: "17-Nov-2020"

	TODO: "[
		Select between file storage and database store
		Specify filename and location
		Select whether initial import from GnuCash, or new initial accounts
		Username and password for admin access

		Improve validation ...
	]"

class
	EAC_INITIALISE_DIALOG

inherit
	EV_DIALOG
		redefine
			create_interface_objects,
			initialize,
			show_relative_to_window,
			show_modal_to_window
		end



	EAC_REPORTER
		undefine default_create, copy end

create
	default_create

feature {EAC_MAIN_WINDOW} -- GUI display elements

	factory: EV_WIDGET_FACTORY

	vbox: EV_VERTICAL_BOX

	hbox_1: EV_HORIZONTAL_BOX
	path_label: EV_LABEL
	storage_path: EV_TEXT_FIELD

	hbox_2: EV_HORIZONTAL_BOX
	name_label: EV_LABEL
	storage_name: EV_TEXT_FIELD

	hbox_3: EV_HORIZONTAL_BOX
	user_label: EV_LABEL
	user_name: EV_TEXT_FIELD

	hbox_4: EV_HORIZONTAL_BOX
	pwd_label: EV_LABEL
	password: EV_PASSWORD_FIELD

	hbox_5: EV_HORIZONTAL_BOX
	url_label: EV_LABEL
	host_url: EV_TEXT_FIELD

	hbox_6: EV_HORIZONTAL_BOX
	ok_button: EV_BUTTON
	cancel_button: EV_BUTTON

	status_text: EV_TEXT_FIELD

feature {NONE}

	create_interface_objects
		do
			Precursor
			create factory
			create vbox; factory.register_widget (vbox, "vbox")
		--	vbox := factory.ev_vertical_box ("vbox")
			create hbox_1
			create hbox_2
			create hbox_3
			create hbox_4
			create hbox_5
			create hbox_6

			create path_label;	create storage_path
			create name_label;	create storage_name
			create url_label;	create host_url
			create user_label;	create user_name
			create pwd_label;	create password
			create ok_button;	create cancel_button

			create status_text

			ok_button.set_text ("Connect")
			cancel_button.set_text ("Cancel")
		end

	initialize
		do
			Precursor
			set_title ("Initialise a new accounts datastore ...")
			extend (vbox)
			path_label.set_text ("File path:")
			name_label.set_text ("Filename :")
			user_label.set_text ("Username :")
			pwd_label.set_text  ("Password :")
			url_label.set_text  ("Host URL :")

			hbox_1.extend (path_label)
			hbox_1.extend (storage_path)
			vbox.extend (hbox_1)

			hbox_2.extend (name_label)
			hbox_2.extend (storage_name)
			vbox.extend (hbox_2)

			hbox_3.extend (user_label)
			hbox_3.extend (user_name)
			vbox.extend (hbox_3)

			hbox_4.extend (pwd_label)
			hbox_4.extend (password)
			vbox.extend (hbox_4)

			hbox_5.extend (url_label)
			hbox_5.extend (host_url)
			vbox.extend (hbox_5)

			hbox_6.extend (ok_button)
			hbox_6.extend (cancel_button)
			vbox.extend (hbox_6)

			vbox.extend (status_text)

			key_press_actions.extend (agent cancel)

			vbox.key_press_actions.extend (agent cancel)
			cancel_button.key_press_actions.extend (agent cancel)

			vbox.resize_actions.extend (agent fixed_resized)
			user_name.focus_out_actions.extend (agent user_name_leave_focus)

			ok_button.select_actions.extend (agent set_ok_selected)
			cancel_button.select_actions.extend (agent destroy)
		end

feature

	cancel (a_key: EV_KEY)
		do
			if a_key.code.is_equal (a_key.key_escape) then
				destroy
			end
		end

	ok_selected: BOOLEAN

	set_ok_selected
		do
			if valid_entries then
				ok_selected := True
				destroy
			else
				status_text.set_text ("Invalid and/or missing entries")
			end
		end

	valid_entries: BOOLEAN
		do
			if storage_path.text_length > 0
			and then storage_name.text_length > 0
			and then user_name.text_length > 0
			and then password.text_length > 0
			then
				Result := True
			end
		end

	user_name_leave_focus
		do
			if host_url.text.is_empty then
					-- TEMP !!!
				if user_name.text.is_equal ("jt") then
					host_url.set_text ("tcp://ht-shuttle")
				elseif user_name.text.is_equal ("ht") then
					host_url.set_text ("inproc://eac_test")
				end
			end
		end

	fixed_resized (x, y, w, h: INTEGER_32)
		local
			l_w: INTEGER
		do
			l_w := w - user_name.x_position - 15
--			fixed.set_item_width (storage_path, l_w)
--			fixed.set_item_width (storage_name, l_w)
--			fixed.set_item_width (user_name, l_w)
--			fixed.set_item_width (password, l_w)
--			fixed.set_item_width (host_url, l_w)
		end

	show_relative_to_window (a_window: EV_WINDOW)
		do
			load_defaults
			ok_selected := False
			Precursor (a_window)
		end

	show_modal_to_window (a_window: EV_WINDOW)
		do
			load_defaults
			ok_selected := False
			Precursor (a_window)
		end

	load_defaults
		do
		end

end

