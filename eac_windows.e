note
	description: "Shared access to windows in EAC"
	author: "Howard Thomson"
	date: "24-April-2021"

class
	EAC_WINDOWS

feature -- Access

	main_window: detachable EAC_MAIN_WINDOW

	accounts_window: detachable EAC_ACCOUNTS_WINDOW

feature -- Setting

	set_main_window (a_window: EAC_MAIN_WINDOW)
		do
			main_window := a_window
		end

	set_accounts_window (a_window: EAC_ACCOUNTS_WINDOW)
		do
			accounts_window := a_window
		end


end
