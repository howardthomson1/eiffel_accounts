note
	description: "Summary description for {EAC_PRINT}."
	author: "Howard Thomson"
	place_holder: "[
		This is a place-holder class for accessing PDF generation facilities
		using the Haru 'C' PDF print library.
		
		https://github.com/libharu/libharu
		
		/data/git/libharu
		
		Use WrapC to generate Eiffel wrapping classes ... ?
		
		Use new_cursor: ITERATION_CURSOR [ EV_WIDGET ] to iterate
		ove the structure of a window, to generate the print data
		
		Either that, or I need a Visitor class for the widget hierarchy.
	]"

class
	EAC_PRINT

inherit
	EAC_REPORTER

	AEL_PRINTF

feature

	print_window (a_window: EV_WINDOW)
		local
			l_cursor: ITERATION_CURSOR [ EV_WIDGET ]
			l_widget: EV_WIDGET
		do
			from
				l_cursor := a_window.new_cursor
			until
				l_cursor.after
			loop
				l_widget := l_cursor.item

				report ("Widget at x: "); report (l_widget.screen_x.out);
				report ("  y: "); report (l_widget.screen_y.out)
				report ("%N%N")

				l_cursor.forth
			end
		end

	formatted_currency_string (a_value: INTEGER_64): STRING
		local
			l_value: INTEGER_64
			l_str: STRING
		do
			create Result.make_from_string ("�")
			if a_value < 0 then
				l_value := - a_value
				Result.prepend ("- ")
			else
				l_value := a_value
			end
			Result.append (aprintf ("%%#d", l_value // 100))
			Result.append_character ('.')
		--	Result.append (aprintf ("%%#2.2d", l_value \\ 100))
				-- Generate the last two digits, preceeded by a '1'
			l_str := aprintf ("%%d", (l_value \\ 100) + 100)
			l_str.keep_tail (2)
			Result.append (l_str)
--			Result.append (" ")
		end


--	items_recursive (a_widget: EV_WIDGET)
--			-- Does structure include `an_item` or
--			-- does any structure recursively included by structure,
--			-- include `an_item`.
--			-- (from EV_CONTAINER)
--		require -- from EV_CONTAINER
--			not_destroyed: not a_widget.is_destroyed
--		local
--			lst: LINEAR [EV_WIDGET]
--			c: detachable CURSOR
--			cs: detachable CURSOR_STRUCTURE [EV_WIDGET]
--		do
--			Result := a_widget.has (an_item)
--			lst := a_widget.linear_representation
--			if attached {CURSOR_STRUCTURE [EV_WIDGET]} lst as l_cursor_structure then
--				cs := l_cursor_structure
--				c := l_cursor_structure.cursor
--			end
--			across
--				lst as ic
--			until
--				Result
--			loop
--				if attached {EV_CONTAINER} ic.item as ct then
--					Result := ct.has_recursive (an_item)
--				end
--			end
--			if cs /= Void and then c /= Void then
--				cs.go_to (c)
--			end
--		end


end
