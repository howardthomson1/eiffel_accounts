note
	description: "Summary description for {EAC_MESSAGE_FACTORY}."
	author: "Howard Thomson"
	date: "$Date$"
	revision: "$Revision$"

	extended_description: "[
		This factory generates messages [EAC_MESSAGE] for request/reply
		communication with the EAC store process.
	]"

class
	EAC_MESSAGE_FACTORY

inherit
	EAC_MSG_TYPE_IDS

create
	make

feature {NONE} -- Creation

	make
		do
		--	create last_message.make_gc_delay
		end

feature

--	send_message (

--	receive_message (a_message: NNG_MESSAGE)
--			-- Convert a raw received message into a processable class member
--		require
--			message_has_content: a_message.has_content
--		local
--			l_msg_type: INTEGER_32
--		do
--			l_msg_type := a_message.trim_integer_32
--			inspect l_msg_type
--			when EAC_GC_CYCLE then
--				{EXECUTION_ENVIRONMENT}.sleep (10_000)
--			else
--				check False end
--			end

--		end

--	last_message: EAC_MESSAGE


end
