note
	description: "Summary description for {EAC_ACCOUNT_SELECTION_GRID}."
	author: "Howard Thomson"
	date: "$24-Sep-2021$"
	revision: "$Revision$"

class
	EAC_ACCOUNT_SELECTION_GRID

inherit
	EAC_GRID_COMMON

feature

	setup_columns
		do

		end

	update_status_bar
		do

		end

	mouse_click (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
		do
		end

end
