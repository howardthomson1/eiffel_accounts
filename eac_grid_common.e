note
	description: "Summary description for {EAC_GNUCASH_TAB}."
	author: "Howard Thomson"
	date: "23-May-2020"

	TODO: "[
		Sortable columns ...
		Events, identifying the column/row ...
	]"

deferred class
	EAC_GRID_COMMON

inherit
--	EV_GRID
	JV_GRID

	EAC_SHARED_DATA
		undefine
			default_create, copy
		end

	EAC_PRINT
		undefine
			default_create, copy
		end

feature -- Creation

	make_common
		do
			enable_tree
			set_minimum_size (300, 300)

			setup_columns

			enable_row_separators
			enable_column_separators
			enable_single_row_selection
		--	enable_item_tab_navigation

			set_row_height (row_height + 2)
			setup_actions
		end

	setup_columns
		deferred
		end

	main_window: detachable EV_WINDOW
			-- TODO: find a better way to do this ?
		local
			l_widget: EV_WIDGET
			l_window: EV_WINDOW
		do
			from l_widget := Current
			until Result /= Void or else l_widget = Void
			loop
				if attached l_widget.parent as lp then
					l_widget := lp
					if attached {EV_WINDOW} lp as lw then
						Result := lw
					end
				end
			end
		end

feature -- Actions, referred to by agents

	setup_actions
			-- Add agent actions
		do
				-- Enable scrolling within the accounts area
			mouse_wheel_actions.extend (agent mouse_wheel)

--			column (2).header_item.pointer_button_press_actions.extend (agent mouse_click)

				-- Popup menu, when mouse right-click
			pointer_button_press_actions.extend (agent mouse_click)
				-- Update status bar when this tab selected
			focus_in_actions.extend (agent update_status_bar)
				-- Event tree node expanded
			row_expand_actions.extend (agent row_has_been_expanded)
		end

	mouse_wheel (a_steps: INTEGER)
		local
			i: INTEGER
		do
				-- EiffelVision BUG
				-- When the scroll_bar is not visible, step_forward/back does not work correctly ...
			if vertical_scroll_bar.is_displayed then
				if a_steps > 0 then
					from i := 1 until i > a_steps loop
						vertical_scroll_bar.step_backward
						i := i + 1
					end
				else
					from i := 1 until i > (- a_steps) loop
						vertical_scroll_bar.step_forward
						i := i + 1
					end
				end
			end
		end

	expand_all_tree
		local
			l_tree_row: EV_GRID_ROW
			i: INTEGER
		do
			from i := 1
			until i > row_count
			loop
				expand_row (row (i))
				i := i + 1
			end
		end

	expand_row (a_grid_row: EV_GRID_ROW)
		local
			i: INTEGER
		do
			if a_grid_row.is_expandable then
				a_grid_row.expand
				from i := 1
				until i >= a_grid_row.subrow_count
				loop
					expand_row (a_grid_row.subrow (i))
					i := i + 1
				end
			end
		end

	update_status_bar
		deferred
		end

	row_has_been_expanded (a_row: EV_GRID_ROW)
		do
				-- Resize columns here ?
--			report ("Row has been expanded ...%N")
			-- TODO: Make deferred, and define in descendants
		end

	mouse_click (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
		deferred
		end
end
