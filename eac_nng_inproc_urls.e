note
	description: "{EAC_NNG_INRPOC_URLS} Urls for NNG Inproc Protocol within EAC"
	author: "Howard Thomson"
	date: "$Date$"

class
	EAC_NNG_INPROC_URLS

feature

	inproc_url_data_store: STRING = "inproc://EAC-DataStore"
		-- LibNNG inproc protocol string for access to the Data Store

	inproc_url_gnucash_import: STRING = "inproc://GnuCash-Import"
		-- LibNNG inproc protocol string for the GnuCash import thread
	
end
