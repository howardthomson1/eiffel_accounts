note
	description: "Report (print) tracing for Eiffel Accounts"
	author: "Howard Thomson"
	date: "17-April-2020"
	revision: "$Revision$"

class
	EAC_REPORTER

feature -- Vision connection

	report_window: EV_TEXT
		once ("thread")
			create Result
			Result.disable_edit
		end

feature -- Routines for output reporting

	report (a_string: STRING)
		do
			report_window.append_text (a_string)
				-- Update the display, before continuing other
				-- potentially lengthy activities ...
			report_window.scroll_to_end
--			if attached environment.application as l_app then
--				l_app.process_events
--			end
		end

	report_while_idle (a_string: STRING)
		do
			report_window.append_text (a_string)
		end

	report_nl (a_string: STRING)
		do
			report (a_string)
			report (once "%N")
		end

feature {NONE}

	environment: EV_ENVIRONMENT
		once
			Create Result
		end
end
