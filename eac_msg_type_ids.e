note
	description: "Summary description for {EAC_MSG_TYPE_IDS}."
	author: "Howard Thomson"
	date: "15-May-2020"

	Extended_description: "[
		Integer Type IDs for NanoMsg messages between the thread
		of the EAC program
	]"
class
	EAC_MSG_TYPE_IDS

feature -- Message type IDs

		-- Garbage Collector coordination
--	EAC_GC_CYCLE: INTEGER = 1

	EAC_DB_INIT: INTEGER = 2
	EAC_DB_OPEN: INTEGER = 3
	EAC_DB_COMMIT: INTEGER = 4
	EAC_DB_CLOSE: INTEGER = 5

	EAC_ACCOUNT_READ_ROOT: INTEGER = 6

feature -- Message type IDs

		-- Odd numbers for Requests
		-- Even numbers for Replys
	Msg_connect: INTEGER = 1
	Rsp_login: INTEGER = 2

--	Msg_


end
