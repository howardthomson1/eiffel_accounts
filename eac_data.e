note
	description: "Summary description for {EAC_DATA}."
	author: "Howard Thomson"
	date: "May 2020"

	TODO: "[
		Need a generic sequence of events/actions for starting/committing/
		notifying all parties to changes in the data ...
	
		Move eac_stored !
		Move eac_sockets elsewhere ?
		xml_ stuff should be local to the import code
	]"
class
	EAC_DATA

inherit
	ANY
		redefine
			default_create
		end

feature -- Creation

	default_create
		do
			create eac_accounts.make (1000)
			create eac_transactions.make (1000)
		end

feature -- Native data transformation

	eac_accounts: HASH_TABLE [EAC_ACCOUNT, STRING]
		-- Native accounts, indexed by GUID

	eac_transactions: HASH_TABLE [EAC_TRANSACTION, STRING]
		-- Native transactions, indexed by group GUID
		-- Where no. splits = 2, indexed by transaction GUID
		--	 In this case, there is only on native record
		-- Where no. splits >=3, indexed by split GUID
		--   In this case there is one record per split

	eac_sockets: ARRAY [ NNG_SOCKET ]
		once ("process")
			create Result.make_empty
		end

feature -- Data replacement -- TEMP

	set_accounts (a_accounts: like eac_accounts)
		do
			eac_accounts := a_accounts
		end

	set_transactions (a_transactions: like eac_transactions)
		do
			eac_transactions := a_transactions
		end

feature




end
