note
	description: "Summary description for {EAC_ACCOUNT_SELECTOR}."
	author: "Howard Thomson"
	date: "21-May-2021"

	extended_description: "[
		Popup window for selecting an EAC_ACCOUNT from the available
		accounts.
	]"

	TODO: "[
		Inherit from EV_DIALOG ...
	]"
class
	EAC_ACCOUNT_SELECTOR

inherit
	EAC_GRID_COMMON
--	EV_DIALOG

create
	make_selector

feature -- Creation

	make_selector
		do
			default_create
		end

	setup_columns
		do

		end

	update_status_bar
		do

		end


	mouse_click (a_x, a_y, a_button: INTEGER; a_x_tilt, a_y_tilt, a_pressure: DOUBLE; a_screen_x, a_screen_y: INTEGER)
		do
		end
end
